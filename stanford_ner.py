
# coding: utf-8

# In[1]:

import os
import nltk.tag.stanford as nert
from nltk.internals import find_jars_within_path


# In[2]:

STANFORD_PARAMS = {}
HOME_ROOT = 'LibraryFiles/lib/stanford-ner-2015-12-09'

STANFORD_PARAMS['home'] = [HOME_ROOT + '/classifiers', HOME_ROOT + '/classifiers/english.muc.7class.distsim.crf.ser.gz', HOME_ROOT + '/stanford-ner.jar']


# In[3]:

def create_tagger():
    params = STANFORD_PARAMS['home']
    os.environ['STANFORD_MODELS'] = params[0]
    os.environ['CLASSPATH'] = params[2]
    tagger = nert.StanfordNERTagger(params[1])
    stanford_dir = [i.rpartition('/')[0] for i in tagger._stanford_jar]
    stanford_jars = find_jars_within_path(stanford_dir[0])
    tagger._stanford_jar = ':'.join(stanford_jars)
    
    return tagger
    


# In[4]:

# tagger = create_tagger()

# tagger.tag('Dave Johnson will be studying at Stony Brook University in New York next year and will pay $ 500 as rent which will be due by March 1st 10:00 AM'.split())


# In[ ]:



