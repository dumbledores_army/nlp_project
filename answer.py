
# coding: utf-8

# In[1]:

import nltk
from ans_preprocess import *
from ans_helper import * 
import sys
import stanford_ner
import stanford_parser
from question_helper import *
from postag import *
import re
from corefence import *


# In[2]:


# Run tagger to determine which WH question
def det_ans(qa):
#Create Stanford NER Tagger instance
    try:
        i=0
        answer=[]
        for q in qa:
    #         print i 
            i=i+1
            tagger = stanford_ner.create_tagger()
            q_t = q[0].translate(None, string.punctuation)
            a_t = q[1].translate(None, string.punctuation)
            q_type = q[2]
            indices =q[3]
            tags = tagger.tag(a_t.split())
            tags =get_continuous_chunks(tags)
            if q_type == 'who':
                #if who question
                answer.append( answer_who(q_t,a_t,tags))
            elif q_type == 'when':
                #if when question
                answer.append( answer_when(q_t,a_t,tags))
            elif q_type == 'how':
                #if when question
                answer.append( answer_how(q_t,a_t,tags))
            elif q_type == 'where':
                #if when question
                answer.append( answer_where(q_t,a_t,tags))
            elif q_type == 'binary':
                answer.append(answer_binary(q_t,a_t))
            else:
                 answer.append(q[1])
    except:
        pass
    return answer


# In[3]:

'''
This function sets the question type to the corresponding  question type
'''
wh_words = ['how','whose','who','whom','where','when','why','what','which']
bin_words = ['is','did','had','was']
#'why', 'which', , 'what' #q_type = 'how_many'???
def ques_type(a):
    try:
        qsw_tag= nltk.pos_tag(a.lower().split()[0:])
        word = qsw_tag[0][0]
        #print word
        if word in wh_words:
            if word == 'how':
                if qsw_tag[1][0]== 'much' or qsw_tag[1][0] =='many':
                    q_type ='how'
            elif word == 'who' or word == 'whom' or word == 'whose':
                q_type ='who'
            elif word == 'when':
                q_type ='when'
            elif word == 'where':
                q_type ='where' 
            elif word == 'which':
                q_type ='which' 
            elif word == 'why':
                q_type ='why' 
            elif word == 'what':
                q_type = 'what'
        elif word in bin_words:
            q_type ='binary'
        else:
            q_type = 'other'
    except:
        pass
    return q_type
    
    


# In[4]:

'''
Determine the question type of each of the questions
'''
def find_qtype(sent_list):
    try:
        qt_list = list()
        for s in sent_list:
            z = ques_type(s)
            qt_list.append(z)
    except:
        pass       
    return qt_list
    


# In[5]:

'''  
     preprocesses the data and the question set. 
     Then it finds the sentence with highest cosine similarity to the question.
     returns a tuple containing the question, best answer and the question type
 '''
def gen_ans(file1, file2):
    try:
        k = prep_data(file1)
        f1 = open(file2)
        sent_list = f1.read().splitlines()
        f1.close()
        a = []
        indices = []
        for i in sent_list:
            d = best_match(k,i)
            #print d
            #print type(d)
            #d = int(d)
            #print type(d)
            a.append(k[d])
            indices.append(int(d))
        ques_type = find_qtype(sent_list)
        #print ques_type
        qa=zip(sent_list,a, ques_type,indices) 
    except:
        pass
    return qa


# In[6]:

#return question_answer pairs
def answering_sys(filename, qfile):
    answe=[]
    try:
        k = prep_data(filename)
        qa = gen_ans(filename, qfile)
        newans = list()
        sent_list = list()
        ques_type = list()
        indices = list()
        for i in range(len(qa)):
            t = qa[i][3]
            l = list()
            for j in range (t-12,t+1):
                l.append(k[j])
            text = ''.join(l)
            n_ans=process_answer(text,qa[i][1])
            newans.append(n_ans)

        for q in range(len(qa)):
            sent_list.append(qa[q][0])
            ques_type.append(qa[q][2])
            indices.append(qa[q][3])
        #print indices
        qa2=zip(sent_list,newans, ques_type,indices)
        answe =det_ans(qa2)
    except:
        pass
    return answe



# In[8]:

filename  = sys.argv[1] 
qfile = sys.argv[2] 

# filename  = 'data/S08/data/set4/a5o.htm'
    # qfile ='data/S08/eg_Anders_Celsius.txt'
f_ans = answering_sys(filename,qfile)

for i in f_ans:
    print i


# In[ ]:




# In[ ]:



