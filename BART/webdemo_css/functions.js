function prepare_nodes() {
  var corefNode=document.getElementById("coref");
  var markables=corefNode.getElementsByTagName("span");
  for (var i=0; i<markables.length; i++) {
    e=markables[i];
    if (e.hasAttribute("set-id")) {
      var s=e.getAttribute("set-id");
      e.setAttribute("onClick",
        "set_activeset('"+s+"')");
      e.setAttribute("class", "coref-inactive");
    }
  }
}

function set_activeset(setid) {
  var corefNode=document.getElementById("coref");
  var markables=corefNode.getElementsByTagName("span");
  //alert(markables);
  var s="";
  for (var i=0; i<markables.length; i++) {
    e=markables[i];
    //alert(e);
    if (e.hasAttribute("set-id")) {
        if (e.getAttribute("set-id")==setid) {
          e.setAttribute("class","coref-active");
          s+="<div>"+e.textContent+"</div>";
        } else {
          e.setAttribute("class","coref-inactive");
        }
      }
  }
  document.getElementById("coref-chain").innerHTML=s;
}

function display_wait(s) {
  var mainPanel=document.getElementById("leftpanel");
  mainPanel.innerHTML='<div class="waitingmsg">'+s+'<br/><img src="anicat30.gif"></div>';
}
function display_newdoc() {
  var mainPanel=document.getElementById("leftpanel");
  mainPanel.innerHTML='<b>Please enter text to preprocess:<b><br>'+
'<form name="newdoc_form"><textarea name="text" rows="10" cols="80">'+
'Enter some text that BART should preprocess and do coreference resolution on. If BART has found a cluster of coreferent mentions, they will be displayed using the same number'+
'</textarea><br/>'+
'<input type="button" value="Preprocess" onClick="submit_newtext()"/> </form>';
}



//
// AJAX stuff
//
// we have two request objects:
// leftRequest -- stuff that should go into the leftpane div
// rightRequest -- stuff that should go into the rightpane div
//

function createXMLHttpRequest() {
   try { return new XMLHttpRequest(); } catch(e) {}
   try { return new ActiveXObject("Msxml2.XMLHTTP"); } catch (e) {}
   try { return new ActiveXObject("Microsoft.XMLHTTP"); } catch (e) {}
   alert("XMLHttpRequest not supported");
   return null;
 }
 
var leftRequest=createXMLHttpRequest();
var leftActive=false;
var rightRequest=createXMLHttpRequest();
var rightActive=false;
var rightUpdateNeeded=false;
var corefSetupNeeded=false;

function updateRight() {
    if (!rightActive) {
        rightRequest.open("GET","/BARTDemo/ShowText/listDocs");
        rightRequest.onreadystatechange=displayRight;
        rightRequest.send(null);
        rightUpdateNeeded=false;
    }
}

function displayLeft() {
    if (leftRequest.readyState==4) {
        var mainPanel=document.getElementById("leftpanel");
        mainPanel.innerHTML=leftRequest.responseText;
        leftActive=false;
        if (rightUpdateNeeded) {
            updateRight();
        } else if (corefSetupNeeded) {
            prepare_nodes();
        }
    }
}

function displayRight() {
    if (rightRequest.readyState==4) {
        var rightPanel=document.getElementById("rightpanel");
        rightPanel.innerHTML=rightRequest.responseText+"<hr>"+
        '<a href="#" onclick="display_newdoc()">Create new document...</a>';
        rightActive=false;
    }
}

function submit_newtext() {
    if (!leftActive) {
        leftRequest.open("POST","/BARTDemo/ShowText/addDoc/");
        leftRequest.onreadystatechange=displayLeft;
        var text=document.forms.newdoc_form.text.value;
        display_wait("Preprocessing Text...");
        leftRequest.send(text);
        rightUpdateNeeded=true;
    }
}

function renderDoc(docid,fmt) {
    if (!leftActive) {
        leftRequest.open("GET","/BARTDemo/ShowText/renderDoc?docId="+docid+
            "&fmt="+fmt);
        leftRequest.onreadystatechange=displayLeft;
        leftRequest.send(null);
    }
}

function renderCoref(docid) {
    if (!leftActive) {
        leftRequest.open("GET","/BARTDemo/ShowText/renderCoref?docId="+docid);
        leftRequest.onreadystatechange=displayLeft;
        leftRequest.send(null);
        display_wait("Resolving Coreference ...");
        corefSetupNeeded=true;
    }
}