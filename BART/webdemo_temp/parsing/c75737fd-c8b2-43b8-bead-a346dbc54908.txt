Un torcicollo. Questa la causa dell'assenza del premier Silvio Berlusconi
all'assemblea di Confcommercio. Il premier avrebbe dovuto prendere la parola
dal palco ma ha annullato l'intervento �perch� immobilizzato a casa dal
torcicollo che lo tormenta da tempo e che forse � qualcosa di pi�� ha detto
il sottosegretario alla presidenza, Gianni Letta, intervenuto al posto del
presidente del Consiglio. �Non � riuscito a muoversi - ha spiegato Letta -,
ha chiamato un medico per farsi una puntura. Se dovesse passargli il
torcicollo forse riuscir� a venire ad ascoltare le conclusioni del
presidente Sangalli, altrimenti spero che lo comprenderete�, ha aggiunto il
sottosegretario alla presidenza. �Se non ci fosse stata una ragione cos�
seria e cos� fortemente impeditiva, Berlusconi avrebbe fatto qualunque
rinuncia per essere qui� ha aggiunto Letta, scusandosi con la platea. �Ma
Berlusconi � presente in spirito - ha proseguito - e sar� sicuramente
presente alla prossima assemblea della vostra associazione�. Prima di
fornire questa spiegazione, Letta aveva ironizzato sul tono del presidente
Sangalli che lo aveva brevemente preceduto sul palco: �Ha parlato con
amarezza, sofferenza e tormento...sembrava dovesse dare l'annuncio di un
evento luttuoso tanto era il suo rammarico per dovervi dire una cosa che
neanche vi ha detto, e cio� che il presidente Berlusconi non sarebbe
venuto�.
