/*
 * WebPatterns.java
 *
 * Created on July 29, 2007, 11:20 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package elkfed.knowledge;

import elkfed.config.ConfigProperties;
import elkfed.knowledge.ngram.GoogleNGram;
import elkfed.ml.util.FeatureVector;
import elkfed.web.MSNSearch;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.rmi.RemoteException;
import java.util.EnumMap;
import java.util.Map;
import org.apache.axis2.AxisFault;

/**
 *
 * @author yannick
 */
public class WebPatterns {
    protected static WebPatterns _instance;
    protected static MSNSearch _the_search;
    
    public static MSNSearch getSearch()
    {
        if (_the_search==null)
        {
            try {
                _the_search=new MSNSearch();
            } catch (AxisFault e) {
                e.printStackTrace();
                throw new RuntimeException("Cannot make MSNSearch instance",e);
            }
        }
        return _the_search;
    }
    
    public static enum Filler
    {
      ANA,
      ANA_DEF,
      ANA_INDEF,
      ANA_PL,
      ANA_DEF_PL,
      ANTE;
    };
    
    public static enum Pattern
    {
        AND_OTHERS(Filler.ANTE,"and other",Filler.ANA_PL, 4.501e-12),
        SUCH_AS(Filler.ANA_PL,"such as",Filler.ANTE, 1.186e-12),
        COPULA(Filler.ANTE,"is",Filler.ANA_INDEF, 7.271e-12),
        APP_SG(Filler.ANA_DEF,"",Filler.ANTE, 4.173e-12),
        APP_PL(Filler.ANA_DEF_PL,"",Filler.ANTE, 4.702e-12),
        INCLUDING(Filler.ANA_PL,"including",Filler.ANTE,4e-12);
        private final Filler _slot1;
        private final Filler _slot2;
        private final String _between;
        private final double _basefreq;
        
        Pattern(Filler slot1, String between, Filler slot2, double basefreq)
        {
            _slot1=slot1;
            _slot2=slot2;
            _between=between;
            _basefreq=basefreq;
        }
        public String produceString(Map<Filler,String> fillers)
        {
            return String.format("%s %s %s",
                    fillers.get(_slot1),_between,fillers.get(_slot2));
        }

        public Filler getSlot1() {
            return _slot1;
        }

        public Filler getSlot2() {
            return _slot2;
        }

        public String getBetween() {
            return _between;
        }

        public double getBasefreq() {
            return _basefreq;
        }
        
    };
    
    protected MSNSearch _search;
    protected final Pattern[] _patterns;
    protected final double _threshold;
    
    public static WebPatterns getInstance() {
        if (_instance==null) {
            _instance=new WebPatterns();
        }
        return _instance;
    }
    
    /** Creates a new instance of WebPatterns */
    public WebPatterns() {
        _search=getSearch();
        _patterns=Pattern.values();
        _threshold=-8.0;
    }
    
    public WebPatterns(Pattern[] patterns, double threshold) {
        _search=getSearch();
        _patterns=patterns;
        _threshold=threshold;
    }
            
    public boolean isMatch(String ana, String ante) {
        try
        {
            boolean anyMatch=false;
            String anaPl=sg2pl(ana);
            EnumMap<Filler, String> fillers = genFillers(ante, ana, anaPl);
            EnumMap<Pattern,Double> patternFreq=
                    new EnumMap<Pattern,Double>(Pattern.class);
            EnumMap<Filler, Double> fillerFreq = getFillerFreqs(fillers);
            double score=0.0;
            for (Pattern p: _patterns)
            {
                double expected=(fillerFreq.get(p.getSlot1())+0.5) *
                        (fillerFreq.get(p.getSlot2())+0.5) *
                        p.getBasefreq();
                if (expected < 0.5)
                {
                    System.err.format("%s: expected: %g, not querying\n",
                            p.produceString(fillers), expected);
                    continue;
                }
                double patFreq=_search.webcount_phrase(p.produceString(fillers));
                if (patFreq>0.0) anyMatch=true;
                patternFreq.put(p,
                        (double)patFreq);
                //System.err.format("%s: expected: %g (baseFreq %g) got: %g\n",p.toString(),
                        //expected, p.getBasefreq(), patFreq);
                score += Math.log(patFreq+0.1) - Math.log(expected);
            }
//            System.out.println(fillerFreq);
//            System.out.println(patternFreq);
            System.err.format("score: %f\n",score);
            return (score > _threshold && anyMatch);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Cannot run web patterns", e);
        }
    }

    public void addMatchInfo(String ana, String ante,FeatureVector<String> tok) {
        try
        {
            boolean anyMatch=false;
            String anaPl=sg2pl(ana);
            EnumMap<Filler, String> fillers = genFillers(ante, ana, anaPl);
            EnumMap<Pattern,Double> patternFreq=
                    new EnumMap<Pattern,Double>(Pattern.class);
            EnumMap<Filler, Double> fillerFreq = getFillerFreqs(fillers);
            double score=0.0;
            for (Pattern p: _patterns)
            {
                double expected=(fillerFreq.get(p.getSlot1())+0.5) *
                        (fillerFreq.get(p.getSlot2())+0.5) *
                        p.getBasefreq();
                if (expected < 0.5)
                {
                    System.err.format("%s: expected: %g, not querying\n",
                            p.produceString(fillers), expected);
                    tok.setFeatureValue("NQ"+p,1.0);
                    continue;
                }
                double patFreq=_search.webcount_phrase(p.produceString(fillers));
                score += Math.log(patFreq+0.1) - Math.log(expected);
                if (patFreq==0.0)
                {
                    tok.setFeatureValue("NM"+p,1.0);
                    tok.setFeatureValue("NR"+p,Math.max(-Math.log(0.5/expected)/5.0,0.0));
                }
                else
                {
                    tok.setFeatureValue("WR"+p,Math.max(0.0,1.0+Math.log(patFreq+0.5/expected)/5.0));
                    anyMatch=true;
                }
            }
            tok.setFeatureValue("WAny",anyMatch?1.0:0.0);
            tok.setFeatureValue("WPredict",anyMatch&&score>_threshold?1.0:0.0);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Cannot run web patterns", e);
        }
    }
    
    private EnumMap<Filler, Double> getFillerFreqs(final EnumMap<Filler, String> fillers)
        throws RemoteException {
        EnumMap<Filler,Double> fillerFreq=
                new EnumMap<Filler,Double>(Filler.class);
        for (Filler f: Filler.values())
        {
            String str=fillers.get(f);
            assert(str!=null);
            fillerFreq.put(f, 
                    (double)_search.webcount_phrase(str));
        }
        return fillerFreq;
    }

    private EnumMap<Filler, String> genFillers(final String ante,
            final String ana, final String anaPl)
    {
        EnumMap<Filler,String> fillers=
                new EnumMap<Filler,String>(Filler.class);
        fillers.put(Filler.ANA,ana);
        fillers.put(Filler.ANTE,ante);
        fillers.put(Filler.ANA_PL,anaPl);
        fillers.put(Filler.ANA_DEF_PL,"the "+anaPl);
        fillers.put(Filler.ANA_DEF,"the "+ana);
        if (isVowel(ana.charAt(0)))
        {
            if (ana.toLowerCase().startsWith("uni"))
            {
                fillers.put(Filler.ANA_INDEF,"a "+ana);
            }
            else
            {
                fillers.put(Filler.ANA_INDEF,"an "+ana);
            }
        }
        else
        {
            fillers.put(Filler.ANA_INDEF,"a "+ana);
        }
        return fillers;
    }
    
    public static String sg2pl(String sg)
    throws IOException, InterruptedException {
        String[] cmd={ConfigProperties.getInstance().getMorphgExe()};
        String result=null;
        try {
            Process morphg=Runtime.getRuntime().exec(cmd);
            PrintWriter w=new PrintWriter(morphg.getOutputStream());
            w.format("%s+s_NNS\n",sg);
            w.close();
            BufferedReader r=new BufferedReader(
                    new InputStreamReader(morphg.getInputStream()));
            result=r.readLine();
            r.close();
            morphg.getErrorStream().close();
            morphg.waitFor();
            if (result==null) throw new IOException();
        }
        catch (IOException e)
        {
            // sleep a while and then try again... grr
            e.printStackTrace();
            Thread.sleep(300);
            Process morphg=Runtime.getRuntime().exec(cmd);
            PrintWriter w=new PrintWriter(morphg.getOutputStream());
            w.format("%s+s_NNS\n",sg);
            w.close();
            BufferedReader r=new BufferedReader(
                    new InputStreamReader(morphg.getInputStream()));
            result=r.readLine();
            r.close();
            morphg.getErrorStream().close();
            morphg.waitFor();
        }
        return result;
    }
    
    public static boolean isVowel(char c) {
        switch (c)
        {
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
            case 'A':
            case 'E':
            case 'I':
            case 'O':
            case 'U':
                return true;
            default:
                return false;
        }
    }
    
    public static void main(String[] args)
    {
        if (args.length==0)
        {
             try{
                String[] examples={"house", "mouse", "flower"};
                for (String ex: examples) {
                    System.out.format("%s -> %s\n",ex,sg2pl(ex));
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
                System.exit(1);
            }
            try{
                String[][] testValuesPos={
                    {"New York", "city"},
                    {"Dollar", "currency"},
                    {"Clinton", "president"},
                    {"China", "country"},
                    {"Europe", "continent"},
                    {"Monsanto", "company"},
                    {"Gordon Brown", "prime minister"},
                    {"AIDS","disease"},
                    {"FAA", "agency"},
                    {"Gitano", "company"},
                    {"Linea Gitano", "brand"},
                    {"START", "treaty"}
                };
                String[][] testValuesNeg={
                    {"New York", "country"},
                    {"Dollar", "shore"},
                    {"China", "region"},
                    {"Europe", "ocean"},
                    {"Gordon Brown", "country"},
                    {"AIDS", "treatment"}
                };
                for (String[] vals: testValuesPos)
                {
                    boolean match=getInstance().isMatch(vals[1],vals[0]);
                    System.out.format("%s %s -> %s\n",vals[1],vals[0],match);
                }
                for (String[] vals: testValuesNeg)
                {
                    boolean match=getInstance().isMatch(vals[1],vals[0]);
                    System.out.format("%s %s -> %s\n",vals[1],vals[0],match);
                }

            }
            catch (Exception e)
            {
                e.printStackTrace();
                System.exit(1);
            }
        }
        else
        {
            try{
                int[] result=new int[4];
                BufferedReader br=new BufferedReader(new FileReader(args[0]));
                String line;
                Pattern[] used_patterns={Pattern.AND_OTHERS,
                    Pattern.SUCH_AS};
                //WebPatterns w=new WebPatterns(used_patterns,-5.0);
                while ((line=br.readLine())!=null)
                {
                    String[] fields=line.split("\t");
                    //boolean match=w.isMatch(fields[0],fields[2]);
                    String ana_pl=sg2pl(fields[0]);
                    boolean match=(GoogleNGram.getInstance().pattern_ratio(
                            fields[2],"and other",ana_pl)>1.0);
//                            GoogleNGram.getInstance().pattern_ratio(
//                            ana_pl,"such as",fields[2])>2.0);
                    System.out.format("%s %s -> %s\n",fields[0],fields[2],match);
                    boolean wanted_match="true".equals(fields[5]);
                    result[(match?2:0)+(wanted_match?1:0)]++;
                }
                System.out.format("TP:%d FP:%d FN:%d TN:%d\n",
                        result[3],result[2],
                        result[1],result[0]);
                System.out.format("Precision: %.3f Recall: %.3f\n",
                        result[3]/Math.max(result[3]+result[2],1.0),
                        result[3]/Math.max(result[3]+result[1],1.0));
            }
            catch (Exception e)
            {
                e.printStackTrace();
                System.exit(1);
            }                
        }
    }
}
