/*
 * NGramPatterns.java
 *
 * Created on August 8, 2007, 2:53 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package elkfed.knowledge;

import elkfed.ml.util.FeatureVector;
import elkfed.ml.util.Alphabet;
import elkfed.knowledge.ngram.GoogleNGram;
import elkfed.ml.util.KVFunc;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;

/**
 *
 * @author yannick
 */
public class NGramPatterns {
    
    public static FeatureVector<String> makePatternToken(String ana, String ante,
            Alphabet<String> dict) {
        try {
            String ana_pl=WebPatterns.sg2pl(ana);
            FeatureVector<String> tok=new FeatureVector<String>(dict);
            System.err.println("Step 1");
            GoogleNGram.extractPatternVector(ante,ana,"ji",tok);
            System.err.println("Step 2");
            GoogleNGram.extractPatternVector(ana,ante,"ij",tok);
            System.err.println("Step 3");
            GoogleNGram.extractPatternVector(ante,ana_pl,"jI",tok);
            System.err.println("Step 4");
            GoogleNGram.extractPatternVector(ana_pl,ante,"Ij",tok);
            if (tok.getFeatures().isEmpty()) {
                tok.setFeatureValue("*nomatch*",1.0);
            }
            return tok;
        } catch (IOException e) {
            throw new RuntimeException("sg2pl failed", e);
        } catch (InterruptedException e) {
            throw new RuntimeException("sg2pl failed", e);
        }
    }
    
    public static FeatureVector<String>
            makeSinglePatternToken(String ana, String ante, int pat, Alphabet dict) {
        try {
            String ana_pl=WebPatterns.sg2pl(ana);
            FeatureVector tok=new FeatureVector(dict);
            switch(pat) {
                case 0:
                    System.err.println("Step 1");
                    GoogleNGram.extractPatternVector(ante,ana,"ji",tok);
                    break;
                case 1:
                    System.err.println("Step 2");
                    GoogleNGram.extractPatternVector(ana,ante,"ij",tok);
                    break;
                case 2:
                    System.err.println("Step 3");
                    GoogleNGram.extractPatternVector(ante,ana_pl,"jI",tok);
                    break;
                case 3:
                    System.err.println("Step 4");
                    GoogleNGram.extractPatternVector(ana_pl,ante,"Ij",tok);
                    break;
            }
            if (tok.getFeatures()==null) {
                tok.setFeatureValue("*nomatch*",1.0);
            }
            return tok;
        } catch (IOException e) {
            throw new RuntimeException("sg2pl failed", e);
        } catch (InterruptedException e) {
            throw new RuntimeException("sg2pl failed", e);
        }
    }
    
    private static final WebPatterns.Pattern[] used_patterns={
        WebPatterns.Pattern.AND_OTHERS,
        WebPatterns.Pattern.SUCH_AS};
    private static final WebPatterns webpat=new WebPatterns(used_patterns, -8.0);
    public static FeatureVector<String> makeWebToken(String ana, String ante,
            Alphabet dict) {
        FeatureVector<String> tok=new FeatureVector<String>(dict);
        webpat.addMatchInfo(ana,ante,tok);
        return tok;
    }
    
    public static void printFeatures(FeatureVector fv, final PrintWriter wr) {
        fv.put(new KVFunc() {
            public void put(int idx, double val) {
                wr.format(" %d:%f",idx+1,val);
            }
        });
    }
    
    private final static int __NUM_PAT=4;
    public static void main(String[] args) {
        if (args.length>0) {
            try{
                BufferedReader br=new BufferedReader(new FileReader(args[0]));
                String line;
                Alphabet<String> dict=new Alphabet<String>();
                String basename=args[0];
                PrintWriter wr=new PrintWriter(new FileOutputStream(basename+".data"));
                while ((line=br.readLine())!=null) {
                    String[] fields=line.split("\t");
                    if (fields[5].equals("false"))
                        wr.print("-1");
                    else
                        wr.print("+1");
                    boolean any_pattern=false;
//                    for (int i=0; i<__NUM_PAT; i++) {
//                        Token result=makeSinglePatternToken(fields[0], fields[2], i);
//                        if (result.getFeatureValue("*nomatch*")==0.0) {
//                            any_pattern=true;
//                        }
//                        Instance inst=new Instance(result,fields[5],null,null);
//                        pipe[i].pipe(inst);
//                        printFeatures((FeatureVector)inst.getData(),wr);
//                        wr.print(" |BV|");
//                    }
                    FeatureVector<String> result=makePatternToken(fields[0], fields[2],
                            dict);
                    System.out.println(result);
                    if (result.getFeatureValue("*nomatch*")==0.0) {
                        any_pattern=true;
                    }
                    printFeatures(result,wr);
                    wr.print(" |BV|");
                    FeatureVector<String> resultW=makeWebToken(fields[0],fields[2],dict);
                    if (any_pattern) {
                        resultW.setFeatureValue("PAny",1.0);
                    } else {
                        resultW.setFeatureValue("PNone",1.0);
                    }
                    printFeatures(resultW,wr);
                    wr.print(" |EV|");
                    wr.println();
                }
                wr.close();
                ObjectOutputStream os=new ObjectOutputStream(new FileOutputStream(basename+".dict"));
//                for (int i=0; i<__NUM_PAT; i++) {
//                    os.writeObject(pipe[i].getDataAlphabet());
//                }
                os.writeObject(dict);
                os.close();
            } catch (Exception e) {
                e.printStackTrace();
                System.exit(1);
            }
        } else {
            Alphabet<String> dict=new Alphabet<String>();
            FeatureVector<String> result=makePatternToken("New York","city",dict);
            System.err.println(result);
        }
    }
}
