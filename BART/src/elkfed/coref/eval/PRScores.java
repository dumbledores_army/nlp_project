package elkfed.coref.eval;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import static elkfed.util.Strings.*;
import static elkfed.coref.eval.PRScore.round;
import static elkfed.coref.eval.PRScore.SCALE;

/** A list of p/r/f1 scores
 *
 * @author ponzo
 */
public class PRScores extends ArrayList<PRScore>
{ 
    private static final String SEPARATOR = getNTimes('-',72) + "\n";
    
    private final StringBuffer buffer;
    
    public PRScores()
    {
        super();
        this.buffer = new StringBuffer();
    }
    
    public String toString()
    {
        buffer.setLength(0);
        buffer.
                append(toPaddedString("ID")).append(getNTimes(' ', 2)).
                append(toPaddedString("RECALL",10)).
                append(toPaddedString("PRECISION",10)).
                append(toPaddedString("F_1",10)).append("\n").append(SEPARATOR);
        for (PRScore score : this)
        {
            buffer.
                append(toPaddedString(score.getId())).append(getNTimes(' ', 2)).
                append(toPaddedString(round(score.getRecall()),10)).
                append(toPaddedString(round(score.getPrecision()),10)).
                append(toPaddedString(round(score.getScore()),10)).
                append("\n");
        }
        return buffer.append(SEPARATOR).toString();
    }
    
    public static void main(String[] args)
    {
        PRScores scores = new PRScores();
        scores.add(new PRScore(0.6, 0.3, "A"));
        scores.add(new PRScore(0.1, 0.8, "B"));
        scores.add(new PRScore(0.8, 0.4, "C"));
        
        System.out.println(scores);
    }
}
