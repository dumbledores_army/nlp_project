/*
 * AFE_DefRatio.java
 *
 * Created on August 10, 2007, 4:50 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package elkfed.coref.features.anaphoricity;

import elkfed.coref.AnaphoricityInstance;
import elkfed.knowledge.WebPatterns;
import elkfed.ml.FeatureDescription;
import elkfed.ml.FeatureExtractor;
import elkfed.ml.FeatureType;
import elkfed.web.MSNSearch;
import java.rmi.RemoteException;
import java.util.List;

/**
 *
 * @author yannick
 */
public class AFE_DefRatio implements FeatureExtractor<AnaphoricityInstance>
{
    public static final FeatureDescription<Boolean> FD_DEF_RATIO=
            new FeatureDescription<Boolean>(FeatureType.FT_BOOL, "DefRatio");
    MSNSearch _search=WebPatterns.getSearch();

    public void describeFeatures(List<FeatureDescription> fds) {
        fds.add(FD_DEF_RATIO);
    }

    public void extractFeatures(AnaphoricityInstance inst) {
        if (inst.getMention().getDefinite())
        {
            String head=inst.getMention().getHeadString();
            String def="the "+head;
            String indef;
            if (WebPatterns.isVowel(head.charAt(0)) &&
                !head.toLowerCase().startsWith("uni"))
                indef="a "+head;
            else
                indef="an "+head;
            try {
                int num_def=_search.webcount_phrase(def);
                int num_indef=_search.webcount_phrase(indef);
                inst.setFeature(FD_DEF_RATIO,(num_def/num_indef > 7));
            } catch (RemoteException ex) {
                throw new RuntimeException("cannot run web count",ex);
            }
        }
    }
    
}
