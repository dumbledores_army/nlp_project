/*
 * FE_WebPatterns.java
 *
 * Created on July 30, 2007, 2:22 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package elkfed.coref.features.pairs;

import elkfed.coref.PairFeatureExtractor;
import elkfed.coref.PairInstance;
import elkfed.knowledge.WebPatterns;
import elkfed.ml.FeatureDescription;
import elkfed.ml.FeatureType;
import elkfed.nlp.util.AdjectiveMap;
import elkfed.nlp.util.Stopwords;
import java.util.List;
import static elkfed.lang.EnglishLinguisticConstants.*;

/**
 *
 * @author yannick
 */
public class FE_WebPatterns implements PairFeatureExtractor {
    public static final FeatureDescription<Boolean> FD_WEB_MATCH=
            new FeatureDescription<Boolean>(FeatureType.FT_BOOL,
                "WebMatch");
    private final WebPatterns _wp;
    
    public FE_WebPatterns()
    {
        WebPatterns.Pattern[] used_patterns={
            WebPatterns.Pattern.AND_OTHERS,
            WebPatterns.Pattern.SUCH_AS};
        _wp=new WebPatterns(used_patterns,-8.0);
    }
 
    public void describeFeatures(List<FeatureDescription> fds) {
        fds.add(FD_WEB_MATCH);
    }

    public void extractFeatures(PairInstance inst) 
    {
        if (!inst.getAnaphor().getDefinite() ||
                !inst.getAntecedent().getProperName())
        {
            inst.setFeature(FD_WEB_MATCH, false);
        }
        else if (!inst.getAntecedent().getNumber() ||
                !inst.getAnaphor().getNumber())
        {
            inst.setFeature(FD_WEB_MATCH, false);
        }
        else if (inst.getAnaphor().getSentId() -
                    inst.getAntecedent().getSentId() > 4)
        {
            inst.setFeature(FD_WEB_MATCH, false);
        }
        else if (inst.getAnaphor().getPremodifiers()!=null)
        {
            inst.setFeature(FD_WEB_MATCH, false);            
        }
        else if (inst.getAntecedent().getHeadOrName().toLowerCase()
            .endsWith(inst.getAnaphor().getHeadString().toLowerCase()))
        {
            inst.setFeature(FD_WEB_MATCH, 
                    inst.getAntecedent().getMarkableString().toLowerCase()
                        .startsWith(inst.getAnaphor().getHeadString()));
        }
        else if (!inst.getAnaphor().getPostmodifiers().isEmpty())
        {
            inst.setFeature(FD_WEB_MATCH, false);            
        }
        else if (inst.getAntecedent().getMarkableString().matches(PRONOUN) ||
                inst.getAnaphor().getMarkableString().matches(PRONOUN) ||
                Stopwords.getInstance()
                    .contains(inst.getAntecedent().getMarkableString().toLowerCase()) ||
                Stopwords.getInstance()
                    .contains(inst.getAnaphor().getMarkableString().toLowerCase()))
        {
            // work around bugs in markable creation ?!
            inst.setFeature(FD_WEB_MATCH, false);
        }
        else
        {
            String ana=inst.getAnaphor().getHeadOrName();
            String ante=inst.getAntecedent().getHeadOrName();
            ante=AdjectiveMap.getInstance().map(ante);
            if (inst.getAntecedent().getMarkableString().toLowerCase().startsWith("the ") &&
                    !ante.toLowerCase().startsWith("the "))
                    ante="the "+ante;
            inst.setFeature(FD_WEB_MATCH,
                    _wp.isMatch(
                        ana,ante));
        }
    }
    
}
