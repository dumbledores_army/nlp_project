/*
 * FE_Wiki_Alias.java
 *
 * Created on August 8, 2007, 11:28 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package elkfed.coref.features.pairs.wiki;

import elkfed.coref.PairFeatureExtractor;
import elkfed.coref.PairInstance;
import elkfed.knowledge.WikiDB;
import elkfed.ml.FeatureDescription;
import elkfed.ml.FeatureType;
import static elkfed.lang.EnglishLinguisticConstants.PRONOUN;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import net.sf.snowball.ext.EnglishStemmer;

/**
 * Checks to see if the two mentions redirect or link to the same Wikipedia page
 * @author jrsmith
 */
public class FE_Wiki_Alias implements PairFeatureExtractor {
        
    /** The antecedent head */
    private String antecedentString;
    
    /** The anaphora head */
    private String anaphoraString;
    
    public static final FeatureDescription<Double> FD_IS_WIKI_ALIAS=
            new FeatureDescription<Double>(FeatureType.FT_SCALAR, "FD_IS_WIKI_ALIAS");
    
    public static final FeatureDescription<Boolean> FD_IS_WIKI_REDIR=
            new FeatureDescription<Boolean>(FeatureType.FT_BOOL, "FD_IS_WIKI_REDIR");
    
    public static final FeatureDescription<Boolean> FD_WIKI_LISTS=
            new FeatureDescription<Boolean>(FeatureType.FT_BOOL, "FD_WIKI_LISTS");
   
    /** The statement to the db */
    private transient WikiDB wikidb;
    
    /** Creates a new instance of FE_Wiki_Alias */
    public FE_Wiki_Alias() {
        wikidb=WikiDB.getInstance();
    }
    
    public void extractFeatures(PairInstance inst)
    {
        setStrings(inst);
        try {
            setFeatures(inst);
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
    }
    
    /** Sets the Wikipedia strings to be parsed */
    private void setStrings(PairInstance inst)
    {
        this.antecedentString=inst.getAntecedent().getHeadOrName().toLowerCase();
        this.anaphoraString=inst.getAnaphor().getHeadOrName().toLowerCase();
    }
    
    /** Queries the wikipedia database to determine whether or not two names link to the same
     * page
     */
    private void setFeatures(PairInstance inst) throws SQLException
    {
        double alias = 0.0;
        boolean redir = false;
        boolean list = false;
        if (!antecedentString.matches(PRONOUN) && !anaphoraString.matches(PRONOUN))
        {
            alias = getAliases(inst);
            redir = getRedirects(inst);
            list = getLists(inst);
        }        
        
        inst.setFeature(FD_WIKI_LISTS, list);
        inst.setFeature(FD_IS_WIKI_REDIR, redir);
        inst.setFeature(FD_IS_WIKI_ALIAS, alias);
    }
    
    private double getAliases(PairInstance inst) throws SQLException
    {
        int i, j;
        double match = 0.0;
        if (antecedentString.equalsIgnoreCase(anaphoraString))
        {
            return 1.0;
        }
        String antString = sql_escape(antecedentString.replaceAll(" ", "_"));
        String anaString = sql_escape(anaphoraString.replaceAll(" ", "_"));
        
        HashMap<String, Double> ids = new HashMap<String, Double>();
        
        String antIds = wikidb.getAlias(antString);
        if ((antIds != null) && (!antIds.equals("")))
        {
            String[] antIdsArray = antIds.split("\t");
            
            for(i = 0; i < antIdsArray.length; i++)
            {
                String[] antIdWeight = antIdsArray[i].split(" ");
                ids.put(antIdWeight[0], Double.valueOf(antIdWeight[1]));
            }
        }
        
        String anaIds = wikidb.getAlias(anaString);
        if ((anaIds != null) && !anaIds.equals(""))
        {
            String[] anaIdsArray = anaIds.split("\t");
            
            for(i = 0; i < anaIdsArray.length; i++)
            {
                String[] anaIdWeight = anaIdsArray[i].split(" ");
                if ((anaIdWeight.length == 2) && (ids.containsKey(anaIdWeight[0])))
                {
                    double weightAna = Double.valueOf(anaIdWeight[1]);
                    double weightAnt = ids.get(anaIdWeight[0]);

                    match += weightAna * weightAnt;
                }
            }
        }
        
        return match;
    }
    
    private boolean getRedirects(PairInstance inst) throws SQLException
    {
        
        int i, j;
        boolean match = false;
        
        if (antecedentString.equalsIgnoreCase(anaphoraString))
        {
            return true;
        }
        
        String antString = sql_escape(antecedentString.replaceAll(" ", "_"));
        String anaString = sql_escape(anaphoraString.replaceAll(" ", "_"));
        
        String antId = wikidb.getRedir(antString);
        String anaId = wikidb.getRedir(anaString);
        if ((anaId != null) && (antId != null) &&
                (!anaId.equals("")) && (!antId.equals("")) && anaId.equals(antId))
        {
            match = true;
        }
        
        return match;
    }
    
    private boolean getLists(PairInstance inst) throws SQLException
    {
        int i, j;
        boolean match = false;
        
        if (antecedentString.equalsIgnoreCase(anaphoraString))
        {
            return true;
        }
        
        EnglishStemmer stemmer = new EnglishStemmer();
        
        stemmer.setCurrent(inst.getAntecedent().getHeadOrName().toLowerCase());
	stemmer.stem();
	String antStem = stemmer.getCurrent();
        
        stemmer.setCurrent(inst.getAnaphor().getHeadOrName().toLowerCase());
	stemmer.stem();
	String anaStem = stemmer.getCurrent();
        
        String antString = sql_escape(antecedentString.replaceAll(" ", "_"));
        String anaString = sql_escape(anaphoraString.replaceAll(" ", "_"));
        
        String antIds = wikidb.getLists(antString);
        if ((antIds != null) && (!antIds.equals("")))
        {
            String[] antIdsArray = antIds.split(" ");
            
            for(i = 0; i < antIdsArray.length; i++)
            {
                if (anaStem.equalsIgnoreCase(antIdsArray[i]))
                {
                    match = true;
                }
            }
        }
        
        String anaIds = wikidb.getLists(anaString);
        if ((anaIds != null) && !anaIds.equals(""))
        {
            String[] anaIdsArray = anaIds.split(" ");
            
            for(i = 0; i < anaIdsArray.length; i++)
            {
                if (antStem.equalsIgnoreCase(anaIdsArray[i]))
                {
                    match = true;
                }
            }
        }
        
        return match;
        
    }
    
    /** Modifies a string use in an sql query to avoid escape sequences */
    private String sql_escape(String str)
    {
        String newStr = "";
        for (char c : str.toCharArray())
        {
            if (c == '\'')
            {
                newStr += "\\'";
            }
            else if (c == '"')
            {
                newStr += "\\\"";
            }
            else if (c == '\\')
            {
                newStr += "\\\\";
            }
            else
            {
                newStr += c;
            }
            
        }
        
        return newStr;
    }
    
    public void describeFeatures(List<FeatureDescription> fds) {
        fds.add(FD_IS_WIKI_ALIAS);
        fds.add(FD_IS_WIKI_REDIR);
        fds.add(FD_WIKI_LISTS);
    }
    
    /** Gets the query to connect to a wiki db */
    private String getWikiConnection(String dbName)
    {
       return
          "jdbc:mysql://ndc04:3306/"+ dbName +
          "?useOldUTF8Behavior=true&useUnicode=true&characterEncoding=UTF-8";
    }
       
}
