/*
 * FE_Wiki_Incomp.java
 *
 * Created on August 7, 2007, 4:25 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package elkfed.coref.features.pairs.wiki;


import edu.brandeis.cs.steele.wn.DictionaryDatabase;
import edu.brandeis.cs.steele.wn.FileBackedDictionary;
import edu.brandeis.cs.steele.wn.IndexWord;
import edu.brandeis.cs.steele.wn.POS;
import edu.brandeis.cs.steele.wn.PointerTarget;
import edu.brandeis.cs.steele.wn.PointerType;
import edu.brandeis.cs.steele.wn.Synset;
import elkfed.coref.discourse_entities.DiscourseEntity;
import elkfed.util.IncompatibilityUtil;
import java.util.List;
import elkfed.coref.*;
import elkfed.ml.*;
import static elkfed.lang.EnglishLinguisticConstants.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import ponzo.nlp.wikipedia.WikipediaPageFactory;
//import ponzo.nlp.wikipedia.lang.Language;
import ponzo.nlp.wikipedia.path.WikiPath;
import ponzo.nlp.wikipedia.similarity.WikiSimilarity;
import ponzo.nlp.wikipedia.similarity.WikiSimilarityFactory;
import elkfed.config.ConfigProperties;
import elkfed.knowledge.WNInterface;
import elkfed.knowledge.WikiTaxonomy;
import java.util.regex.Pattern;
import ponzo.nlp.wikipedia.WikiConstants;
import ponzo.nlp.wikipedia.lang.Language;

/**
 * TO DO :
 * Use Wordnet to assign compatibility
 * 
 * 
 *change what was used for wikipedia to the discourse entity stuff
 *compute scores for attbitute comparision
 *
 * Ideas:
 *  check the full markable string for compatibility
 *      -check markable string with only either pre/post modification
 *      -check modifiers against each other one by one
 *
 * distance is irrelevant, cannot use path length features like relatedness features
 * complematary tasks achieved by wordnet/wiki
 *  wiki gets proper nouns and modifiers such as India/America by establishing a graph structure,
 * while wordnet gets common noun synonyms 
 *if there are more than one modifier then having the same modifier doesnt provide information
 *  ex. small house || small house --> yes, we know compatible
 *     red small house || blue small house --> small doesnt give us anything
 *
 *
 *when comparing modifiers (adj) check similar to and adj
 *when comparing nouns
 *
 *DONE: 
 * -get antonym and look up antonym in word net, get its similar to and then compare
 * @author vae2101
 */
public class FE_Wiki_Inc implements PairFeatureExtractor {
    
/**Creates the logger*/
    protected static Logger _log=Logger.getAnonymousLogger();
    
     private static final WikiTaxonomy TAXONOMY_PAIRS_FULL =
        new WikiTaxonomy(ConfigProperties.getInstance().getTaxonomyPairsFull());

    private static final WikiTaxonomy TAXONOMY_PAIRS_HEADS =
        new WikiTaxonomy(ConfigProperties.getInstance().getTaxonomyPairsHeads());
    
   
    private String antecPostModifier;
    private String antecPreModifier;
    private String anaphorPreModifier;
    private String anaphorPostModifier;
    private double incompatibleMeasure;
    private boolean checkModifiers = false;    
    
    /** 
     * File output variables
     */
    public static Random randomGenerator   = new Random();
    PrintWriter out = null;
    PrintWriter dict=  null;
    PrintWriter simMatrix = null;
    private boolean writeMat = false;
    private boolean writeDebug = false;
    private boolean writeDic = false;
    public static int    randomInt         = 0;
  //  public static String outputFilePrefix  = "/home/ws07elerfed/vae2101/Dictionary/";
    public static String outputFilePrefix  = "./";
   
    
    /**
     * Discourse entity information
     */    
    private ArrayList<ArrayList<String>> de_ana_attributes = new ArrayList<ArrayList<String>>();
    private ArrayList<ArrayList<String>> de_ant_attributes = new ArrayList<ArrayList<String>> ();
    private ArrayList<ArrayList<String>>  de_ana_relations = new ArrayList<ArrayList<String>> ();
    private ArrayList<ArrayList<String>>  de_ant_relations = new ArrayList<ArrayList<String>> ();
    private List<Character> de_ana_attributes_pos;
    private List<Character> de_ant_attributes_pos;

    
    /** The antecedent head */
    private String antecedentString;
    
   /** The anaphora head */
    private String anaphoraString;
    
    /** The similarity measures */
    private WikiSimilarity similarity;
     
    /**Setup dictionary / compatibility matrix variables*/
    private static HashMap<String,Integer> dictionary = new HashMap<String,Integer>();
    
    /**Initial size of dictionary made for compatibility matrix*/
    private static Integer dictSize=0;

     
    /** Creates a new instance of FE_Wiki_Incomp */
    public FE_Wiki_Inc() throws IOException{
    
        
        WikiSimilarityFactory.SIMILARITY = true;
        WikiSimilarityFactory.PAGERANK_FILTERING = true;
        
        WikipediaPageFactory.getInstance().setCaching(true);
        WikiSimilarityFactory.getInstance().setCaching(true);
    	WikiSimilarityFactory.getInstance().setComputeGlossOverlapMeasure(false);
    	WikiSimilarityFactory.getInstance().setComputeTextOverlapMeasure(false);
  
        
    
           
           /** Create writers to output to file
            */
   
     
           randomInt      = randomGenerator.nextInt(1000);
	   String outputFilename = new String(outputFilePrefix + "Debug-Nwire-" + randomInt + ".data");
           String outDict=   new String(outputFilePrefix + "Dict-Nwire-" + randomInt + ".data");
           String outMatrix =  new String(outputFilePrefix + "Matrix-Nwire-" + randomInt + ".data");
   

         if(writeDebug)
         out= new PrintWriter(new BufferedWriter(new FileWriter(outputFilename)));

         if(writeDic)
          dict = new PrintWriter(new BufferedWriter(new FileWriter(outDict)));       

         if(writeMat)  
          simMatrix = new PrintWriter(new BufferedWriter(new FileWriter(outMatrix)));       


    }
    
    
    
     public static final FeatureDescription<Double> FD_IS_INCOMPATIBLE=
            new FeatureDescription<Double>(FeatureType.FT_SCALAR, "Semantic_Incompatibility_Measure ");
     
     public static final FeatureDescription<Boolean> FD_IS_W_MOD=
            new FeatureDescription<Boolean>(FeatureType.FT_BOOL, "Modifier_Present ");
     
     private static final Pattern COMPANY_PATTERN =
            Pattern.compile("\\s+"+COMPANY_DESIGNATOR, Pattern.CASE_INSENSITIVE);
      
     private double Wiki_featValue = 0.5;
     private double WN_featValue = 0.5;
     
     public void describeFeatures(List<FeatureDescription> fds) {
        fds.add(FD_IS_INCOMPATIBLE); 
        fds.add(FD_IS_W_MOD);
     }
       
         
     public void extractFeatures(PairInstance inst) {
        
        if(writeDebug)
           _log.log(Level.INFO,"");
            _log.log(Level.INFO,"\n |||||| NEW ENTRY |||| \n");
             
        this.checkModifiers = false;
       WN_featValue = 0.5;
       Wiki_featValue = 0.5;
       
        //set default value for features
        inst.setFeature(FD_IS_W_MOD,this.checkModifiers);
        inst.setFeature(FD_IS_INCOMPATIBLE,0.0);
    
        setStrings(inst);
        setDiscourseEntity(inst);
        
        /**
         * Check wikipedia taxonomy for potential match
         *TODO: use taxonomy match for modifiers
         * get rid of taxonomy match here all together cuz its not checking any modifiers
         */
        boolean taxonomyMatch = 
                TAXONOMY_PAIRS_FULL.isa(
                    inst.getAnaphor().getMarkableString(), 
                    inst.getAntecedent().getMarkableString()
                )
            ||
                TAXONOMY_PAIRS_HEADS.isa(anaphoraString, antecedentString);
       /* if (taxonomyMatch)
        { 
           //  System.out.println("Taxonomy match: " + antecedentString + " <---> " + anaphoraString); 
           Wiki_featValue = 1.0;// inst.setFeature(FD_IS_INCOMPATIBLE,1.0);
           
      if(writeMat)
          simMatrix.write(dictionary.get(this.antecedentString) +  " " + dictionary.get(this.anaphoraString) + " 1\n" );
           
           if(!dictionary.containsKey(this.antecedentString))
              {      dictionary.put(this.antecedentString,dictSize);
              dictSize++;
              if(writeDic)dict.write(dictSize-1 +" " + this.antecedentString + "\n");
              
              }
             
           if(!dictionary.containsKey(this.anaphoraString))
              {   dictionary.put(this.anaphoraString,dictSize);
                
                  dictSize++;
                   if(writeDic)  dict.write(dictSize-1 +" " + this.anaphoraString + "\n");
                    
               
              }
        }
        else{
        
          */  
            
        
        
        //}
     
        if(this.checkModifiers) 
         {
            /**Check attributes against each other and check relations against each other*/
                modifierCompatibility(this.de_ana_attributes,this.de_ant_attributes, false);
                modifierCompatibility(this.de_ana_relations,this.de_ant_relations, true);
                
                compareAttributes(IncompatibilityUtil.getInstance().getStringList(this.de_ant_attributes,0),
                                  IncompatibilityUtil.getInstance().getStringList(this.de_ana_attributes,0),
                                  this.de_ant_attributes_pos,this.de_ana_attributes_pos  );
          }
        
    
     /*  Meaningfully combine Wiki and Wordnet result
      */ 
        
        double finalValue=0.0;
       if(this.Wiki_featValue == 0.0 || this.WN_featValue == 0.0) 
            finalValue =0.0;
       else if (this.Wiki_featValue  == 1.0 || this.WN_featValue == 1.0)
           finalValue = 1.0;
       else
            finalValue = 0.5;
      
        
       inst.setFeature(FD_IS_INCOMPATIBLE, finalValue );
       inst.setFeature(FD_IS_W_MOD,this.checkModifiers);
       
       
         if(writeDebug) {  Boolean isCor = inst.getAnaphor().isCoreferent(inst.getAntecedent());
                        _log.log(Level.INFO,"Wiki : " + this.Wiki_featValue + " WN : " + this.WN_featValue + " ");
                        _log.log(Level.INFO,"Cor " + isCor);
                        out.flush();}
      
       if(writeDic) dict.flush();
       if(writeMat)  simMatrix.flush();

        
       }
       
     
     
        /**
         * Get the discourse entities for each mention and its attributes and relations.
         * Get the pos for the prenominals, for postnominals its always N
         **/ 
       private void setDiscourseEntity(PairInstance inst)
       {
                 DiscourseEntity deAna = (inst.getAnaphor().getDiscourseEntity());
                 DiscourseEntity deAnt = (inst.getAntecedent().getDiscourseEntity());

                //postnominal pp and argument
                 this.de_ana_relations =  IncompatibilityUtil.getInstance().setModifiers_DE(deAna.getRelations(),(byte)3);
                 this.de_ant_relations =  IncompatibilityUtil.getInstance().setModifiers_DE(deAnt.getRelations(),(byte)3);
                 
                //prenominal adj & nn
                 this.de_ana_attributes = IncompatibilityUtil.getInstance().setModifiers_DE(deAna.getAttributes(),(byte)2);
                 this.de_ant_attributes = IncompatibilityUtil.getInstance().setModifiers_DE(deAnt.getAttributes(),(byte)2);
                
                 //pos for all attributes 
                 this.de_ana_attributes_pos = IncompatibilityUtil.getInstance().setModifiers_DE_POS(deAna.getAttributes());
                 this.de_ant_attributes_pos = IncompatibilityUtil.getInstance().setModifiers_DE_POS(deAnt.getAttributes());
       }
     
     
       
    
    /**
     * Check the pp that occur in the postmodifier. If they are the same, then we will continue checking 
     * the modifiers for compatibility, if not, then we will stop right here 
     */
  private boolean modifierSimilarity_PP()
    {
     /**Compare postmodifier pp in antecedent and anaphor*/
      if(writeDebug) _log.log(Level.INFO,"in pp");
      ArrayList<String> antA  = IncompatibilityUtil.getInstance().getStringList(this.de_ana_relations,1);
      ArrayList<String> anaA = IncompatibilityUtil.getInstance().getStringList(this.de_ant_relations,1);
      
      for(String antM : antA)
       {     
          for(String anaM : anaA)
            {
               if(writeDebug) _log.log(Level.INFO,"Comparing " + antM + " " + anaM + "in prep compare  "); 
              
               if(antM.equals(anaM))
               {   
                          if(writeDebug) _log.log(Level.INFO,"FOUND TWO MATCHING PP");
                       
                         return true;
               
               
               }
            }
       }
        
     return false;
   }
    

       
 
       
       /**
        * returns a compatibility measure from 0 (incompatible) to 1 (perfectly compatible)
        * Currently return values are either
        * 0 : top node in the middle of the path
        * 0.5:  no connection
        * 1: Hyponomy relation (top node at an end point of the chain
        */  
     
   private void modifierCompatibility(ArrayList<ArrayList<String>> anaList, ArrayList<ArrayList<String>> antList ,boolean relation)
    {
       /**if computing relations - check if pp is the same, if same then continue computing Wiki similarity*/
       boolean samePP =false;
     if(relation) samePP = modifierSimilarity_PP();
    
     if(samePP || !relation){   
                               
         if(writeDebug) _log.log(Level.INFO,"In modifierSimilarity + r:s" + relation + ":" + samePP);
       
          ArrayList<Double> results = new ArrayList<Double>();
          StringBuffer sb = new StringBuffer();
        
       ArrayList<String> anaAtt = IncompatibilityUtil.getInstance().getStringList(anaList,0);  
       ArrayList<String> antAtt = IncompatibilityUtil.getInstance().getStringList(antList,0);
       
        int anaMdSize = anaAtt.size();
        int antMdSize = antAtt.size();
        
        /**
         *one has modifiers while the other doesnt
         */
        if((anaMdSize > 0 && antMdSize ==0) || (antMdSize > 0 && anaMdSize ==0))
        {
            Wiki_featValue = 0.5;// inst.setFeature(FD_IS_INCOMPATIBLE,0.5);
            sb.append(" Assigning 0.5 - One with mod and one without ");
        }
        
        
        for(int i=0;i<anaMdSize;i++)
        {
                String antM = anaAtt.get(i);
                
            for(int j=0;j<antMdSize;j++)
                
         {
                String anaM = antAtt.get(j);
                        sb.append("Comparing in mod" + antM + " " + anaM + "\n");
              
               /**
                * Add the strings to the dictionary for lookup later
                */         
              if(!dictionary.containsKey(anaM))
              {
                  dictionary.put(anaM,dictSize);
                  dictSize++;
                  if(writeDic) dict.write(dictSize-1 +" " + anaM+ "\n");
              }
             
              if(!dictionary.containsKey(antM))
              {  
                  dictionary.put(antM,dictSize);
                  if(writeDic) dict.write(dictSize-1 +" " + antM+ "\n");
                  dictSize++;
              }
              
                        
               /*check string match
                *if a match, skip this
                */
                if(antM.equals(anaM))
                {
                    results.add(1.0); 
                    i++;
                    if(writeMat)  simMatrix.write(dictionary.get(antM) +  " " + dictionary.get(anaM) + " 1\n" );
                }
                 
              /**
              *check date match if we use CC tag
                          
                else if(antM.matches("[0-9]{4}") || anaM.matches("[0-9]{4}"))
                {
                    if(antM.equals(anaM))
                    {    results.add(1.0);
                         
                   if(writeMat)  simMatrix.write(dictionary.get(antM) +  " " + dictionary.get(anaM) + " 1\n" );
                    }
                    else
                    {
                     if(writeMat) simMatrix.write(dictionary.get(antM) +  " " + dictionary.get(anaM) + " 0\n" );
                        results.add(0.0);
                    }       
                }
                */
                        /** finally go to Wikipedia to calculate similarity
                         */
                else  {      
                        WikiSimilarity similarity =
                WikiSimilarityFactory.getInstance().getWikiSimilarity(antM, anaM);
            
                    //check whether there is any connection between the twoas
                    if(similarity != null)
                    {
       
                        if (similarity.getPaths() == null || similarity.getShortestPath() == null)
                               {
                               //     sb.append(" adding .5 \n");
                                  results.add(0.5);
                               //  if(writeMat)  simMatrix.write(dictionary.get(antM) +  " " + dictionary.get(anaM) + " 0.5\n" );
                               }
                   //go through paths to determine the location of the top node relative to the two query nodes
                    else {
                        try {

                                   WikiPath temp = similarity.getShortestPath();
                            //       sb.append("Getting temp " + temp.getTopNode() + "| " + similarity.getShortestPath().toString() + "|");
                             if (temp.isEmpty() || temp.getTopNode() == null)
                            { 
                          //   sb.append(" temp is emptty \n");
                             results.add(0.5);
                          //  if(writeMat)  simMatrix.write(dictionary.get(antM) +  " " + dictionary.get(anaM) + " 0.5\n" );
                             }
                            else
                            {
                                    boolean topNodePassed = false; 
                                    for (int node = 0; node < temp.size()-1; node++)
                                    {    
                                          sb.append(temp.get(node));

                                            if (temp.get(node).equals(temp.getTopNode()))
                                            { 
                                               topNodePassed = true; 
                                                             //top node in the middle of path
                                   //                 sb.append(" adding .0 \n"); 
                                               results.add(0.0);
                                           if(writeMat)     simMatrix.write(dictionary.get(antM) +  " " + dictionary.get(anaM) + " 0\n" );
                                            }

                                    }
                                     sb.append(temp.get(temp.size()-1)).toString();
                                  if(!topNodePassed )
                                  {                                  
                                      //top node is on outside node of path
                              //            sb.append(" adding 1 \n");
                                        results.add(1.0);
                                      if(writeMat)   simMatrix.write(dictionary.get(antM) +  " " + dictionary.get(anaM) + " 1\n" );
                                  }
                                  else{
                                      results.add(0.0);
                                   if(writeMat)    simMatrix.write(dictionary.get(antM) +  " " + dictionary.get(anaM) + " 0\n" );
                              //       sb.append("0 cuz path is 0 \n");

                                  }
                               }        
                         }
                        catch(Exception e){ e.printStackTrace();  }                    
                    }
                  }
                    
                    //since we couldnt compare assign a .5
                    else
                    {
                       results.add(0.5);
                    //  if(writeMat)  simMatrix.write(dictionary.get(antM) +  " " + dictionary.get(anaM) + " 0.5\n" );
                     //  sb.append("adding .5 in null \n");
                    
                    }
                
                
            }}    
         }
        
         double ret = IncompatibilityUtil.getInstance().calcCompatibility_MultipleModifiers(results);
           if(ret!= -1.0)
               Wiki_featValue = ret;
      
    if(writeDebug)_log.log(Level.INFO,sb.toString());
     } //end goOn
    
   }
    
    
    
    /** 
     * Sets the Wikipedia similarity measures 
     */
    private void setWikiSimilarity(PairInstance inst)
    {       
        // do not pull pronouns and do not count head matching
        if (
               inst.getAntecedent().getMarkableString().matches(PRONOUN)
            ||
                inst.getAnaphor().getMarkableString().matches(PRONOUN)
          ||
                WikiConstants.getInstance().getStopList(Language.EN).contains(this.antecedentString.toLowerCase())
            ||
                WikiConstants.getInstance().getStopList(Language.EN).contains(this.anaphoraString.toLowerCase())
        )
        { 
           this.similarity = null;
           Wiki_featValue = 0.5;//  
        }
        else
        {
            this.similarity =
                WikiSimilarityFactory.getInstance().getWikiSimilarity(antecedentString, anaphoraString);
     
        }
    }
 
    /**
     * Compare the attributes using Wordnet as the lexical source
     */
    private void compareAttributes(ArrayList<String> antMod, ArrayList<String> anaMod , List<Character> antPOS, List<Character> anaPOS)
    {
        /**
         * See if both have modifiers
         */
        if(writeDebug) _log.log(Level.INFO,"in compareAttributes");
        boolean DEBUG = true;
        
         if(antMod != null && anaMod != null)
         {
            for(String s1 : antMod)
            {
                computeSets(s1,antPOS.get(antMod.indexOf(s1)));     
            }
            for(String s2 : anaMod)
            {
                computeSets(s2,anaPOS.get(anaMod.indexOf(s2)));      
            }
            
            //actually compare the modifiers
           double ret = IncompatibilityUtil.getInstance().compareCompatWN(antMod,anaMod);
           if(ret!= -1.0)
               this.WN_featValue =ret;
            
         }
     
    }
    
    /**
     * Get word from wordnet and lookup 
     *  for Adj:
     *      Similar to, Antonym
     *  for Noun:
     *      ??
     *
     *  TODO:  do lookup that makes sense for Nouns
     */
    private void computeSets(String targetWord, Character posTag)
    {
        IndexWord word = null;
        boolean DEBUG_WN = true;        
        if(writeDebug) _log.log(Level.INFO,"Computing " + targetWord + " pos " + posTag);
    //    System.out.println("computing "+targetWord+ " pos " + posTag);
         List<String> container = new ArrayList<String>();
    
         List<String> Synset = new ArrayList<String>();
         List<String> Ant = new ArrayList<String>();
         List<String> mainSense = new ArrayList<String>();
         
         DictionaryDatabase wndict = WNInterface.getInstance().getDictionary();
	
        if(posTag.equals('A'))
             word = wndict.lookupIndexWord(POS.ADJ, targetWord);
        else if(posTag.equals('N'))
             word = wndict.lookupIndexWord(POS.NOUN, targetWord);  

                /** Check if word exists in Wordnet
                 */
          if(word != null){
                Synset[] senses = word.getSenses();


          //get first sense assumption             
        for(String tok : senses[0].getDescription().split(",\\s+"))
            mainSense.add(tok);

               
        /** Get similar set of words
         */
             PointerTarget[] pt = senses[0].getTargets(PointerType.SIMILAR_TO);


            //get all similar to
          if(DEBUG_WN){  _log.log(Level.INFO,"Sense: " + senses[0].getDescription());
                      for(String s : mainSense)
                         _log.log(Level.INFO,s);
                        _log.log(Level.INFO,"Synset :");}
             
            for(PointerTarget syn : pt){
               for(String tok : syn.getDescription().split(",\\s+"))
               {
                Synset.add(tok);
               }
            }

            if(DEBUG_WN) {for(String s : Synset)
                        _log.log(Level.INFO,s);

                       _log.log(Level.INFO,"Ant :");
                        }
            
            //get antonym
            PointerTarget[] AntParents = senses[0].getTargets(PointerType.ANTONYM);
            for(PointerTarget ant : AntParents){
                for(String tok : ant.getDescription().split(",\\s+"))  
                {
                  if(DEBUG_WN)  _log.log(Level.INFO,tok);
                     Ant.add(tok);
                }
            }
                }

           
            container.addAll(mainSense);
            container.addAll(Synset);
          System.out.println("sending "+ container.size() + " over");
            /*Add this word to the cache */
            IncompatibilityUtil.getInstance().cacheDB(container,Ant);
         }
    
    
    
    public static void main(String args[])
    {
         int i=0;
     /**check path in wordnet
      
          WNSimilarity sim = WNSimilarityFactory.getInstance().getWNSimilarity("dog","cat");
        System.out.println(WNSimilarityMeasure.PATH.toString());
      */
        
        /** Check hashmap for dictionary lookup table
      
       
        HashMap<String,Integer> att = new HashMap<String,Integer>();
        att.put("executive",i);
        if(att.containsKey("executive"))
        {
      //  System.out.println("contains");
        }
    */
        
        /**Check Wordnet synset lookup
         */ 
         
         List<String> Synset = new ArrayList<String>();
         List<String> Ant = new ArrayList<String>();
         List<String> mainSense = new ArrayList<String>();
         
		DictionaryDatabase dictionary = WNInterface.getInstance().getDictionary();
		IndexWord word = dictionary.lookupIndexWord(POS.NOUN, "children");

                /** Check if word exists in Wordnet
                 */
                if(word != null){
                Synset[] senses = word.getSenses();
		int taggedCount = word.getTaggedSenseCount();

  //get first sense assumption             
for(String tok : senses[0].getDescription().split(",\\s+"))
    mainSense.add(tok);


                
/** Get similar set of words
 */
     PointerTarget[] pt = senses[0].getTargets(PointerType.SIMILAR_TO);


//get all similar to
System.out.println("Sense: " + senses[0].getDescription());

for(String s : mainSense)
    System.out.println(s);

System.out.println("Synset :");
for(PointerTarget syn : pt){
   for(String tok : syn.getDescription().split(",\\s+"))
   {
       
    Synset.add(tok);
   }
}

for(String s : Synset)
    System.out.println(s);
//get antonym
System.out.println("Ant :");
PointerTarget[] AntParents = senses[0].getTargets(PointerType.ANTONYM);
for(PointerTarget ant : AntParents){
    for(String tok : ant.getDescription().split(",\\s+"))  
    {
        System.out.println(tok);
         Ant.add(tok);
    }
}
    }
                
            ArrayList<ArrayList<String>> t = new ArrayList<ArrayList<String>>();
            ArrayList<String> s1 = new ArrayList<String>();
               ArrayList<String> s2 = new ArrayList<String>();
               s1.add("1"); s1.add("2");
               s2.add("3");s2.add("4");
               t.add(s1);t.add(s2);
        //       getStringList(t,0);
    }
      
     
    /**
     *  Compute what we need to go on
     */
       
    /** Sets the Wikipedia strings to be parsed */
    private void setStrings(PairInstance inst)
    {
       /* this.antecedentString=inst.getAntecedent().getHeadOrName();
        this.anaphoraString=inst.getAnaphor().getHeadOrName();
        */
        
        String[] posTag = {"nnp","jj","cd","in"};
         
        this.antecedentString = COMPANY_PATTERN.
                    matcher(inst.getAntecedent().getHeadOrName()).replaceAll("");
        this.anaphoraString = COMPANY_PATTERN.
                    matcher(inst.getAnaphor().getHeadOrName()).replaceAll("");
        
       
        //check whether both are the same thing --> means we need to check modifiers
        if(this.antecedentString.equals(this.anaphoraString))
        {  
            
            this.checkModifiers = true;
            if(writeDebug) _log.log(Level.INFO,"SAME FOUND \n");

        }
                
            if(writeDebug){ StringBuffer buffer = new StringBuffer();
                        buffer.append("Ant + " + this.antecedentString + " " + inst.getAntecedent().getPremodifiers().toString() + " " + inst.getAntecedent().getPostmodifiers().toString()  +"\n");
                        buffer.append("Ana + " + this.anaphoraString + " " + inst.getAnaphor().getPremodifiers().toString() + " " + inst.getAnaphor().getPostmodifiers().toString() +"\n");
                        _log.log(Level.INFO,buffer.toString());
                            }
    }
    
   

}
// private void computeOnPath(PairInstance inst)
//         {
//
//
//           /**Add new words to dictionary*/
//             if(!dictionary.containsKey(this.antecedentString))
//                      {      dictionary.put(this.antecedentString,dictSize);
//                      dictSize++;
//                      if(writeDic) dict.write(dictSize-1 +" " + this.antecedentString + "\n");
//
//                      }
//
//              if(!dictionary.containsKey(this.anaphoraString))
//                      {   dictionary.put(this.anaphoraString,dictSize);
//
//                          dictSize++;
//                           if(writeDic)  dict.write(dictSize-1 +" " + this.anaphoraString + "\n");
//                      }
//
//   //set initial unknown incompatibility 
//          featValue = 0.5;// inst.setFeature(FD_IS_INCOMPATIBLE,0.5);
//  
//            
//           final StringBuffer buffer = new StringBuffer();
//
//           //check whether there is any connection between the two
//           if(similarity != null)
//           {     
//               if (similarity.getPaths() == null || similarity.getShortestPath() == null)
//                   {
//
//                     //no connection
//                featValue = 0.5;//     inst.setFeature(FD_IS_INCOMPATIBLE,0.5);
//                   
//                  
//                   }
//                   //go through paths to determine the location of the top node relative to the two query nodes
//               else {
//                    try {
//                               
//                               WikiPath temp = similarity.getShortestPath();
//
//                         if (temp.isEmpty())
//                        { }
//                        else
//                        {
//                                boolean topNodePassed = false;
//
//                                for (int node = 0; node < temp.size()-1; node++)
//                                { 
//                                      //  buffer.append(temp.get(node));
//                                        if (temp.get(node).equals(temp.getTopNode()))
//                                        {
//                                            topNodePassed = true; 
//                                                         //top node in the middle of path
//                                       featValue = 0.0;//   inst.setFeature(FD_IS_INCOMPATIBLE,0.0);
//                                          if(writeMat) simMatrix.write(dictionary.get(this.antecedentString) +  " " + dictionary.get(this.anaphoraString) + " 0\n" );
//                                          if(writeDebug)_log.log(Level.INFO,"Assingin 0 \n");
//                                        }
//                                        if (topNodePassed)
//                                            
//                                        {  
//             
//                                         //   System.out.println("assigning 0 to " + antecedentString + " " + anaphoraString );
//
//                                        }
//                                        
//                                }
//                              if(!topNodePassed)
//                              { //  System.out.println("assigning 1 " + antecedentString + " " + anaphoraString);
//                                  //since the mentions are in a possible compatible relationship check the pre/post modifiers to determine further compatibility
//                                    
//                                  //check the length of the path, if 0, two are the same thing and we should check modifiers
//                                  if(false)
//                                     this.checkModifiers = true;
//                                  
//                                  //top node is on outside node of path
//                                 featValue = 1.0;// inst.setFeature(FD_IS_INCOMPATIBLE,1.0);
//                                 if(writeMat)  simMatrix.write(dictionary.get(this.antecedentString) +  " " + dictionary.get(this.anaphoraString) + " 1\n" );
//                                 if(writeDebug) _log.log(Level.INFO,"Assigning 1 \n");
//                              }
//                          //      buffer.append(temp.get(temp.size()-1)).toString();
//                      //  System.out.println(buffer);
//                     
//                        }
//
//        
//           }
//                    catch(Exception e){}
//                       buffer.append("Post size " + post.size() + " pre size " + pre.size() + "\n");
//                      if(writeDebug)  _log.log(Level.INFO,buffer.toString());
//           }
//           
//       } 
//           
//     }