/*
 * FE_Wiki.java
 *
 * Created on July 17, 2007, 11:29 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package elkfed.coref.features.pairs.wiki;

import elkfed.config.ConfigProperties;
import elkfed.coref.features.pairs.FE_Alias;
import elkfed.coref.features.pairs.FE_StringMatch;
import elkfed.knowledge.WikiTaxonomy;
import java.math.BigDecimal;
import java.util.List;
import elkfed.coref.*;
import elkfed.ml.*;
import java.util.regex.Pattern;
import ponzo.nlp.wikipedia.WikipediaPageFactory;
import ponzo.nlp.wikipedia.similarity.LeacockChodorow;
import ponzo.nlp.wikipedia.similarity.WikiSimilarity;
import ponzo.nlp.wikipedia.similarity.WikiSimilarityFactory;

import static elkfed.lang.EnglishLinguisticConstants.*;

/**
 *Feature use to determine semantic relatedness of an instance based on Wikipedia entries. Sets average/shortest lengths for path, Wu-Palmer, Leacock-Chodorow,Resnik, Overlap.
 * @author vae2101
 */
public class FE_Wiki implements PairFeatureExtractor  {
    
    private final WikiTaxonomy taxonomyPairsFull;
    
    private final WikiTaxonomy taxonomyPairsHeads;
    
    /** The antecedent head */
    private String antecedentString;
    
    /** The anaphora head */
    private String anaphoraString;
    
    /** The similarity measures */
    private WikiSimilarity similarity;
    
     public static final FeatureDescription<Double> FD_IS_SHORT_PATH=
            new FeatureDescription<Double>(FeatureType.FT_SCALAR, "Shortest_PL");
     public static final FeatureDescription<Double> FD_IS_AVE_PATH=
            new FeatureDescription<Double>(FeatureType.FT_SCALAR, "Average_PL");
   
     public static final FeatureDescription<Double> FD_IS_SHORT_WUP=
            new FeatureDescription<Double>(FeatureType.FT_SCALAR, "Shortest_WUP");
     public static final FeatureDescription<Double> FD_IS_AVE_WUP=
            new FeatureDescription<Double>(FeatureType.FT_SCALAR, "Average_WUP");
     
     public static final FeatureDescription<Double> FD_IS_SHORT_LCH=
            new FeatureDescription<Double>(FeatureType.FT_SCALAR, "Shortest_LCH");
     public static final FeatureDescription<Double> FD_IS_AVE_LCH=
            new FeatureDescription<Double>(FeatureType.FT_SCALAR, "Average_LCH");
       
     public static final FeatureDescription<Double> FD_IS_SHORT_RESNIK=
            new FeatureDescription<Double>(FeatureType.FT_SCALAR, "Shortest_RES"); 
     public static final FeatureDescription<Double> FD_IS_AVE_RESNIK=
            new FeatureDescription<Double>(FeatureType.FT_SCALAR, "Average_RES"); 

     public static final FeatureDescription<Boolean> FD_ANTE_MISSING =
            new FeatureDescription<Boolean>(FeatureType.FT_BOOL, "Antecedent_missing_in_Wiki"); 

     public static final FeatureDescription<Boolean> FD_ANA_MISSING =
            new FeatureDescription<Boolean>(FeatureType.FT_BOOL, "Anaphora_missing_in_Wiki");

     public static final FeatureDescription<Boolean> FD_SAME_QUERY =
            new FeatureDescription<Boolean>(FeatureType.FT_BOOL, "Same_query_in_Wiki");

     public static final FeatureDescription<Boolean> FD_DIRECT_ISA =
            new FeatureDescription<Boolean>(FeatureType.FT_BOOL, "Direct_isa_Wiki_category_pair");     
     
     private static final Pattern COMPANY_PATTERN =
            Pattern.compile("\\s+"+COMPANY_DESIGNATOR, Pattern.CASE_INSENSITIVE);
     
    /** Creates a new instance of FE_Wiki */
    public FE_Wiki() {
        this.taxonomyPairsFull = 
                new WikiTaxonomy(ConfigProperties.getInstance().getTaxonomyPairsFull());
        this.taxonomyPairsHeads =
                new WikiTaxonomy(ConfigProperties.getInstance().getTaxonomyPairsHeads());
        // taxonomyPairsHeads.doWordNetExpansion();
        setWikiSimilarityOptions();
    }
    
    private void setWikiSimilarityOptions()
    {
        WikiSimilarityFactory.SIMILARITY = true;
        WikiSimilarityFactory.PAGERANK_FILTERING = true;
        
        WikipediaPageFactory.getInstance().setCaching(true);
        WikiSimilarityFactory.getInstance().setCaching(true);
    	WikiSimilarityFactory.getInstance().setComputeGlossOverlapMeasure(false);
    	WikiSimilarityFactory.getInstance().setComputeTextOverlapMeasure(false);
    }
    
    public void describeFeatures(List<FeatureDescription> fds) {
        
        fds.add(FD_IS_SHORT_PATH);
        fds.add(FD_IS_AVE_PATH);
        
        fds.add(FD_IS_SHORT_WUP);
        fds.add(FD_IS_AVE_WUP);
          
        fds.add(FD_IS_SHORT_LCH);
        fds.add(FD_IS_AVE_LCH);
        
        fds.add(FD_IS_SHORT_RESNIK);
        fds.add(FD_IS_AVE_RESNIK);
         
        fds.add(FD_ANTE_MISSING);
        fds.add(FD_ANA_MISSING);
        
        fds.add(FD_SAME_QUERY);
        fds.add(FD_DIRECT_ISA);
    }

    public void extractFeatures(PairInstance inst) {
                 
        // by deafult we have everything in Wikipedia :-)
        inst.setFeature(FD_ANTE_MISSING, false);
        inst.setFeature(FD_ANA_MISSING, false);
        
        // by default we query different things
        inst.setFeature(FD_SAME_QUERY, false);
        
        boolean taxonomyMatch =
                taxonomyPairsFull.isa(
                    inst.getAnaphor().getMarkableString(), 
                    inst.getAntecedent().getMarkableString()
                )
            ||
                taxonomyPairsHeads.isa(anaphoraString, antecedentString);
        inst.setFeature(FD_DIRECT_ISA, taxonomyMatch);
        
        setStrings(inst);
        boolean toSet = setWikiSimilarity(inst);
        
        if (toSet)
        { setSimilarityScores(inst); }
    }

    /** Sets the Wikipedia strings to be parsed */
    private void setStrings(PairInstance inst)
    {
        this.antecedentString = 
                COMPANY_PATTERN.
                    matcher(inst.getAntecedent().getHeadOrName()).replaceAll("");
        this.anaphoraString = 
                COMPANY_PATTERN.
                    matcher(inst.getAnaphor().getHeadOrName()).replaceAll("");
    }
    
    /** Sets the Wikipedia similarity measures */
    private boolean setWikiSimilarity(PairInstance inst)
    {   
        // 1. Do not compute if 
        //  a. string matching
        //  b. same query
        if (
                inst.getFeature(FE_StringMatch.FD_IS_STRINGMATCH)
            ||
                antecedentString.equalsIgnoreCase(anaphoraString)
        ) 
        { 
            setMaxSimilarity(inst);
            setIsSameQuery(inst);
            return false;
        }
        //  c. alias matching
        else if (inst.getFeature(FE_Alias.FD_IS_ALIAS))
        { 
            setMaxSimilarity(inst);
            return false;
        }
        
        boolean stillToCompute = true;
        // d. pronouns are not computed either, except US
        if (
                inst.getAntecedent().getMarkableString().toLowerCase().matches(PRONOUN)
              &&
                //  silly exception...
                !inst.getAntecedent().getMarkableString().matches("US")
        )
        { setIsAntecendMissing(inst); stillToCompute = false; }
        if (
                // matches a pronoun ... BUT
                inst.getAnaphor().getMarkableString().toLowerCase().matches(PRONOUN)
              &&
                //  silly exception...
                !inst.getAnaphor().getMarkableString().matches("US")
        )
        { setIsAnaphoraMissing(inst); stillToCompute = false; }
        
        // compute!
        if (stillToCompute)
        {   
            this.similarity =
                WikiSimilarityFactory.getInstance().getWikiSimilarity(antecedentString, anaphoraString);            
            return checkIsMissing(inst, similarity);            
        }
        else
        { this.similarity = null; }
        
        return true;
    }
    
    /** Checks and enforses that BOTH entities are in Wikipedia */
    private boolean checkIsMissing(PairInstance inst, WikiSimilarity similarity)
    {
        boolean newQuery = false;
        String fallback1 = antecedentString;
        String fallback2 = anaphoraString;

        if (similarity.getPage1().isEmpty())
        {
            setIsAntecendMissing(inst);
            if (inst.getAntecedent().isEnamex())
            { fallback1 = inst.getAntecedent().getEnamexType(); newQuery = true; }
        }
        
        if (similarity.getPage2().isEmpty())
        {
            setIsAnaphoraMissing(inst);
            if (inst.getAnaphor().isEnamex())
            { fallback2 = inst.getAnaphor().getEnamexType(); newQuery = true; }
        }
        
        if (newQuery)
        {
            if (fallback1.equalsIgnoreCase(fallback2))
            {
                setMaxSimilarity(inst);
                setIsSameQuery(inst);
                return false;
            }
            else
            {            
                this.similarity =
                    WikiSimilarityFactory.getInstance().getWikiSimilarity(fallback1, fallback2);
            }
        }
        return true;
    }
    
    /** Sets the similarity measures */
    private void setSimilarityScores(PairInstance inst)
    {
        setPath(inst); setWuPalmer(inst); setLeacockChodorow(inst);
        setResnik(inst); // setOverlap(inst); setContain(inst);
    }
    
    /** Sets the path similarity measure */
    private void setPath(PairInstance inst)
    {
        if (similarity == null || similarity.getShortestPathMeasure() == -1)
        { inst.setFeature(FD_IS_SHORT_PATH, 0.0); }
        else
        { inst.setFeature(FD_IS_SHORT_PATH, round(similarity.getShortestPathMeasure())); }
        if (similarity == null || similarity.getAveragePathMeasure() == -1)
        { inst.setFeature(FD_IS_AVE_PATH, 0.0); }
        else
        { inst.setFeature(FD_IS_AVE_PATH, round(similarity.getAveragePathMeasure())); }
    }

    /** Sets the Wu and Palmer similarity measure */
    private void setWuPalmer(PairInstance inst)
    {
        if (similarity == null || similarity.getShortestWuPalmerMeasure() == -1)
        { inst.setFeature(FD_IS_SHORT_WUP, 0.0); }
        else
        { inst.setFeature(FD_IS_SHORT_WUP, round(similarity.getShortestWuPalmerMeasure())); }
        if (similarity == null || similarity.getAverageWuPalmerMeasure() == -1)
        { inst.setFeature(FD_IS_AVE_WUP, 0.0); }
        else
        { inst.setFeature(FD_IS_AVE_WUP, round(similarity.getAverageWuPalmerMeasure())); }
    }
    
    /** Sets the Leacock and Chodorow similarity measure */
    private void setLeacockChodorow(PairInstance inst)
    {
        if (similarity == null || similarity.getShortestLeacockChodorowMeasure() == -1)
        { inst.setFeature(FD_IS_SHORT_LCH, 0.0); }
        else
        { inst.setFeature(FD_IS_SHORT_LCH, round(LeacockChodorow.normalize(similarity.getShortestLeacockChodorowMeasure()))); }
        if (similarity == null || similarity.getAverageLeacockChodorowMeasure() == -1)
        { inst.setFeature(FD_IS_AVE_LCH, 0.0); }
        else
        { inst.setFeature(FD_IS_AVE_LCH, round(LeacockChodorow.normalize(similarity.getAverageLeacockChodorowMeasure()))); }
    }
    
    /** Sets the Resnik similarity measure */
    private void setResnik(PairInstance inst)
    {
        if (similarity == null || similarity.getBestResnik() == -1)
        { inst.setFeature(FD_IS_SHORT_RESNIK, 0.0); }
        else
        { inst.setFeature(FD_IS_SHORT_RESNIK, round(similarity.getBestResnik())); }
        if (similarity == null || similarity.getAverageResnik() == -1)
        { inst.setFeature(FD_IS_AVE_RESNIK, 0.0); }
        else
        { inst.setFeature(FD_IS_AVE_RESNIK, round(similarity.getAverageResnik())); }
    }

    /** Signals that we have maximum similarity */
    private void setMaxSimilarity(PairInstance inst)
    { 
        inst.setFeature(FD_IS_SHORT_PATH, 1.0);
        inst.setFeature(FD_IS_AVE_PATH, 1.0);

        inst.setFeature(FD_IS_SHORT_WUP, 1.0);
        inst.setFeature(FD_IS_AVE_WUP, 1.0);
        
        inst.setFeature(FD_IS_SHORT_LCH, 1.0);
        inst.setFeature(FD_IS_AVE_LCH, 1.0);
        
        inst.setFeature(FD_IS_SHORT_RESNIK, 1.0);
        inst.setFeature(FD_IS_AVE_RESNIK, 1.0);
    }
    
    /** Signals that the antecedent could not be found in Wikipedia */
    private void setIsAntecendMissing(PairInstance inst)
    { inst.setFeature(FD_ANTE_MISSING, true); }

    /** Signals that the anaphora could not be found in Wikipedia */
    private void setIsAnaphoraMissing(PairInstance inst)
    { inst.setFeature(FD_ANA_MISSING, true); }

    /** Signals that we are making the same query in Wikipedia */
    private void setIsSameQuery(PairInstance inst)
    { inst.setFeature(FD_SAME_QUERY, true); }
    
    public static double round(double d)
    { return new BigDecimal(d).setScale(3, BigDecimal.ROUND_UP).doubleValue(); }
}
