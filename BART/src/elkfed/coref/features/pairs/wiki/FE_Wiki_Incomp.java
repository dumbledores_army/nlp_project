/*
 * FE_Wiki_Incomp.java
 *
 * Created on August 7, 2007, 4:25 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package elkfed.coref.features.pairs.wiki;


import java.util.List;
import elkfed.coref.*;
import elkfed.ml.*;
import static elkfed.lang.EnglishLinguisticConstants.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import ponzo.nlp.wikipedia.WikipediaPageFactory;
//import ponzo.nlp.wikipedia.lang.Language;
import ponzo.nlp.wikipedia.path.WikiPath;
import ponzo.nlp.wikipedia.similarity.WikiSimilarity;
import ponzo.nlp.wikipedia.similarity.WikiSimilarityFactory;

/**
 *
 * @author vae2101
 */
public class FE_Wiki_Incomp implements PairFeatureExtractor {
    
   
    private String antecPostModifier;
    private String antecPreModifier;
    private String anaphorPreModifier;
    private String anaphorPostModifier;
    private double incompatibleMeasure;
    private boolean checkModifiers = false;    
    
    private ArrayList<String> antPostModNNP = new ArrayList<String>();
    private ArrayList<String> anaPostModNNP = new ArrayList<String>();
    
    private ArrayList<String> antPostModJJ = new ArrayList<String>();
    private ArrayList<String> anaPostModJJ = new ArrayList<String>();
    
    private ArrayList<String> antPreModNNP = new ArrayList<String>();
    private ArrayList<String> anaPreModNNP = new ArrayList<String>();
    
    private ArrayList<String> antPreModJJ = new ArrayList<String>();
    private ArrayList<String> anaPreModJJ = new ArrayList<String>();
    
    private ArrayList<String> antPreModCD = new ArrayList<String>();
    private ArrayList<String> antPostModCD = new ArrayList<String>();
    
    private ArrayList<String> anaPreModCD = new ArrayList<String>();
    private ArrayList<String> anaPostModCD = new ArrayList<String>();
    
    ArrayList<String> post = new ArrayList<String>();
    ArrayList<String> pre = new ArrayList<String>();
    
    /** The antecedent head */
    private String antecedentString;
    
    
    /** The anaphora head */
    private String anaphoraString;
    
    /** The similarity measures */
    private WikiSimilarity similarity;
    
   private boolean DEBUG = false;
        private PrintWriter out = null;   
    /** Creates a new instance of FE_Wiki_Incomp */
    public FE_Wiki_Incomp() throws IOException{
    
        if(DEBUG)
        {
         PrintWriter out= new PrintWriter(new BufferedWriter(new FileWriter("/home/ws07elerfed/vae2101/runOut816_ACE_v4")));
        }
        
//WikiSimilarityFactory.getInstance().
  WikiSimilarityFactory.getInstance().setCaching(true);
  WikipediaPageFactory.getInstance().setCaching(true);
  
    }
    
     public static final FeatureDescription<Double> FD_IS_INCOMPATIBLE=
            new FeatureDescription<Double>(FeatureType.FT_SCALAR, "Semantic_Incompatibility_Measure ");
     
       public void describeFeatures(List<FeatureDescription> fds) {
        fds.add(FD_IS_INCOMPATIBLE);
       }
       
         
       public void extractFeatures(PairInstance inst) {
        
           
           this.checkModifiers = false;
           post.clear();
        pre.clear();
        if(DEBUG)out.write("\n ||||||_____|||| \n");
        setStrings(inst);
        setWikiSimilarity(inst);
        computeOnPath(inst);
       Boolean isCor = inst.getAnaphor().isCoreferent(inst.getAntecedent());
       if(DEBUG)out.write("Cor " + isCor);
        }
       
       
       /**
        * returns a compatibility measure from 0 (incompatible) to 1 (perfectly compatible)
        * Currently return values are either
        * 0 : top node in the middle of the path
        * 0.5:  no connection
        * 1: Hyponomy relation (top node at an end point of the chain
        */  
       private void computeOnPath(PairInstance inst)
         {

           
           inst.setFeature(FD_IS_INCOMPATIBLE,0.5);
           
            
           final StringBuffer buffer = new StringBuffer();

           //check whether there is any connection between the two
           if(similarity != null)
           {     
               if (similarity.getPaths() == null || similarity.getShortestPath() == null)
                   {

                     //no connection
                     inst.setFeature(FD_IS_INCOMPATIBLE,0.5);
                   }
                   //go through paths to determine the location of the top node relative to the two query nodes
               else {
                    try {
                               
                               WikiPath temp = similarity.getShortestPath();

                         if (temp.isEmpty())
                        { }
                        else
                        {
                                boolean topNodePassed = false;

                                for (int node = 0; node < temp.size()-1; node++)
                                { 
                                      //  buffer.append(temp.get(node));
                                        if (temp.get(node).equals(temp.getTopNode()))
                                        { topNodePassed = true; 
                          
                                          //top node in the middle of path
                                          inst.setFeature(FD_IS_INCOMPATIBLE,0.0);
                                         if(DEBUG)out.write("Assingin 0 \n");
                                        }
                                        if (topNodePassed)
                                            
                                        {  
                                            //buffer.append
                                         //   System.out.println("assigning 0 to " + antecedentString + " " + anaphoraString );

                                        }
                                        
                                }
                              if(!topNodePassed)
                              { //  System.out.println("assigning 1 " + antecedentString + " " + anaphoraString);
                                  //since the mentions are in a possible compatible relationship check the pre/post modifiers to determine further compatibility
                                    
                                  //check the length of the path, if 0, two are the same thing and we should check modifiers
                                  if(false)
                                     this.checkModifiers = true;
                                  
                                  //top node is on outside node of path
                                  inst.setFeature(FD_IS_INCOMPATIBLE,1.0);
                               if(DEBUG)  out.write("Assigning 1 \n");
                              }
                          //      buffer.append(temp.get(temp.size()-1)).toString();
                      //  System.out.println(buffer);
                     
                        }

        
           }
                    catch(Exception e){}
                       buffer.append("Post size " + post.size() + " pre size " + pre.size() + "\n");
                       if(DEBUG)out.write(buffer.toString());
           }
           
       } 
           
              if(this.checkModifiers) 
              {
                modifierSimilarity(inst);
              }
         
         }
        /** Sets the Wikipedia strings to be parsed */
    private void setStrings(PairInstance inst)
    {
        this.antecedentString=inst.getAntecedent().getHeadOrName();
        this.anaphoraString=inst.getAnaphor().getHeadOrName();
        
        //check whether both are the same thing --> means we need to check modifiers
        if(this.antecedentString.equals(this.anaphoraString))
        {  this.checkModifiers = true;
        
        if(DEBUG)out.write("SAME SPELLING FOUND \n");
      
        }
        
       if(inst.getAntecedent().getPostmodifiers() != null) {this.antecPostModifier = inst.getAntecedent().getPostmodifiers().toString();
       
        this.antPostModNNP = setModifiers(this.antecPostModifier,"nnp",true);
        this.antPostModJJ = setModifiers(this.antecPostModifier,"jj",true);
         this.antPostModCD = setModifiers(this.antecPostModifier,"cd",true);
       }
       if(inst.getAntecedent().getPremodifiers() != null){ this.antecPreModifier = inst.getAntecedent().getPremodifiers().toString();
       this.antPreModNNP = setModifiers(this.antecPreModifier,"nnp",false);
       this.antPreModJJ = setModifiers(this.antecPreModifier,"jj",false);
        this.antPreModCD = setModifiers(this.antecPreModifier,"cd",false);
        
       }
        
       if(inst.getAnaphor().getPostmodifiers() != null){this.anaphorPostModifier = inst.getAnaphor().getPostmodifiers().toString();
       this.anaPostModNNP =  setModifiers(this.anaphorPostModifier,"nnp",true);   
       this.anaPostModJJ =  setModifiers(this.anaphorPostModifier,"jj",true);
           this.anaPostModCD =  setModifiers(this.anaphorPostModifier,"cd",true); 
          }
        if(inst.getAnaphor().getPremodifiers() != null){this.anaphorPreModifier = inst.getAnaphor().getPremodifiers().toString();
       
       this.anaPreModNNP =  setModifiers(this.anaphorPreModifier,"nnp",false);
       this.anaPreModJJ = setModifiers(this.anaphorPreModifier,"jj",false);
        this.anaPreModCD = setModifiers(this.anaphorPreModifier,"cd",false);
    }
      StringBuffer buffer = new StringBuffer();
        buffer.append("Ant + " + this.antecedentString + " " + this.antecPreModifier + " " + this.antecPostModifier + this.antPostModNNP.size() + " " + this.antPreModNNP.size() +"\n");
        buffer.append("Ana + " + this.anaphoraString + " " + this.anaphorPreModifier + " " + this.anaphorPostModifier +"\n");
        if(DEBUG)out.write(buffer.toString());
    }
    
    
    
    private void modifierSimilarity(PairInstance inst)
    {
       
        
    //add cd stuff
        
        ArrayList<String> anaMd = new ArrayList<String>();
        ArrayList<String> antMd = new ArrayList<String>();
        
        antMd.addAll(this.antPostModJJ);
        antMd.addAll(this.antPreModJJ);
        antMd.addAll(this.antPostModNNP);
        antMd.addAll(this.antPreModNNP);
        
        anaMd.addAll(this.anaPostModJJ);
        anaMd.addAll(this.anaPreModJJ);
        anaMd.addAll(this.anaPostModNNP);
        anaMd.addAll(this.anaPreModNNP);
        
         if(DEBUG)out.write("In modifierSimilarity +\n");
        ArrayList<Double> results = new ArrayList<Double>();
      //  Iterator<String> i = post.iterator();
      //  Iterator<String> j = pre.iterator();
        StringBuffer sb = new StringBuffer();
        
        int anaMdSize = anaMd.size();
        int antMdSize = antMd.size();
        
        /**
         *one has modifiers while the other doesnt
         */
        if((anaMdSize > 0 && antMdSize ==0) || (antMdSize > 0 && anaMdSize ==0))
        {
            inst.setFeature(FD_IS_INCOMPATIBLE,0.5);
            sb.append(" Assigning 0.5 - One with mod and one without ");
        }
        
        for(int i=0;i<anaMd.size();i++)
        {
                String antM = anaMd.get(i);
                
            for(int j=0;j<antMd.size();j++)
                
         {
                String anaM = antMd.get(j);
                        sb.append("Comparing " + antM + " " + anaM + "\n");
                        
               /*check string match
                *
                */
                if(antM.equals(anaM))
                {
                results.add(1.0);
                i++;
                }
                      /*
                       *check date match
                       */  
                   
                else if(antM.matches("[0-9]{4}") || anaM.matches("[0-9]{4}"))
                {
                    if(antM.equals(anaM))
                        results.add(1.0);
                    else
                        results.add(0.0);
                            
                }
          
                else  {      
                        WikiSimilarity similarity =
                WikiSimilarityFactory.getInstance().getWikiSimilarity(antM, anaM);
            
                    //check whether there is any connection between the twoas
                    if(similarity != null)
                    {
        //    sb.append("goin in to smiiliary != null");
            
                    if (similarity.getPaths() == null || similarity.getShortestPath() == null)
                           {
                           //     sb.append(" adding .5 \n");
                              results.add(0.5);
                           }
                   //go through paths to determine the location of the top node relative to the two query nodes
                    else {
                        try {
                               
                               WikiPath temp = similarity.getShortestPath();
                        //       sb.append("Getting temp " + temp.getTopNode() + "| " + similarity.getShortestPath().toString() + "|");
                         if (temp.isEmpty() || temp.getTopNode() == null)
                        { 
                      //   sb.append(" temp is emptty \n");
                         results.add(0.5);
                         }
                        else
                        {
                                boolean topNodePassed = false; 
                                for (int node = 0; node < temp.size()-1; node++)
                                {    
                                      sb.append(temp.get(node));
                                      
                                        if (temp.get(node).equals(temp.getTopNode()))
                                        { 
                                           topNodePassed = true; 
                                                         //top node in the middle of path
                               //                 sb.append(" adding .0 \n"); 
                                           results.add(0.0);
                                        }
                                                                             
                                }
                                 sb.append(temp.get(temp.size()-1)).toString();
                              if(!topNodePassed )
                              {                                  
                                  //top node is on outside node of path
                          //            sb.append(" adding 1 \n");
                                    results.add(1.0);
                              }
                              else{
                                  results.add(0.0);
                          //       sb.append("0 cuz path is 0 \n");
                              }

                        }

        
           }
                    catch(Exception e){
                    
                   // sb.append("went to exception");
                    e.printStackTrace();
                    }
                    
           }
         }
                    //since similary couldnt be compared assign a .5
                    else
                    {
                       results.add(0.5);
                     //  sb.append("adding .5 in null \n");
                    
                    }
                
                
            }}    
         }
        
       boolean allZero = true, 
               noOnes = true,
               oneZero = false;
        Iterator<Double> y  = results.iterator(); 
       while(y.hasNext())
       {
         Double temp = y.next();
            if(temp == 1.0)
         {   allZero = false;
              noOnes = false;
         }   
            else if (temp == 0.5)
            {
            allZero = false;
            
            
            }
            else if (temp == 0.0)
                oneZero = true;
         
       }
        if(results.size() == 0){     sb.append("nothing compared\n");} //if no modifiers keep what we already assigned
        else{     
                if(allZero || oneZero){
                inst.setFeature(FD_IS_INCOMPATIBLE,0.0); sb.append("Assingin 0\n");
                }
                else if (noOnes && !allZero){
                 inst.setFeature(FD_IS_INCOMPATIBLE,0.5);sb.append("Assingin 0.5\n");
                }
                else if(!noOnes){
                    inst.setFeature(FD_IS_INCOMPATIBLE,1.0);sb.append("Assingin 1\n");
                }
        }
   if(DEBUG)out.write(sb.toString());
    }
    
    
    private ArrayList<String> setModifiers(String mod, String type, Boolean postPre)
    {
        StringBuffer buff = new StringBuffer();
        ArrayList<String> modBuffer = new ArrayList<String>();

     //  buff.append("Starting " + type);
        boolean nnp = false;
        for(String tok : mod.replaceAll("\\)", ") ").split("\\s+")) 
        {
                if ((tok.matches(".+\\)")) && nnp)
                {
                    tok=tok.replaceAll("\\)", "");
                    // tok is an nnp
                    
                    //add check for date
                    if(type.equals("cd"))
                    {
                            if(tok.matches("[0-9]{4}"))
                            {
                            modBuffer.add(tok);
                           buff.append(type + " " + tok + "\n");
                            }
                    }   
                    
                    else
                    {
                    modBuffer.add(tok);
                   buff.append(type + " " + tok + "\n");
                    }
                }
                //either nnp or jj
                 if (tok.matches("\\(" + type) || tok.matches("\\[\\(" + type))
                {
                    nnp = true;
                }
                else
                {
                    nnp = false;
                }
        }    
      if(modBuffer != null)
      {
        if(postPre)
            post.addAll(modBuffer);
        else
            pre.addAll(modBuffer);
      }
     //   System.out.println(buff.toString());
        return modBuffer;
    }
    
    /** Sets the Wikipedia similarity measures */
    private void setWikiSimilarity(PairInstance inst)
    {       
        // do not pull pronouns and do not count head matching
        if (
               inst.getAntecedent().getMarkableString().matches(PRONOUN)
            ||
                inst.getAnaphor().getMarkableString().matches(PRONOUN)
        //    ||
         //       WikiConstants.getInstance().getStopList(Language.EN).contains(antecedentString.toLowerCase())
           // ||
          //      WikiConstants.getInstance().getStopList(Language.EN).contains(anaphoraString.toLowerCase())
        )
        { this.similarity = null;
        inst.setFeature(FD_IS_INCOMPATIBLE,.5);
        }
        else
        {
            this.similarity =
                WikiSimilarityFactory.getInstance().getWikiSimilarity(antecedentString, anaphoraString);
     
        }
    }
    
}
