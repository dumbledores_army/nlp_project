/*
 * FE_SynPos.java
 *
 * Created on August 6, 2007, 6:46 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package elkfed.coref.features.pairs;

import elkfed.coref.PairFeatureExtractor;
import elkfed.coref.PairInstance;
import elkfed.ml.FeatureDescription;
import elkfed.ml.FeatureType;
import java.util.List;

/** determines the root path of the antecedent.
 * this should approximate syntax-based salience
 * if we give it enough data...
 */
public class FE_SynPos implements PairFeatureExtractor {
    public static final FeatureDescription<String> FD_ANTE_SYN_POS=
            new FeatureDescription<String>(FeatureType.FT_STRING,"Ante_SyntaxPath");
    public void describeFeatures(List<FeatureDescription> fds) {
        fds.add(FD_ANTE_SYN_POS);
    }

    private static int nthIndex(String path,String sep,int n)
    {
        int idx=0;
        for (int i=0;i<n;i++)
        {
            idx=path.indexOf(sep,idx)+1;
            if (idx==0) return -1;
        }
        return idx;
    }
    
    public void extractFeatures(PairInstance inst) {
        String rootPath=inst.getAntecedent().getRootPath();
        int off=nthIndex(rootPath,".",3);
        if (off == -1)
        {
            inst.setFeature(FD_ANTE_SYN_POS, rootPath);
        }
        else
        {
            inst.setFeature(FD_ANTE_SYN_POS, rootPath.substring(0,off));
        }
    }
    
}
