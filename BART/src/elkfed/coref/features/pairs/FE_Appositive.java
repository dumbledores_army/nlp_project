/*
 * FE_Appositive.java
 *
 * Created on July 11, 2007, 6:12 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package elkfed.coref.features.pairs;

import elkfed.coref.mentions.Mention;
import java.util.List;
import elkfed.mmax.minidisc.Markable;
import elkfed.mmax.minidisc.MiniDiscourse;

import elkfed.ml.*;
import elkfed.coref.*;
import elkfed.config.ConfigProperties;

import static elkfed.lang.EnglishLinguisticConstants.*;
import static elkfed.mmax.MarkableLevels.DEFAULT_POS_LEVEL;
import static elkfed.mmax.pipeline.MarkableCreator.LABEL_ATTRIBUTE;
import static elkfed.mmax.pipeline.MarkableCreator.SENTENCE_ID_ATTRIBUTE;

/**
 * Feature used to determine whether markble is a possible 
 * appositive construct of another markable
 * 
 * @author vae2101
 */
public class FE_Appositive implements PairFeatureExtractor {
    
    public static final FeatureDescription<Boolean> FD_IS_APPOSITIVE=
        new FeatureDescription<Boolean>(FeatureType.FT_BOOL, "Appositive");
    
    public void describeFeatures(List<FeatureDescription> fds) {
        fds.add(FD_IS_APPOSITIVE);        
    }

    public void extractFeatures(PairInstance inst) {
        inst.setFeature(FD_IS_APPOSITIVE,getAppositive(inst));
    }
        
    public static boolean getAppositive(PairInstance inst)
    {
        // for a pair to be an apposition 
        if (
                // *trick to speed up* right at the beginning: they must be
                // in the same sentence...
                sameSentence(inst)
            &&  
                // the anaphora must NOT contain a verb (we check PoS)
                !containsVerb(inst.getAnaphor())
            &&
                // antecedent and anaphora must be only comma separated
                areCommaSeparated(inst)
            &&
                // one of the two must be a proper name
                atLeastOneProperName(inst)
            &&
                // anaphora must be a noun phrase in any case
                isNounPhrase(inst.getAnaphor().getMarkable())
        )
        { 
            // in MUC-6 the anaphor must be a definite for the
            // expression to be an apposition
            if (
                    ConfigProperties.getInstance().getTrainingDataId().
                        equals(ConfigProperties.MUC6_ID)
                ||
                    ConfigProperties.getInstance().getTestDataId().
                        equals(ConfigProperties.MUC6_ID)
            )
            { 
                return  
                        inst.getAnaphor().getMarkableString().split(" ")[0].matches(DEF_ARTICLE)
                     ||
                        startsWithNoun(inst.getAnaphor().getMarkable());
            }
            else
            {
                // not MUC-6
                return  
                        inst.getAnaphor().getMarkableString().split(" ")[0].matches(DEF_ARTICLE)
                     ||
                        inst.getAnaphor().getMarkableString().split(" ")[0].matches(INDEF_ARTICLE)
                ;
            }
        }
        else
        { return false; }   
    }
    
    /** Checks wether the antecedent and anaphora are in the same sentence */
    public static boolean sameSentence(PairInstance inst)
    {
        final int sent1 = inst.getAnaphor().getSentId();
        final int sent2 = inst.getAntecedent().getSentId();
        return (sent1 == sent2); 
    }
    
    /** Checks whether the markable contains a verb */
    private static boolean containsVerb(final Mention mention)
    {
        Markable markable = mention.getMarkable();
        for (String pos : markable.getAttributeValue(DEFAULT_POS_LEVEL).split(" "))
        {
            if (pos.startsWith("v"))
            { return true; }
        }
        return false;
    }
    
    /** Checks whether the instance has at least one proper-name */
    private static boolean atLeastOneProperName(PairInstance inst)
    {
        return (
              inst.getAntecedent().getProperName() // IsProperNameFE.get().isProperName(instance.getAntecedent())
            ||
              inst.getAnaphor().getProperName()  // IsProperNameFE.get().isProperName(instance.getAnaphora())
        );
    }
    
    /** Checks whether the antecedent and anaphor are only separated by a comma */
    protected static boolean areCommaSeparated(PairInstance inst)
    {   
        final StringBuffer textBuffer = new StringBuffer();
        final MiniDiscourse doc=inst.getAnaphor().getDocument();
        textBuffer.setLength(0);

        for (int token = inst.getAntecedent().getMarkable().getRightmostDiscoursePosition()+1;
             token < inst.getAnaphor().getMarkable().getLeftmostDiscoursePosition();
             token++)
        {
            textBuffer.append(" ").
                append(doc.getDiscourseElementAtDiscoursePosition(token).toString());
        }
        if (textBuffer.length() > 0)
        {
            if (textBuffer.deleteCharAt(0).toString().replaceAll("\\s", "").equals(","))
            { return true; }
            else
            { return false; }
        }
        else
        { return false; }
    }
    
    /** Checks whether the markable is a noun phrase */
    private static boolean isNounPhrase(Markable markable)
    { return markable.getAttributeValue(LABEL_ATTRIBUTE).equals("np"); }

    
    /** Checks whether the markable starts with a noun */
    private static boolean startsWithNoun(Markable markable)
    { 
        return 
                markable.getAttributeValue(DEFAULT_POS_LEVEL).startsWith("nn")
              ||
                markable.getAttributeValue(DEFAULT_POS_LEVEL).startsWith("jj");
    }
}
