/*
 * FE_WNSimilarity.java
 *
 * Created on July 17, 2007, 1:42 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package elkfed.coref.features.pairs.wn;

import java.math.BigDecimal;
import java.util.List;
import elkfed.coref.*;
import elkfed.ml.*;
import elkfed.mmax.util.NPHeadFinder;
import ponzo.nlp.wordnet.WNSimilarity;
import ponzo.nlp.wordnet.WNSimilarityFactory;
import ponzo.nlp.wordnet.WNSimilarityMeasure;
import ponzo.nlp.wikipedia.lang.Language;
import ponzo.nlp.wikipedia.WikiConstants;


import static elkfed.lang.EnglishLinguisticConstants.*;

/**
 * Feature used to determine the WordNet relatedness of features. Creates features for average/shortest path for Wu-Palmer,Leacock-Chodorow,Resnik,Jiang-Conrat, and linear.
 * @author vae2101
 */
public class FE_WNSimilarity implements PairFeatureExtractor {
   
     private static final WNSimilarityMeasure[] MEASURES = {
        WNSimilarityMeasure.PATH,
        WNSimilarityMeasure.WUP,
        WNSimilarityMeasure.LCH,
        WNSimilarityMeasure.RES,
        WNSimilarityMeasure.JCN,
        // WNSimilarityMeasure.LIN
    };
     
        /** The antecedent head */
    protected String antecedentString;
    
    /** The anaphora head */
    protected String anaphoraString;
    
    /** The similarity measures */
    WNSimilarity similarity;
    
    
    /** Creates a new instance of FE_WNSimilarity */
    public FE_WNSimilarity() {
        WNSimilarityFactory.getInstance().setCaching(true);
    }
 
        public static final FeatureDescription<Double> FD_WN_IS_SHORT_PATH=
            new FeatureDescription<Double>(FeatureType.FT_SCALAR, "Shortest_WN_PL");
       public static final FeatureDescription<Double> FD_WN_IS_AVE_PATH=
            new FeatureDescription<Double>(FeatureType.FT_SCALAR, "Average_WN_PL");
       
       public static final FeatureDescription<Double> FD_WN_IS_SHORT_WUPALMER=
            new FeatureDescription<Double>(FeatureType.FT_SCALAR, "Shortest_WN_WUP");
       public static final FeatureDescription<Double> FD_WN_IS_AVE_WUPALMER=
            new FeatureDescription<Double>(FeatureType.FT_SCALAR, "Average_WN_WUP");
       
       public static final FeatureDescription<Double> FD_WN_IS_SHORT_LeacockChodorow=
            new FeatureDescription<Double>(FeatureType.FT_SCALAR, "Shortest_WN_LCH");
       public static final FeatureDescription<Double> FD_WN_IS_AVE_LeacockChodorow=
            new FeatureDescription<Double>(FeatureType.FT_SCALAR, "Average_WN_LCH");
       
       public static final FeatureDescription<Double> FD_WN_IS_SHORT_RESNIK=
            new FeatureDescription<Double>(FeatureType.FT_SCALAR, "Shortest_WN_RES"); 
       public static final FeatureDescription<Double> FD_WN_IS_AVE_RESNIK=
            new FeatureDescription<Double>(FeatureType.FT_SCALAR, "Average_WN_RES"); 
       
       public static final FeatureDescription<Double> FD_WN_IS_SHORT_JC=
            new FeatureDescription<Double>(FeatureType.FT_SCALAR, "Shortest_WN_JCO"); 
       public static final FeatureDescription<Double> FD_WN_IS_AVE_JC=
            new FeatureDescription<Double>(FeatureType.FT_SCALAR, "AverageWN_WN_JCO"); 
        
//       public static final FeatureDescription<Double> FD_WN_IS_SHORT_LIN=
//            new FeatureDescription<Double>(FeatureType.FT_SCALAR, "Shortest_WN_LIN"); 
//       public static final FeatureDescription<Double> FD_WN_IS_AVE_LIN=
//            new FeatureDescription<Double>(FeatureType.FT_SCALAR, "Average_WN_LIN"); 
        
     public void describeFeatures(List<FeatureDescription> fds) {
 
        fds.add(FD_WN_IS_SHORT_PATH);
        fds.add(FD_WN_IS_AVE_PATH);
        
        fds.add(FD_WN_IS_SHORT_WUPALMER);
        fds.add(FD_WN_IS_AVE_WUPALMER);
          
        fds.add(FD_WN_IS_SHORT_LeacockChodorow);
        fds.add(FD_WN_IS_AVE_LeacockChodorow);
        
        fds.add(FD_WN_IS_SHORT_RESNIK);
        fds.add(FD_WN_IS_AVE_RESNIK);
   
//        fds.add(FD_WN_IS_SHORT_LIN);
//        fds.add(FD_WN_IS_AVE_LIN);
         
        fds.add(FD_WN_IS_SHORT_JC);
        fds.add(FD_WN_IS_AVE_JC);
    }

    public void extractFeatures(PairInstance inst) {
              
        setStrings(inst);
        setWordNetSimilarity(inst);
        setFeatures(inst);
     }
    
    /** Sets the string to be parsed */
    private void setStrings(PairInstance inst)
    {
        // if its a name, get the markable string, else pull the head lemma
        if (inst.getAntecedent().isEnamex())
        { this.antecedentString = inst.getAntecedent().getMarkableString(); }
        else
        { 
            this.antecedentString = 
                    NPHeadFinder.getInstance().getHeadLemma(inst.getAntecedent().getMarkable());
        }
        this.antecedentString = this.antecedentString + "#n";
        
        if (inst.getAnaphor().isEnamex())
        { this.anaphoraString = inst.getAnaphor().getMarkableString(); }
        else
        { 
            this.anaphoraString = 
                    NPHeadFinder.getInstance().getHeadLemma(inst.getAnaphor().getMarkable());
        }
        this.anaphoraString = this.anaphoraString + "#n";
    }
    
    /** Sets the WordNet similarity measures */
    private void setWordNetSimilarity(PairInstance inst)
    {       
        // do not pull pronouns and do not count head matching
        if (
                inst.getAntecedent().getMarkableString().matches(PRONOUN)
            ||
                inst.getAnaphor().getMarkableString().matches(PRONOUN)
            ||
                WikiConstants.getInstance().getStopList(Language.EN).contains(antecedentString.toLowerCase())
            ||
                WikiConstants.getInstance().getStopList(Language.EN).contains(anaphoraString.toLowerCase())
        )
        { this.similarity = null; }
        else
        {
            this.similarity =
                WNSimilarityFactory.getInstance().getWNSimilarity(antecedentString, anaphoraString, MEASURES);
        }
    }
    
    /** Sets the similarity measures */
    private void setFeatures(PairInstance inst)
    {
        setPath(inst); setWuPalmer(inst); setLeacockChodorow(inst);
        setResnik(inst); setJiangConrath(inst); // setLin(inst);
    }
    
    /** Sets the path similarity measures */
    private void setPath(PairInstance inst)
    {
        if (similarity == null || similarity.getShortestPathMeasure() == -1.0)
        { inst.setFeature(FD_WN_IS_SHORT_PATH, 0.0); }
        else
        { inst.setFeature(FD_WN_IS_SHORT_PATH, round(similarity.getShortestPathMeasure())); }
        if (similarity == null || similarity.getAveragePathMeasure() == -1.0)
        { inst.setFeature(FD_WN_IS_AVE_PATH, 0.0); }
        else
        { inst.setFeature(FD_WN_IS_AVE_PATH, round(similarity.getAveragePathMeasure())); }
    }    
    
    /** Sets the Wu and Palmer similarity measures */
    private void setWuPalmer(PairInstance inst)
    {
        if (similarity == null || similarity.getShortestWuPalmerMeasure() == -1.0)
        { inst.setFeature(FD_WN_IS_SHORT_WUPALMER, 0.0); }
        else
        { inst.setFeature(FD_WN_IS_SHORT_WUPALMER, round(similarity.getShortestWuPalmerMeasure())); }
        if (similarity == null || similarity.getAverageWuPalmerMeasure() == -1.0)
        { inst.setFeature(FD_WN_IS_AVE_WUPALMER, 0.0); }
        else
        { inst.setFeature(FD_WN_IS_AVE_WUPALMER, round(similarity.getAverageWuPalmerMeasure())); }
    }
    
    /** Sets the Leacock and Chodorow similarity measures */
    private void setLeacockChodorow(PairInstance inst)
    {
        if (similarity == null || similarity.getShortestLeacockChodorowMeasure() == -1.0)
        { inst.setFeature(FD_WN_IS_SHORT_LeacockChodorow, 0.0); }
        else
        { inst.setFeature(FD_WN_IS_SHORT_LeacockChodorow, round(similarity.getShortestLeacockChodorowMeasure())); }
        if (similarity == null || similarity.getAverageLeacockChodorowMeasure() == -1.0)
        { inst.setFeature(FD_WN_IS_AVE_LeacockChodorow, 0.0); }
        else
        { inst.setFeature(FD_WN_IS_AVE_LeacockChodorow, round(similarity.getAverageLeacockChodorowMeasure())); }
    }
    
    /** Sets the Resnik similarity measures */
    private void setResnik(PairInstance inst)
    {
        if (similarity == null || similarity.getShortestResnikMeasure() == -1.0)
        { inst.setFeature(FD_WN_IS_SHORT_RESNIK, 0.0); }
        else
        { inst.setFeature(FD_WN_IS_SHORT_RESNIK, round(similarity.getShortestResnikMeasure())); }
        if (similarity == null || similarity.getAverageResnikMeasure() == -1.0)
        { inst.setFeature(FD_WN_IS_AVE_RESNIK, 0.0); }
        else
        { inst.setFeature(FD_WN_IS_AVE_RESNIK, round(similarity.getAverageResnikMeasure())); }
    }

    /** Sets the Jiang and Conrath similarity measures */
    private void setJiangConrath(PairInstance inst)
    {
        if (similarity == null || similarity.getShortestJiangConrathMeasure() == -1.0)
        { inst.setFeature(FD_WN_IS_SHORT_JC, 0.0); }
        else
        { inst.setFeature(FD_WN_IS_SHORT_JC, round(similarity.getShortestJiangConrathMeasure())); }
        if (similarity == null || similarity.getAverageJiangConrathMeasure() == -1.0)
        { inst.setFeature(FD_WN_IS_AVE_JC, 0.0); }
        else
        { inst.setFeature(FD_WN_IS_AVE_JC, round(similarity.getAverageJiangConrathMeasure())); }
    }
    
    /** Sets the Lin similarity measures */
//    private void setLin(PairInstance inst)
//    {
//        if (similarity == null || similarity.getShortestLinMeasure() == -1.0)
//        { inst.setFeature(FD_WN_IS_SHORT_LIN, 0.0); }
//        else
//        { inst.setFeature(FD_WN_IS_SHORT_LIN, round(similarity.getShortestLinMeasure())); }
//        if (similarity == null || similarity.getAverageLinMeasure() == -1.0)
//        { inst.setFeature(FD_WN_IS_AVE_LIN, 0.0); }
//        else
//        { inst.setFeature(FD_WN_IS_AVE_LIN, round(similarity.getAverageLinMeasure())); }
//    }
    
    public static double round(double d)
    { return new BigDecimal(d).setScale(3, BigDecimal.ROUND_UP).doubleValue(); }
}
