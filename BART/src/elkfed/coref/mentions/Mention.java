/*
 * Mention.java
 * 
 * Copyright 2007 Project ELERFED
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package elkfed.coref.mentions;

import edu.stanford.nlp.trees.Tree;

import elkfed.config.ConfigProperties;
import elkfed.coref.utterances.Utterance;
import elkfed.coref.discourse_entities.DiscourseEntity;

import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import elkfed.mmax.minidisc.Markable;
import elkfed.mmax.minidisc.MiniDiscourse;
import elkfed.nlp.util.Gender;
import elkfed.nlp.util.NameStructure;
import elkfed.knowledge.SemanticClass;
import elkfed.coref.discourse_entities.DiscourseEntity;

import elkfed.lang.LanguagePlugin;
import elkfed.lang.MentionType;
import java.util.Set;
import static elkfed.lang.MentionType.Features;
import static elkfed.mmax.pipeline.MarkableCreator.ISPRENOMINAL_ATTRIBUTE;
import static elkfed.mmax.pipeline.MarkableCreator.SENTENCE_ID_ATTRIBUTE;



/**
 * Takes a markable and wraps it in a mention object.
 * Information about the mention (ex: gender information,
 * plurality, etc.) is determined as well.
 *
 * @author vae2101
 *
 */
public class Mention implements Comparable<Mention> {
    private static final Logger _logger=Logger.getLogger("elkfed.mentions");
    
    // Mention information
    final private MentionType _mentionType;
    final private String headString;
    final private Markable _markable;
    final private String _markableString;
    final private String _enamexType;
    private String _setID = null;
    private int  _mentionIdx; // in sentence
    final private MiniDiscourse _document;
    
    final private int _sentId;
    private Tree _sentenceTree;
    private Tree _lowestProjection;
    private Tree _highestProjection;
    private List<Tree> _premodifiers;
    private List<Tree> _postmodifiers;
    private int _startWord;
    private int _endWord;
    private HashMap<String,String> _nameStructure;
    
    private Utterance _utterance;
    private int _posInUtterance;
    private boolean isFirstMention;
    
    private DiscourseEntity _discourseEntity;
        
    /**
     * Constructs a mention object and populates all necessary local variables,
     * taking a markable and an MMAX document as arguments.
     *
     */
    public Mention(Markable markable, MiniDiscourse doc) {
        LanguagePlugin lang_plugin=
                ConfigProperties.getInstance().getLanguagePlugin();
        _markable = markable;
        _document = doc;
        _markableString = lang_plugin.markableString(markable);
        _enamexType = lang_plugin.enamexType(markable);
        
        // get semclass, gender, number
        _mentionType=lang_plugin.calcMentionType(markable);
        
        headString = lang_plugin.getHead(markable);
        
        _sentId = Integer.parseInt(getMarkable().getAttributeValue(
                SENTENCE_ID_ATTRIBUTE));
        
        // Utterance - no need to set??
        //_utterance=null;
        //_posInUtterance = -1;
        isFirstMention = false;
        
        // Name internal stucture

        if (getProperName()) {
            _nameStructure = calcNameStructure();
        }
   }

    public MentionType mentionType() {
        return _mentionType;
    }

    public boolean getProperName() {
        return _mentionType.features.contains(MentionType.Features.isProperName);
    }
    
    public void setMentionIdx( int i )
       {
       _mentionIdx = i;
       }
    
    public int getMentionIdx()
    {
       return _mentionIdx;
    }
    /** returns true if mention is a pronoun */
    public boolean getPronoun(){
        return _mentionType.features.contains(MentionType.Features.isPronoun);
    }
    public boolean getReflPronoun(){
        return _mentionType.features.contains(MentionType.Features.isReflexive);
    }
    public boolean getPossPronoun(){
        return _mentionType.features.contains(MentionType.Features.isPossPronoun);
    }
    public boolean getPersPronoun(){
        return _mentionType.features.contains(MentionType.Features.isPersPronoun);
    }
    public boolean getDefinite(){
        return _mentionType.features.contains(MentionType.Features.isDefinite);
    }

    public boolean getIndefinite() {
        return _mentionType.features.contains(MentionType.Features.isIndefinite);
    }

    public boolean getDemonstrative(){
        return _mentionType.features.contains(MentionType.Features.isDemonstrative);
    }
    public boolean getDemPronoun(){
        return _mentionType.features.contains(MentionType.Features.isDemPronoun);
    }
    public boolean getDemNominal(){
        return _mentionType.features.contains(MentionType.Features.isDemNominal);
    }
    /**
     *  Return if mention is singular
     */
    public boolean getNumber(){
        return _mentionType.features.contains(MentionType.Features.isSingular);
    }
    /**
     *  Return mention person
     */
    public boolean getIsFirstSecondPerson() {
        return _mentionType.features.contains(MentionType.Features.isFirstSecondPerson);
    }
    
    /**
     *  Return mention gender
     */
    public Gender getGender(){ return _mentionType.gender;}
    

    /**
     *  Return mention head string of NP
     */
    public String getHeadString(){return headString;}

    
    /**
     * return the head string of the NP as it is needed
     * for pattern searches or WP queries
     */
    public String getHeadOrName() {
        LanguagePlugin lang_plugin=
                ConfigProperties.getInstance().getLanguagePlugin();
        return lang_plugin.getHeadOrName(getMarkable());
    }
    
    public Set<Features> getFeatures() {
        return _mentionType.features;
    }
    
    /**
     * Return the mention's name internal structure.
     * Only really makes sense if mention is a proper name
     */
    public HashMap<String,String> getNameStructure() { return _nameStructure; }
    
    
    /**
     *  Return mentions markable
     */
    public Markable getMarkable(){return _markable;}
    
    /**
     *  Return mentions markable string
     */
    public String getMarkableString() {return _markableString;}
    
    /**
     *  Return mentions document
     */
    public MiniDiscourse getDocument() { return _document; }
    
    /**
     *  Return mentions semantic class
     */
    public SemanticClass getSemanticClass() {
        return _mentionType.semanticClass;
    }
    
    /** set parse information. This should only be used by
     *  CorefMentionFactory */
    public void setParseInfo(Tree sentTree, int start, int end) {
        _sentenceTree=sentTree;
        _startWord=start;
        _endWord=end;
        LanguagePlugin lang_plugin=
                ConfigProperties.getInstance().getLanguagePlugin();
        List<Tree>[] parseInfo=lang_plugin.calcParseInfo(
                sentTree, start, end,
                _mentionType);
        List<Tree> projections=parseInfo[0];
        _premodifiers=parseInfo[1];
        _postmodifiers=parseInfo[2];
        _lowestProjection=projections.get(0);
        _highestProjection=projections.get(projections.size()-1);
        if (_logger.isLoggable(Level.FINE))
        {
            _logger.fine(String.format("Parse info for '%s'",toString()));
            _logger.fine("headOrName: "+getHeadOrName());
            _logger.fine("lowest: "+_lowestProjection);
            _logger.fine("highest: "+_highestProjection);
            _logger.fine("premodify: "+_premodifiers);
            _logger.fine("postmodify: "+_postmodifiers);
        }
    }
    
    /* Utterance info */
    
    public Utterance getUtterance() { return _utterance; }
    public void setUtterance(Utterance utt) {      
        _utterance=utt;
        _utterance.addCF( this );
    }
    
    public int getUttPos() { return _posInUtterance;}
    public void setUttPos(int pos) {
        _posInUtterance=pos;
        if (_posInUtterance==0) { 
            isFirstMention=true; 
        }
    }
    
    public boolean getIsFirstMention() { return isFirstMention; }
    
    // discourse entities
    public DiscourseEntity getDiscourseEntity() { return _discourseEntity; }
    public void createDiscourseEntity() {
        _discourseEntity = new DiscourseEntity(this);
    }
  
    /**
     * Sorting
     */
    public int compareTo(Mention m) {
        if (_startWord < m.getStartWord()) {
            return -1;
        } 
        else if ( m.getStartWord() < _startWord) {
            return 1;
        } else {
            return 0;
        }
    }
    
    /** Uses some heuristics to determine internal structure in names.
     *  i.e. Forename, Middle, Surname, etc.
     *
     */
    private HashMap<String,String> calcNameStructure() {
        return NameStructure.getNameStructure(_markableString);
    }
    

    
    /** returns the parse for the containing sentence */
    public Tree getSentenceTree() { return _sentenceTree; }
    /** returns the baseNP node for that markable */
    public Tree getLowestProjection() { return _lowestProjection; }
    /** returns the highest projection for that markable */
    public Tree getHighestProjection() { return _highestProjection; }
    
    /** returns the sentence-relative word index of the
     *  start of the markable */
    public int getStartWord() { return _startWord; }
    public void setStartWord(int i) { _startWord = i; }
    
    /** returns the sentence-relative word index of the
     *  end of the markable */
    public int getEndWord() { return _endWord; }
    public void setEndWord(int i) { _endWord = i; }
    
    
    /**Determine whether mention is coreferent with a given mention
     */
    public boolean isCoreferent(Mention m) {
        if (this._setID == null)
            return false;
        else if (_setID.equals(m._setID))
            return true;
        else
            return false;
    }
    
    /** see if two mentions have overlapping spans -
     *  actually, a better approximation to syntactic embedding
     *  would probably make sense here, as in
     *  [1 the guardian] of [2[3 his] treasure]
     *  we would like to allow 1--3, but not 1--2(?)
     */
    public boolean overlapsWith(Mention m) {
        Markable m1=getMarkable();
        Markable m2=m.getMarkable();
        if (m1.getRightmostDiscoursePosition() <=
                m2.getLeftmostDiscoursePosition())
            return false;
        else if (m2.getRightmostDiscoursePosition() <=
                m1.getLeftmostDiscoursePosition())
            return false;
        else
            return true;
        
    }
    
    /** see if two mentions have overlapping spans -
     *  like in [1 [2 his] treasure]
     */
    public boolean embeds(Mention m) {
        Markable m1=getMarkable();
        Markable m2=m.getMarkable();
        return
                m1.getLeftmostDiscoursePosition() <= m2.getLeftmostDiscoursePosition()
                &&
                m1.getRightmostDiscoursePosition() >= m2.getRightmostDiscoursePosition();
    }
    
    public void setSetID(String setid) {
        _setID = setid;
    }
    
    public String getSetID() {
       return _setID;
    }
    
    public static String getMarkableString(final Markable markable) {
        return new StringBuffer(markable.toString()).
                deleteCharAt(markable.toString().length()-1).deleteCharAt(0).toString();
    }
    
    public int getSentId() {
        return _sentId;
    }
    
    @Override
    public String toString() {
        return _markableString;
    }
    
    public String[] getLeftContext(int nWords) {
        int n;
        int posN=getMarkable().getLeftmostDiscoursePosition();
        if (posN<nWords)
            n=posN;
        else
            n=nWords;
        String[] result=new String[n];
        for (int token = posN-n; token < posN; token++) {
            result[token-posN+n]=_document
                    .getDiscourseElementAtDiscoursePosition(token).toString();
        }
        return result;
    }
    
    public String[] getRightContext(int nWords) {
        int posN=getMarkable().getRightmostDiscoursePosition()+1;
        String[] result=new String[nWords];
        for (int token = posN; token < posN+nWords; token++) {
            result[token-posN]=_document
                    .getDiscourseElementAtDiscoursePosition(token).toString();
        }
        return result;
    }
    
    /** Checks whether this mention is a named entity */
    public boolean isEnamex()
    {
        return _mentionType.features.contains(Features.isEnamex);
    }
    
    
    /** Gets the type of a named entity */
    public String getEnamexType()
    { return _enamexType; }

    public List<Tree> getPostmodifiers() {
        return _postmodifiers;
    }

    
    public List<Tree> getPremodifiers() {
        return _premodifiers;
    }
    
    public String getRootPath()
    {
        Tree top=getSentenceTree();
        Tree here=getHighestProjection();
        StringBuffer sb=new StringBuffer();
        String lastValue=null;
        while (here!=top)
        {
            here=here.parent(top);
            String val=here.value();
            if (!val.equals(lastValue))
                sb.append(here.value()).append(".");
            lastValue=val;
        }
        return sb.toString();
    }
    
    public boolean isPrenominal()
    {
        return Boolean.parseBoolean(
                     getMarkable().getAttributeValue(ISPRENOMINAL_ATTRIBUTE));
    }

    public void linkToAntecedent(Mention ante) {
        ante.getDiscourseEntity().merge(this);
        _discourseEntity = ante.getDiscourseEntity();
    }
}
