/*
 * DefaultMentionFactory.java
 *
 * Created on July 17, 2007, 12:06 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package elkfed.coref.mentions;

import elkfed.mmax.DiscourseUtils;
import java.io.IOException;
import java.util.*; // for Collections.sort
import elkfed.mmax.minidisc.Markable;
import elkfed.mmax.minidisc.MarkableLevel;
import elkfed.mmax.minidisc.MiniDiscourse;
import elkfed.mmax.util.CorefDocuments;

import static elkfed.mmax.MarkableLevels.DEFAULT_COREF_LEVEL;

/** Given a MMAX2Discourse, extracts all of its mentions *INCLUDED IN TEXT*
 *  (i.e. between the <T(E)XT></T(E)XT>* tags of a SGML/XML document
 *  and returns them in a List.
 *
 * @author brett.shwom
 */
public class DiagnosticMentionFactory extends DefaultMentionFactory {

    //TODO: move the constant to MarkableLevels
    public static String DIAGNOSTIC_LEVEL = "diagnostic";

    @Override
    protected void reportMapping(Markable m_markable,
            Markable m_coref) {
        MiniDiscourse doc;
        if (m_markable!=null) {
            doc = m_markable.getMarkableLevel().getDocument();
        } else {
            doc = m_coref.getMarkableLevel().getDocument();
        }
        MarkableLevel diagnosticLevel = doc.getMarkableLevelByName(DIAGNOSTIC_LEVEL);
        if (m_markable != null && m_coref != null) {
            Markable m_markable2 =
                    CorefDocuments.getInstance().corefElementIsaMarkable(doc, m_markable);
            if (m_markable2 == null) {
                System.out.println("Correct (dubious): " + m_markable.toString() +
                        "(markable:" + m_markable.getID() + "/coref:" + m_coref.getID() + " " + m_coref.toString());
            //BUG: we have to compare IDs instead of object identity?
            } else if (m_markable2 == m_markable) {
                System.out.println("Correct: " + m_markable.toString() +
                        "(markable:" + m_markable.getID() + "/coref:" + m_coref.getID() + ")");
                Map<String, String> attrs = new HashMap<String, String>();
                attrs.put("type", "correct");
                diagnosticLevel.addMarkable(m_markable.getLeftmostDiscoursePosition(),
                        m_markable.getRightmostDiscoursePosition(),
                        attrs);
            } else {
                System.out.println("Duplicate: " + m_markable.toString() +
                        "(markable:" + m_markable.getID() + "/coref:" + m_coref.getID() + " " + m_coref.toString() + "; duplicate of " + m_markable2.getID() + " " + m_markable2.toString());
                Map<String, String> attrs = new HashMap<String, String>();
                attrs.put("type", "duplicate");
                attrs.put("correctWords", m_coref.toString());
                diagnosticLevel.addMarkable(m_markable.getLeftmostDiscoursePosition(),
                        m_markable.getRightmostDiscoursePosition(),
                        attrs);
            }
        } else if (m_markable!=null) {
            System.out.println("Non-Gold: " + m_markable.toString() +
                            "(markable:" + m_markable.getID() + ")");
                    Map<String, String> attrs = new HashMap<String, String>();
                    attrs.put("type", "nongold");
                    diagnosticLevel.addMarkable(m_markable.getLeftmostDiscoursePosition(),
                            m_markable.getRightmostDiscoursePosition(),
                            attrs);
        } else if (m_coref!=null) {
            System.out.println("Missed: " + m_coref.toString() +
                        "(coref:" + m_coref.getID() + ")");
                Map<String, String> attrs = new HashMap<String, String>();
                attrs.put("type", "missed");
                diagnosticLevel.addMarkable(m_coref.getLeftmostDiscoursePosition(),
                        m_coref.getRightmostDiscoursePosition(),
                        attrs);
        }
    }

    @Override
    public List<Mention> extractMentions(MiniDiscourse doc) throws IOException {
        List<Mention> result = super.extractMentions(doc);

        for (Markable m_coref : DiscourseUtils.getMarkables(doc, DEFAULT_COREF_LEVEL)) {
            Markable m_markable = CorefDocuments.getInstance().corefElementIsaMarkable(doc, m_coref);
            if (m_markable == null) {
                reportMapping(null,m_coref);
            }
        }
        doc.getMarkableLevelByName(DIAGNOSTIC_LEVEL).saveMarkables();
        return result;
    }

}
