package elkfed.util;

/** Util methods for Strings
 *
 * @author ponzo
 */
public class Strings {
    
    /** The default pad */
    private static final int DEFAULT_PAD = 15;
    
    /** Creates a new instance of Strings */
    private Strings() { }
        
    public static String toPaddedString(final Object obj)
    { return toPaddedString(obj, DEFAULT_PAD); }
    
    public static String toPaddedString(final Object obj, final int padding)
    {
        if (obj.toString().length() >= padding)
        { return obj.toString().substring(0, padding); }
        else
        {
            final StringBuffer buffer = new StringBuffer();
            int pad = obj.toString().length();
            while (pad++ < padding)
            { buffer.append(" "); } 
            buffer.append(obj.toString());
            return buffer.toString();
        }
    }
    
    public static String getNTimes(final char c, final int times)
    {
        int timesSoFar = 0;
        final StringBuffer buffer = new StringBuffer();
        while (timesSoFar++ < times)
        { buffer.append(c); }
        return buffer.toString();
    }
}
