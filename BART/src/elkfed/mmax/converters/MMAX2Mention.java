package elkfed.mmax.converters;

import elkfed.config.ConfigProperties;
import elkfed.mmax.Corpus;
import elkfed.mmax.CorpusFactory;
import elkfed.mmax.DiscourseUtils;
import elkfed.mmax.MMAX2FilenameFilter;
import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import org.eml.MMAX2.discourse.MMAX2Discourse;
import org.eml.MMAX2.discourse.MMAX2DiscourseLoader;

/**
 * Dumps a MMAX2 document into muc format
 *  using a MMAX2Mention stylesheet
 *  (default is under config/MMAX2Mention.xsl)
 * 
 * @author ponzo
 */
public class MMAX2Mention extends Converter {
    
    private final String MENTION_DIR = "responses-inline";
    
    private final String styleSheet;

    /**
     * Creates a new instance of MMAX2Mention
     */
    public MMAX2Mention() {
        this(ConfigProperties.getInstance().getMMAX2MUC());
    }
    
    /**
     * Creates a new instance of MMAX2Mention
     */
    public MMAX2Mention(String styleSheet) {
        this.styleSheet = styleSheet;
    }
    
    protected FilenameFilter getFilter()
    { return MMAX2FilenameFilter.FILTER_INSTANCE; }
    
    protected String getOutputBaseDir()
    { return MENTION_DIR; }
    
    protected String getOutputExtension()
    { return MentionFilenameFilter.MENTION_EXT; }
    
    /** Dumps a Document */
    public void dumpDocument(String file)
    {
        try 
        {
            // 0. build discourse
            String commonPathFile = 
                    new File(file).getParent() + File.separator + "common_paths.xml";
            
            MMAX2Discourse discourse = 
                    new MMAX2DiscourseLoader(file,false,commonPathFile).getCurrentDiscourse();
            
            String output = getOutputFile(file);
            // 1. open filewriter
            FileWriter writer = new FileWriter(new File(output));
            
            // 2. dump
            LOGGER.info("Converting " + file + " to mention format (" + output + ")");
            discourse.applyStyleSheet(styleSheet);
            writer.write(discourse.getStyleSheetOutput());
            
            // 3. close filewriter
            writer.flush();
            writer.close();
        } 
        catch (IOException ioe) 
        { ioe.printStackTrace(); }
    }
}
