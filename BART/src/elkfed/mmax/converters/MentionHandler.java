/*
 * Copyright (c) 2007 The MITRE Corporation
 *
 * See the full RIGHTS STATEMENT at end of this file.
 *
 * Author: David Day
 * Organization: The MITRE Corporation
 * Project: JHU/WS07/ELERFED
 * Date: July, 2007
 *
 */

package elkfed.mmax.converters;

import java.io.*;
import java.util.*;

import org.xml.sax.*;
import org.xml.sax.helpers.*;

import org.dom4j.*;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

public class MentionHandler extends DefaultHandler {

    // APF default values
    public String   APFDTD           = "apf.v5.1.1.dtd";
    public String   APFSOURCE        = "unknown";
    public String   APFTYPE          = "text";
    public String   APFVERSION       = "5.1.1";
    public String   APFAUTHOR        = "jhu-ws07-elerfed";
    public String   APFENCODING      = "UTF-8";

    // InlineMention-related variables
    public boolean inMentionP          = false;
    public int mentionCount            = 0;
    public String mentionId            = null;
    public String mentionText          = null;
    public StringBuffer signalBuffer = null;
    public String signalString       = null;
    public int signalOffset          = 0;

    // APF variables
    public int entityCount           = 0;
    public int mentionOffsetStart      = 0;
    public int mentionOffsetEnd        = 0;
    public String entityId           = null;
    public String entityType         = null;
    public String entitySubType      = null;
    public String defaultGPESubType  = "Population-Center";
    public String defaultPERSubType  = "Individual";
    public String defaultORGSubType  = "Government";
    public String defaultLOCSubType  = "Region-General";
    public String defaultFACSubType  = "Building-Grounds";
    public String defaultVEHSubType  = "Underspecified";
    public String defaultWEASubType  = "Underspecified";
    public String defaultGPERole     = "GPE";
    public String defaultMetonymy    = "FALSE";

    // Miscellaneous
    public int uniqueIntCycleMarker  = 0;
    // we need a stack of mention to handle embedded tags
    public Stack<Mention> mentions = new Stack<Mention>();
    //  public Mention mention           = new Mention();
    
    // public HashMap mentionTable      = new HashMap();
    public Map<String,List<Mention>> entityTable       = new HashMap<String,List<Mention>>();

    // APF DOM variables
    public Document apfDoc           = null;

    public void startDocument() throws SAXException {
    }

    public void endDocument() throws SAXException {
    }

    public void startElement(String namespaceURI, String localName,
			     String qualifiedName, Attributes attrs)
	throws SAXException {

	// System.out.println("Encountered startElement for element " + localName);
	if(localName.equals("MENTION")) {
	    System.out.print(".");
	    inMentionP          = true;
	    signalBuffer        = new StringBuffer();
	    
            // changed for stack-compatibility
            Mention mention     = new Mention();
            // mention             = new Mention();
            
	    mention.mentionId   = getAttributeFromList(attrs, "ID");
	    mention.entityId    = getAttributeFromList(attrs, "EID");
	    mention.entityCDCId = getAttributeFromList(attrs, "CDCID");
	    if (mention.entityId == null) {
		mention.entityId = new String("ASSIGNED-" + entityCount);
		System.err.println("Warning: Missing MENTION EID.  Assigning unique EID: " + mention.entityId);
	    }
	    mention.startOffset = getAttributeFromList(attrs, "COS");
	    mention.endOffset   = getAttributeFromList(attrs, "COE");
	    // mentionTable.put(mention.mentionId,mention);
	    if (entityTable != null) {
		// Mention rootEntityMention = (Mention) entityTable.get(mention.entityId);
		
                if (!entityTable.containsKey(mention.entityId)) {
                // if (rootEntityMention == null) {
		    entityCount++;
                    
                    entityTable.put(mention.entityId, new ArrayList<Mention>());
                    // entityTable.put(mention.entityId,mention);
		    
                    System.out.print("r" + entityCount);
		} else {
		    System.out.print("m");
		}
                entityTable.get(mention.entityId).add(mention);
	    } else {
		System.err.println("ERROR: entityTable is NULL!");
	    }
	    mention.entityType      = getAttributeFromList(attrs, "TYPE");
	    if (mention.entityType == null) {
		mention.entityType      = getAttributeFromList(attrs, "ETYPE");
	    }
	    mention.entitySubType   = getAttributeFromList(attrs, "SUBTYPE");
	    if (mention.entitySubType == null) {
		mention.entitySubType   = getAttributeFromList(attrs, "ESUBTYPE");
	    }
	    mention.entityClass     = getAttributeFromList(attrs, "CLASS");
	    mention.mentionType     = getAttributeFromList(attrs, "mtype");
	    mention.mentionRole     = getAttributeFromList(attrs, "ROLE");
	    mention.mentionMetonymy = getAttributeFromList(attrs, "METONYMY");
	    mention.mentionLDCType  = getAttributeFromList(attrs, "LDCTYPE");
	    
            // added for stack-compatibility            
            handleDefaultValues(mention, localName, attrs);
	}
    }

    public void handleDefaultValues (Mention mention, String localName, Attributes attrs) {
	if (mention.entityType == null || mention.entityType.equalsIgnoreCase("none")) {
	    System.err.println("WARNING: Missing MENTION TYPE (defaulting to PER)");
	    mention.entityType = "PER";
	}
	if (mention.mentionLDCType != null) {
	    mention.mentionLDCAtr = "TRUE";
	}
	if (mention.entityType.equals("GPE") && mention.mentionRole == null) {
	    mention.mentionRole = defaultGPERole;
	}
	if (mention.entityClass == null) {
	    mention.entityClass = "SPC";
	}
	if (mention.mentionType == null) {
	    mention.mentionType = "NAM";
	}
	if (mention.entitySubType == null) {
	    if (mention.entityType.equalsIgnoreCase("GPE")) {
		mention.entitySubType    = defaultGPESubType;
	    } else if (
                    mention.entityType.equalsIgnoreCase("PER")
                 ||
                    mention.entityType.equalsIgnoreCase("PERSON")
            ) {
		mention.entitySubType    = defaultPERSubType;
	    } else if (
                    mention.entityType.equalsIgnoreCase("ORG")
                 ||
                    mention.entityType.equalsIgnoreCase("ORGANIZATION")
            ) {
		mention.entitySubType    = defaultORGSubType;
	    } else if (
                    mention.entityType.equalsIgnoreCase("LOC")
                 ||
                    mention.entityType.equalsIgnoreCase("LOCATION")
            ) 
            {
		mention.entitySubType    = defaultORGSubType;
	    } else if (mention.entityType.equalsIgnoreCase("FAC")) {
		mention.entitySubType    = defaultFACSubType;
	    } else if (mention.entityType.equalsIgnoreCase("VEH")) {
		mention.entitySubType    = defaultVEHSubType;
	    } else if (mention.entityType.equalsIgnoreCase("WEA")) {
		mention.entitySubType    = defaultVEHSubType;
	    } else {
		System.err.println("Unrecognized mention type (type=" + mention.entityType + ")");
	    }
	}
        // added for stack-compatibility
        mentions.push(mention);
    }

    public String getAttributeFromList(Attributes attrs, String attrName) {
	for (int i = 0; i < attrs.getLength(); i++) {
	    String name  = attrs.getQName(i);
	    if (name.equalsIgnoreCase(attrName)) {
		String type  = attrs.getType(i);
		String value = attrs.getValue(i);
		// System.out.println("extracted attribute value " + value + " from attribute " + attrName);
		return value;
	    }
	}
	// No matching attribute name found.
	return null;
    }


    public void endElement(String uri, String localName, String qName)
	throws SAXException {

	// System.out.println("Encountered endElement for element " + localName);
	if (localName.equals("MENTION")) {
	    mentionCount++;
	    // changed for stack-compatibility
            // inMentionP = false;
	    if (signalBuffer == null) {
		System.err.println("Error: null signalBuffer");
	    } else {
                mentions.peek().text = signalBuffer.toString();
                // changed for stack-compatibility
		// mention.text  = signalBuffer.toString();
	    }
	    mentions.pop();
            if (mentions.size() == 0)
            { 
                inMentionP = false; 
                signalBuffer = new StringBuffer();
            }
            // changed for stack-compatibility
            // mention           = null;
	}
    }

    public void characters(char[] chars, int start, int length) throws SAXException {

	signalOffset = signalOffset + length;
	if (inMentionP) {
	    if (signalBuffer == null) {
		signalBuffer = new StringBuffer();
	    }
	    signalBuffer.append(chars, start, length);
	}
    }


    public void createAPF (String apfFilename) throws IOException {
	// Create DOM model of APF document, starting with header elements
	Element apfDocElement = initializeAPF();
	// Now map over entities and add to source
	addEntityElements(apfDocElement);
	try {
	    writeAPF(apfFilename);
	} catch (Exception e) {
	    e.printStackTrace();
	}
	System.out.println();
	System.out.println(mentionCount + " mention elements were mapped to ACE entity mentions.");
	System.out.println(entityCount + " distinct coref chains were mapped to ACE entities.");
	System.out.println("APF XML output written to: " + apfFilename);
    }


    public void addEntityElements(Element doc) {

	try {

	    // Loop over all the entities (by looping over the entity IDs)
	    // Collection entityCollection = entityTable.values();
	    // Iterator entityIter = entityCollection.iterator();
	    // while (entityIter.hasNext()) {
            for (String entityId : entityTable.keySet()) {
                List<Mention> mentions = entityTable.get(entityId);
                
                Mention rootMention = mentions.get(0);
		// Mention rootMention = (Mention) entityIter.next();
		Element entityElement = doc.addElement("entity");
		entityElement.addAttribute("ID",      rootMention.entityId);
		entityElement.addAttribute("TYPE",    rootMention.entityType);
		entityElement.addAttribute("SUBTYPE", rootMention.entitySubType);
		entityElement.addAttribute("CLASS",   rootMention.entityClass);
		
                for (Mention mention : mentions) {
                    addEntityMentions(entityElement, mention);
                    // addEntityMentions(entityElement, rootMention);
                    addEntityAttributes(entityElement, mention, false);
                    // addEntityAttributes(entityElement, rootMention, false);
                }
	    }
	} catch (Exception t) {
	    t.printStackTrace();
	}
    }


    public Element initializeAPF() {

	apfDoc = DocumentHelper.createDocument();
	apfDoc.addDocType("source_file", "SYSTEM", APFDTD);

	File inXMLFile = new File(Mention2APF.signalFilename);

	Element root = apfDoc.addElement("source_file")
	    .addAttribute("URI",      inXMLFile.getName())
	    .addAttribute("SOURCE",   APFSOURCE)
	    .addAttribute("TYPE",     APFTYPE)
	    .addAttribute("VERSION",  APFVERSION)
	    .addAttribute("AUTHOR",   APFAUTHOR)
	    .addAttribute("ENCODING", APFENCODING);

	Element doc = root.addElement("document")
	    .addAttribute("DOCID",    Mention2APF.apfDocId);

	return doc;
    }


    public void addEntityMentions (Element entityElement, Mention ment) {

	try {
	    Element mentionElement = entityElement.addElement("entity_mention");
	    mentionElement.addAttribute("ID",   ment.mentionId);
	    mentionElement.addAttribute("TYPE", ment.mentionType);
	    if (ment.mentionLDCType != null) {
		mentionElement.addAttribute("LDCTYPE", ment.mentionLDCType);
		mentionElement.addAttribute("LDCATR", "TRUE");
	    } else {
		mentionElement.addAttribute("LDCATR", "FALSE");
	    }
	    if (ment.mentionRole != null) {
		mentionElement.addAttribute("ROLE", ment.mentionRole);
	    }
	    if (ment.mentionMetonymy != null) {
		mentionElement.addAttribute("METONYMY_MENTION", ment.mentionMetonymy);
	    }

	    // Extent
	    Element extentElement  = mentionElement.addElement("extent");
	    Element charseqElement = extentElement.addElement("charseq");
	    int endOffset = Integer.parseInt(ment.endOffset);
	    String endAsString = Integer.toString(endOffset - 1); // charseq defined this way
	    charseqElement.addAttribute("START", ment.startOffset);
	    charseqElement.addAttribute("END",   endAsString);
	    charseqElement.addText(ment.text.trim().replaceAll("(\n\r|\n|\r)"," "));

	    // Head
	    Element headElement  = mentionElement.addElement("head");
	    charseqElement = headElement.addElement("charseq");
	    charseqElement.addAttribute("START", ment.startOffset);
	    charseqElement.addAttribute("END",   endAsString);
	    charseqElement.addText(ment.text.trim().replaceAll("(\n\r|\n|\r)"," "));
	} catch (Exception t) {
	    t.printStackTrace();
	}
	ment = ment.nextMention;
	if (ment != null) {
	    addEntityMentions(entityElement, ment);
	}
    }


    public void addEntityAttributes (Element parentElement, Mention ment, boolean attributesYetP) {

	try {
	    if (ment.mentionType.equals("NAM")) {
		if (!attributesYetP) {
		    Element attributesElement = parentElement.addElement("entity_attributes");
		    attributesYetP = true;
		    parentElement = attributesElement;
		}
		Element nameElement  = parentElement.addElement("name");
		nameElement.addAttribute("NAME", ment.text.trim().replaceAll("(\n\r|\n|\r)"," "));
		Element charseqElement = nameElement.addElement("charseq");
		int endOffset = Integer.parseInt(ment.endOffset);
		String endAsString = Integer.toString(endOffset - 1); // charseq defined this way
		charseqElement.addAttribute("START", ment.startOffset);
		charseqElement.addAttribute("END",   endAsString);
		charseqElement.addText(ment.text.trim().replaceAll("(\n\r|\n|\r)"," "));
	    }
	} catch (Exception t) {
	    t.printStackTrace();
	}
	ment = ment.nextMention;
	if (ment != null) {
	    addEntityAttributes(parentElement, ment, attributesYetP);
	}
    }


    public void writeAPF(String apfFilename) throws IOException {
	try {
	    // System.out.println("Opening xml output file for APF " + apfFilename);
	    FileOutputStream apfOut  = new FileOutputStream(apfFilename);
	    Writer apfStreamWriter   = new OutputStreamWriter(apfOut,"UTF-8");
	    Writer apfBufferedWriter = new BufferedWriter(apfStreamWriter);

	    OutputFormat format = OutputFormat.createPrettyPrint();
	    XMLWriter xmlWriter = new XMLWriter(apfBufferedWriter, format);
	    xmlWriter.write(apfDoc);
	    xmlWriter.flush();
	    xmlWriter.close();
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

}

/*
 * ------------------   RIGHTS STATEMENT   --------------------------
 *
 *                   Copyright (c) 2002-2007
 *                    The MITRE Corporation
 *
 *                      ALL RIGHTS RESERVED
 *
 *
 * The MITRE Corporation (MITRE) provides this software to you without
 * charge to use for your internal purposes only. Any copy you make for
 * such purposes is authorized provided you reproduce MITRE's copyright
 * designation and this License in any such copy. You may not give or
 * sell this software to any other party without the prior written
 * permission of the MITRE Corporation.
 *
 * The government of the United States of America may make unrestricted
 * use of this software.
 *
 * This software is the copyright work of MITRE. No ownership or other
 * proprietary interest in this software is granted you other than what
 * is granted in this license.
 *
 * Any modification or enhancement of this software must inherit this
 * license, including its warranty disclaimers. You hereby agree to
 * provide to MITRE, at no charge, a copy of any such modification or
 * enhancement without limitation.
 *
 * MITRE IS PROVIDING THE PRODUCT "AS IS" AND MAKES NO WARRANTY, EXPRESS
 * OR IMPLIED, AS TO THE ACCURACY, CAPABILITY, EFFICIENCY,
 * MERCHANTABILITY, OR FUNCTIONING OF THIS SOFTWARE AND DOCUMENTATION. IN
 * NO EVENT WILL MITRE BE LIABLE FOR ANY GENERAL, CONSEQUENTIAL,
 * INDIRECT, INCIDENTAL, EXEMPLARY OR SPECIAL DAMAGES, EVEN IF MITRE HAS
 * BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * You accept this software on the condition that you indemnify and hold
 * harmless MITRE, its Board of Trustees, officers, agents, and
 * employees, from any and all liability or damages to third parties,
 * including attorneys' fees, court costs, and other related costs and
 * expenses, arising out of your use of this software irrespective of the
 * cause of said liability.
 *
 * The export from the United States or the subsequent reexport of this
 * software is subject to compliance with United States export control
 * and munitions control restrictions. You agree that in the event you
 * seek to export this software you assume full responsibility for
 * obtaining all necessary export licenses and approvals and for assuring
 * compliance with applicable reexport restrictions.
 */
