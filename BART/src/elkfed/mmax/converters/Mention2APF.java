/*
 * Copyright (c) 2007 The MITRE Corporation
 *
 * See the full RIGHTS STATEMENT at end of this file.
 *
 * Author: David Day
 * Organization: The MITRE Corporation
 * Date: April 23, 2007
 *
 */

// package org.mitre.timex2;
package elkfed.mmax.converters;

import java.io.*;

// We use sax to parse Inline ACE annotation input file
import org.xml.sax.*;
import org.xml.sax.helpers.*;

public class Mention2APF extends Converter
{
    private static final String APF_DIR = "responses-apf";
    
    public static String inFilename     = null;
    public static String apfFilename    = null;
    public static String apfDocId       = null;
    public static String signalFilename = null;
    
    private final XMLReader parser;
    
    public Mention2APF()
    { this.parser = initParser(); }
    
    protected FilenameFilter getFilter()
    { return MentionFilenameFilter.FILTER_INSTANCE; }
    
    protected String getOutputBaseDir()
    { return APF_DIR; }
    
    protected String getOutputExtension()
    { return APFFilenameFilter.APF_EXT; }
    
    @Override
    protected void mkOutputDir(File corpusDir)
    { super.mkOutputDir(corpusDir.getParentFile()); }
    
    public void dumpDocument(String input)
    { dumpDocument(input, getOutputFile(input)); }
    
    public void dumpDocument(String input, String output)
    { dumpDocument(input, output, getBaseFileName(input)); }

    public void dumpDocument(String input, String output, String docId)
    { dumpDocument(input, output, docId, input); }
    
    public void dumpDocument(String input, String output, String docId, String sourceFile)
    {
        signalFilename = sourceFile;
        try
        {
            LOGGER.info("Converting " + input + " to APF format");
            parser.parse(
                new InputSource(
                    new FileReader(new File(input))
                )
            );
            ((MentionHandler)parser.getContentHandler()).createAPF(output);
        }
        catch (Exception e)
        {
	    System.err.println("Could not init parse " + input + " " + e);
	    e.printStackTrace();
        }
    }
    
    private XMLReader initParser()
    {
	try {
	    XMLReader parser = XMLReaderFactory.createXMLReader(); 
	    MentionHandler handler = new MentionHandler();
	    parser.setContentHandler(handler);
            return parser;
	}

	catch (Exception e) {
	    System.err.println("Could not init SAX parser " + e);
	    e.printStackTrace();
            return null;
	}
    }
    
    public static void main(String[] args) {

        // quick and dirty testing TOREMOVE :-)
        for (File file : new File("/tmp/response-inline").listFiles())
        { new Mention2APF().dumpDocument(file.getAbsolutePath()); }
        System.exit(666); // za nambah of da bist!!!
        
	if (args.length == 0) {
	    emitHelp();
	    System.exit(0);
	} else {
	    for (int i = 0; i < args.length; i++) {
		if (args[i].equalsIgnoreCase("-i")) {
		    if (args.length > i+1) {
			inFilename = args[i+1];
			i++;
		    } else {
			System.err.println("No filename provided to the -i command line argument.");
			System.exit(0);
		    }
		} else if (args[i].equalsIgnoreCase("-o")) {
		    if (args.length > i+1) {
			apfFilename = args[i+1];
			i++;
		    } else {
			System.err.println("No filename provided to the -o command line argument.");
			System.exit(0);
		    }
		} else if (args[i].equalsIgnoreCase("-sourcefile")) {
		    if (args.length > i+1) {
			signalFilename = args[i+1];
			i++;
		    } else {
			System.err.println("No filename provided to the -sourcefile command line argument.");
			System.exit(0);
		    }
		} else if (args[i].equalsIgnoreCase("-docid")) {
		    if (args.length > i+1) {
			apfDocId = args[i+1];
			i++;
		    } else {
			System.err.println("No docid provided to the -docid command line argument.");
			System.exit(0);
		    }
		} else if (args[i].equalsIgnoreCase("-h") || args[i].equalsIgnoreCase("-help")) {
		    emitHelp();
		    System.exit(0);
		} else {
		    System.err.println("Unrecognized argument: " + args[i]);
		    emitHelp();
		    System.exit(0);
		}
	    }
	}


	if (apfDocId == null) {
	    System.err.println("No docid provided with the -docid command line argument.");
	    emitHelp();
	    System.exit(0);
	}

	if (signalFilename == null) {
	    signalFilename = inFilename;
	    System.out.println("Signal file assumed to be same as input file: " + signalFilename);
	}

	try {
	    XMLReader parser = XMLReaderFactory.createXMLReader(); 
	    MentionHandler handler = new MentionHandler();
	    parser.setContentHandler(handler);

	    System.out.println("Inline MENTION annotations being extracted from:" + inFilename);
	    InputStream       inStream     = new FileInputStream(inFilename);
	    InputStreamReader inStrmRdr    = new InputStreamReader (inStream, "UTF8");
	    BufferedReader    inStrmBufRdr = new BufferedReader(inStrmRdr);
	    InputSource       xmlSource    = new InputSource(inStrmBufRdr);

	    parser.parse(xmlSource);
	    inStream.close();

	    handler.createAPF(apfFilename);

	}

	catch (Exception e) {
	    System.err.println(e);
	    e.printStackTrace();
	}
    }

    public static void emitHelp () {
	System.out.println("Inline2APFConverter -i <inline XML MENTION annotations filename>");
	System.out.println("                    -o <ACE APF output filename>");
	System.out.println("                    -docid <docid used in APF file and to identify signal file>");
	System.out.println("                    -sourcefile <signal file> (URI attribute of APF source_file,");
	System.out.println("                                               defaults to -i input file)");
	System.out.println("                    -h (print this message and exit)");
    }
}

/*
 * ------------------   RIGHTS STATEMENT   --------------------------
 *
 *                   Copyright (c) 2002-2007
 *                    The MITRE Corporation
 *
 *                      ALL RIGHTS RESERVED
 *
 *
 * The MITRE Corporation (MITRE) provides this software to you without
 * charge to use for your internal purposes only. Any copy you make for
 * such purposes is authorized provided you reproduce MITRE's copyright
 * designation and this License in any such copy. You may not give or
 * sell this software to any other party without the prior written
 * permission of the MITRE Corporation.
 *
 * The government of the United States of America may make unrestricted
 * use of this software.
 *
 * This software is the copyright work of MITRE. No ownership or other
 * proprietary interest in this software is granted you other than what
 * is granted in this license.
 *
 * Any modification or enhancement of this software must inherit this
 * license, including its warranty disclaimers. You hereby agree to
 * provide to MITRE, at no charge, a copy of any such modification or
 * enhancement without limitation.
 *
 * MITRE IS PROVIDING THE PRODUCT "AS IS" AND MAKES NO WARRANTY, EXPRESS
 * OR IMPLIED, AS TO THE ACCURACY, CAPABILITY, EFFICIENCY,
 * MERCHANTABILITY, OR FUNCTIONING OF THIS SOFTWARE AND DOCUMENTATION. IN
 * NO EVENT WILL MITRE BE LIABLE FOR ANY GENERAL, CONSEQUENTIAL,
 * INDIRECT, INCIDENTAL, EXEMPLARY OR SPECIAL DAMAGES, EVEN IF MITRE HAS
 * BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * You accept this software on the condition that you indemnify and hold
 * harmless MITRE, its Board of Trustees, officers, agents, and
 * employees, from any and all liability or damages to third parties,
 * including attorneys' fees, court costs, and other related costs and
 * expenses, arising out of your use of this software irrespective of the
 * cause of said liability.
 *
 * The export from the United States or the subsequent reexport of this
 * software is subject to compliance with United States export control
 * and munitions control restrictions. You agree that in the event you
 * seek to export this software you assume full responsibility for
 * obtaining all necessary export licenses and approvals and for assuring
 * compliance with applicable reexport restrictions.
 */
