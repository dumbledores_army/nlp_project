/*
 * Copyright 2007 Yannick Versley / Univ. Tuebingen
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package elkfed.mmax.minidisc;

/** orders markables by their start position.
 * if start positions are equal, smaller markables are
 * ordered before larger ones
 *
 * @author versley
 */
public class DiscourseOrderMarkableComparator
    implements java.util.Comparator<Markable> {

    @Override
    public boolean equals(Object o) {
        if (o.getClass() == this.getClass()) {
            return true;
        } else {
            return false;
        }
    }
    public int compare(Markable o1, Markable o2) {
        int cmp_start=o1.getLeftmostDiscoursePosition() -
                o2.getLeftmostDiscoursePosition();
        if (cmp_start!=0) return cmp_start;
        return o1.getRightmostDiscoursePosition()-
                o2.getRightmostDiscoursePosition();
    }

    @Override
    public int hashCode() {
        int hash = 0xe1e4fed;
        return hash;
    }
    
}
