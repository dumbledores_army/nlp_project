/*
 * Copyright 2007 EML Research
 * Copyright 2007 Yannick Versley / Univ. Tuebingen
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package elkfed.mmax.pipeline.taggers;

import java.io.IOException;
import crf.chunker.*;

import elkfed.config.ConfigProperties;
import elkfed.mmax.DiscourseUtils;
import java.util.ArrayList;
import java.util.List;
import elkfed.mmax.minidisc.Markable;
import elkfed.mmax.minidisc.MarkableLevel;

import java.io.File;
import static elkfed.mmax.MarkableLevels.DEFAULT_POS_LEVEL;
import static elkfed.mmax.MarkableLevels.DEFAULT_CHUNK_LEVEL;

/**
 * Uses the CRFChunker to chunk a MMAX2Discourse document.
 *
 * This component requires sentence and part of speech markables.  
 * {@link elkfed.mmax.pipeline.SentenceDetector} and {@link elkfed.mmax.pipeline.POSTagger}
 * currently provide this data.
 * 
 * @author ponzetsp
 */
public class CRFChunker extends SequenceTagger {

    /** The tagger model */
    private static final String DEFAULT_MODELDIR = "./models/chunker";
    private Maps chunkerMaps;
    private Model chunkerModel;

    /**
     * Creates a new instance of Chunker
     */
    public CRFChunker() {
        this(new File(ConfigProperties.getInstance().getRoot(),DEFAULT_MODELDIR).getAbsolutePath());
    }

    /**
     * Creates a new instance of Chunker
     */
    public CRFChunker(String modelDir) {
        super();
        Option chunkerOpt = new Option(modelDir);
        if (!chunkerOpt.readOptions()) {
            throw new RuntimeException("cannot run chunker; modelDir="+modelDir);
        }
        chunkerMaps = new Maps();
        Dictionary chunkerDict = new Dictionary();
        FeatureGen chunkerFGen = new FeatureGen(chunkerMaps, chunkerDict);
        Viterbi chunkerVtb = new Viterbi();
        chunkerModel = new Model(chunkerOpt, chunkerMaps, chunkerDict, chunkerFGen, chunkerVtb);
        if (!chunkerModel.init()) {
            throw new RuntimeException("Cannot load CRFChunker model from " + modelDir);
        }
    }

    /** Returns the markable level for chunking data */
    public String getLevelName() {
        return DEFAULT_CHUNK_LEVEL;
    }

    protected void tag() {
        try {
            //File tmpFile = File.createTempFile("crfchunk_in", ".pos.txt");
            ChunkingData chunkerData = new ChunkingData();
            putInput(chunkerData);
            chunkerData.cpGen(chunkerMaps.cpStr2Int);
            chunkerModel.inferenceAll(chunkerData.data);
            getResults(chunkerData);
        } catch (IOException e) {
            throw new RuntimeException("Chunker failed:", e);
        }
    }

    private void putInput(ChunkingData data) throws IOException {
        List<List<Observation>> myData=new ArrayList<List<Observation>>();
        final String[][] sentences = DiscourseUtils.getSentenceTokens(currentDocument);
        final String[][] sentenceIDs = 
                DiscourseUtils.getSentenceTokenIDs(currentDocument);
        final MarkableLevel posLevel =
                currentDocument.getMarkableLevelByName(DEFAULT_POS_LEVEL);
        for (int sentence = 0; sentence < sentences.length; sentence++) {
            List<Observation> sentData=new ArrayList<Observation>();
            for (int token = 0; token < sentences[sentence].length; token++) {
                final String posTag = ((Markable) posLevel.getMarkablesAtDiscourseElementID(
                        sentenceIDs[sentence][token], null).get(0)).getAttributeValue(TAG_ATTRIBUTE, "").toUpperCase();
                Observation obsr=new Observation();
                obsr.originalData=String.format("%s/%s",
                        sentences[sentence][token],posTag);
                sentData.add(obsr);       
            }
            myData.add(sentData);
        }
        data.data=myData;
    }

    private void getResults(ChunkingData chunkerData) {
        for (List<Observation> seq: (List<List<Observation>>)chunkerData.data) {
            for (Observation obs: seq) {
                String lab=(String)chunkerMaps.lbInt2Str.get(obs.modelLabel);
                System.out.println("chunker:"+lab);
                tags.add(lab.toUpperCase());
            }
        }
    }
}
