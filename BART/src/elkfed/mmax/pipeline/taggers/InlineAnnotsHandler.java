/*
 * Copyright (c) 2007 The MITRE Corporation
 *
 * See the full RIGHTS STATEMENT at end of this file.
 *
 * Author: David Day
 * Organization: The MITRE Corporation
 * Date: July, 2007
 *
 */
package elkfed.mmax.pipeline.taggers;

import java.io.*;
import java.util.*;

import org.xml.sax.*;
import org.xml.sax.helpers.*;

public class InlineAnnotsHandler extends DefaultHandler {

    static String eType        = null;
    static String mType        = null;
    static String posType      = null;
    static String BIOState     = null;
    static String noMentionBIO = " O O O";
    static String BIOBegin     = "B";
    static String BIOInternal  = "I";
    static String BIOOutside   = "O";

    public StringBuffer signalBuffer = new StringBuffer();
    public String signalString       = null;


    public void startElement(String namespaceURI, String localName,
			     String qualifiedName, Attributes attrs)
	throws SAXException {
        
        
	if (localName.equalsIgnoreCase("S")) {
	    // Don't do anything. Let the closing S tag handle this.
	} else if (localName.equalsIgnoreCase("GPE") || localName.equalsIgnoreCase("LOC") ||
		   localName.equalsIgnoreCase("PER") || localName.equalsIgnoreCase("ORG") ||
		   localName.equalsIgnoreCase("FAC") || localName.equalsIgnoreCase("WEA") ||
		   localName.equalsIgnoreCase("VEH")) {
	    eType = localName;
	    mType = getAttributeFromList(attrs, "mtype");
	    BIOState = BIOBegin;
	} else if (localName.equalsIgnoreCase("LEX")) {
	    posType = getAttributeFromList(attrs, "pos");
	    signalBuffer = new StringBuffer();
	}
    }


    public void endElement(String uri, String localName, String qName)
	throws SAXException {

	try {
	    if (localName.equalsIgnoreCase("S")) {
		if (CarafembicWrapper.writeBioToFile) {
		    CarafembicWrapper.BIOWriter.write("\n");
		} // else {
		    // for (int i = 0; i < 5; i++) {
			// CarafembicWrapper.BIOLexTable[CarafembicWrapper.bioLexCount][i] = "";

		    // }
		    // CarafembicWrapper.bioLexCount++;
		// }
	    } else if (localName.equalsIgnoreCase("GPE") || localName.equalsIgnoreCase("LOC") ||
		       localName.equalsIgnoreCase("PER") || localName.equalsIgnoreCase("ORG") ||
		       localName.equalsIgnoreCase("FAC") || localName.equalsIgnoreCase("WEA") ||
		       localName.equalsIgnoreCase("VEH")) {
		eType = null;
		mType = null;
		BIOState = BIOOutside;
	    } else if (localName.equalsIgnoreCase("LEX")) {
		if (signalBuffer != null) {
		    signalString = signalBuffer.toString();
		}
		signalBuffer = new StringBuffer();
		if (eType == null) {
		    if (CarafembicWrapper.writeBioToFile) {
			CarafembicWrapper.BIOWriter.write(signalString + " " + posType + noMentionBIO + "\n");
		    } else {
			CarafembicWrapper.BIOLexTable[CarafembicWrapper.bioLexCount][0] = signalString;
			CarafembicWrapper.BIOLexTable[CarafembicWrapper.bioLexCount][1] = posType;
			CarafembicWrapper.BIOLexTable[CarafembicWrapper.bioLexCount][2] = "O";
			CarafembicWrapper.BIOLexTable[CarafembicWrapper.bioLexCount][3] = "O";
			CarafembicWrapper.BIOLexTable[CarafembicWrapper.bioLexCount][4] = "O";
			CarafembicWrapper.bioLexCount++;
		    }
		} else {
		    if (CarafembicWrapper.writeBioToFile) {
			CarafembicWrapper.BIOWriter.write(signalString + " " + posType + " " +
							  BIOState + " " +
							  eType + " " +
							  mType + "\n");
		    } else {
			CarafembicWrapper.BIOLexTable[CarafembicWrapper.bioLexCount][0] = signalString;
			CarafembicWrapper.BIOLexTable[CarafembicWrapper.bioLexCount][1] = posType;
			CarafembicWrapper.BIOLexTable[CarafembicWrapper.bioLexCount][2] = BIOState;
			CarafembicWrapper.BIOLexTable[CarafembicWrapper.bioLexCount][3] = eType;
			CarafembicWrapper.BIOLexTable[CarafembicWrapper.bioLexCount][4] = mType;
			CarafembicWrapper.bioLexCount++;
		    }
		    BIOState = BIOInternal;
		}
	    }
	}

	catch (Exception e) {
	    System.err.println(e);
	    e.printStackTrace();
	}
    }


    public void characters(char[] chars, int start, int length) throws SAXException {
	if (signalBuffer == null) {
	    signalBuffer = new StringBuffer();
	}
	signalBuffer.append(chars, start, length);
    }


    public String getAttributeFromList(Attributes attrs, String attrName) {
	for (int i = 0; i < attrs.getLength(); i++) {
	    String name  = attrs.getQName(i);
	    if (name.equalsIgnoreCase(attrName)) {
		String type  = attrs.getType(i);
		String value = attrs.getValue(i);
		// System.out.println("extracted attribute value " + value + " from attribute " + attrName);
		return value;
	    }
	}
	// No matching attribute name found.
	return null;
    }

}

/*
 * ----------------   RIGHTS STATEMENT   ----------------------------
 *
 *                   Copyright (c) 2007
 *                 The MITRE Corporation
 *
 *                  ALL RIGHTS RESERVED
 *
 *
 * The MITRE Corporation (MITRE) provides this software to you without
 * charge to use for your internal purposes only. Any copy you make for
 * such purposes is authorized provided you reproduce MITRE's copyright
 * designation and this License in any such copy. You may not give or
 * sell this software to any other party without the prior written
 * permission of the MITRE Corporation.
 *
 * The government of the United States of America may make unrestricted
 * use of this software.
 *
 * This software is the copyright work of MITRE. No ownership or other
 * proprietary interest in this software is granted you other than what
 * is granted in this license.
 *
 * Any modification or enhancement of this software must inherit this
 * license, including its warranty disclaimers. You hereby agree to
 * provide to MITRE, at no charge, a copy of any such modification or
 * enhancement without limitation.
 *
 * MITRE IS PROVIDING THE PRODUCT "AS IS" AND MAKES NO WARRANTY, EXPRESS
 * OR IMPLIED, AS TO THE ACCURACY, CAPABILITY, EFFICIENCY,
 * MERCHANTABILITY, OR FUNCTIONING OF THIS SOFTWARE AND DOCUMENTATION. IN
 * NO EVENT WILL MITRE BE LIABLE FOR ANY GENERAL, CONSEQUENTIAL,
 * INDIRECT, INCIDENTAL, EXEMPLARY OR SPECIAL DAMAGES, EVEN IF MITRE HAS
 * BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * You accept this software on the condition that you indemnify and hold
 * harmless MITRE, its Board of Trustees, officers, agents, and
 * employees, from any and all liability or damages to third parties,
 * including attorneys' fees, court costs, and other related costs and
 * expenses, arising out of your use of this software irrespective of the
 * cause of said liability.
 *
 * The export from the United States or the subsequent reexport of this
 * software is subject to compliance with United States export control
 * and munitions control restrictions. You agree that in the event you
 * seek to export this software you assume full responsibility for
 * obtaining all necessary export licenses and approvals and for assuring
 * compliance with applicable reexport restrictions.
 */
