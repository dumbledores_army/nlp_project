/*
 * Copyright (c) 2007 The MITRE Corporation
 *
 * See the full RIGHTS STATEMENT at end of this file.
 *
 * Author: David Day
 * Organization: The MITRE Corporation
 * Date: July, 2007
 *
 */
package elkfed.mmax.pipeline.taggers;

import java.io.*;
import java.util.*;

import org.xml.sax.*;
import org.xml.sax.helpers.*;

public class CarafembicWrapper {

    // Recognized file types: html, ace, muc, text
    public static String fileType          = "html";
    public static String inputFilename     = null;
    public static String execString        = null;
    public static String carafembicScript  = null;
    public static String carafembicACEScript  =
	"/export/ws07elerfed/software/carafembic/systems/carafembic/utilities/scripts/run-carafe-ace.csh";
    public static String carafembicHTMLScript =
	"/export/ws07elerfed/software/carafembic/systems/carafembic/utilities/scripts/run-elerfed-carafembic-html.csh";
    public static String carafembicTextScript =
	"/export/ws07elerfed/software/carafembic/systems/carafembic/utilities/scripts/run-elerfed-carafembic-text.csh";
    public static Random randomGenerator   = new Random();
    public static int    randomInt         = 0;
    public static String outputFilePrefix  = "/tmp/carafembic-output-";
    public static String outputFilename    = null;
    public static String BIOFilename       = null;
    public static BufferedWriter BIOWriter = null;
    public static boolean   writeBioToFile = false;
    public static int          bioLexCount = 0;
    public static int        MAX_LEX_COUNT = 25000;
    public static String [][]  BIOLexTable = new String[MAX_LEX_COUNT][5];

    public static String testText = "China late this year will start building a nuclear-powered\ndesalinator project in Yantai City in east China's Shandong Province.";
    
    // public static String testText = "\nJINAN, June 24 (Xinhua)\n\nChina late this year will start building a nuclear-powered\ndesalinator project in Yantai City in east China's Shandong Province,\nwith a designed capacity of producing 52 million tons of fresh water\na year.\n\nOfficials with the Development Planning Commission of the Shandong\nprovincial government said the project, to be built in cooperation\nwith Qinghua University, would alleviate water shortages in the city\nand other parts of the Shandong Peninsula.\n\nYantai and other parts of Shandong suffered acute water shortages due\nto droughts in recent years, changing climate and other reasons.\n\nApproved by the State Development and Reform Commission, the project\nis expected to cost 1.6 billion yuan (nearly 200 million US dollars)\n\n";
    

    public static void main(String[] args) throws IOException {

	// By the very fact of running main, we assume user wants BIO output to
	// be written to an output file.  This can be overridden with the
	// -testnofiles command-line argument.
	writeBioToFile = true;

	if (args.length == 0) {
	    emitHelp();
	    System.exit(0);
	} else {
	    for (int i = 0; i < args.length; i++) {
		if (args[i].equalsIgnoreCase("-i")) {
		    if (args.length > i+1) {
			inputFilename = args[i+1];
			i++;
		    } else {
			System.err.println("No value provided to -i command line argument.");
			System.exit(0);
		    }
		} else if (args[i].equalsIgnoreCase("-o")) {
		    if (args.length > i+1) {
			BIOFilename = args[i+1];
			i++;
		    } else {
			System.err.println("No value provided to -o command line argument.");
			System.exit(0);
		    }
		} else if (args[i].equalsIgnoreCase("-filetype")) {
		    if (args.length > i+1) {
			fileType = args[i+1];
			i++;
			if (!(fileType.equalsIgnoreCase("html") ||
			      fileType.equalsIgnoreCase("spock") ||
			      fileType.equalsIgnoreCase("semeval") ||
			      fileType.equalsIgnoreCase("ace") ||
			      fileType.equalsIgnoreCase("text") ||
			      fileType.equalsIgnoreCase("txt") ||
			      fileType.equalsIgnoreCase("muc"))) {
			    System.err.println("Unrecognized filetype (" + fileType + ").");
			    emitHelp();
			    System.exit(0);
			}
		    } else {
			System.err.println("No value provided to -filetype command line argument.");
			System.exit(0);
		    }
		} else if (args[i].equalsIgnoreCase("-script")) {
		    if (args.length > i+1) {
			carafembicScript = args[i+1];
			i++;
		    } else {
			System.err.println("No value provided to -script command line argument.");
			System.exit(0);
		    }
		} else if (args[i].equalsIgnoreCase("-testnofiles")) {
		    writeBioToFile = false;
		} else if (args[i].equalsIgnoreCase("-h") || args[i].equalsIgnoreCase("-help")) {
		    emitHelp();
		    System.exit(0);
		} else {
		    System.err.println("Unrecognized argument: " + args[i]);
		    System.exit(0);
		}
	    }
	}

	if (inputFilename == null) {
	    System.err.println("No input file specified with the -i argument.");
	    System.exit(0);
	}

	File inFile = new File(inputFilename);
	if (!inFile.exists()) {
	    System.err.println("Input file not found: " + inputFilename);
	    System.exit(0);
	}

	if (BIOFilename == null) {
	    System.err.println("No input file specified with the -o argument.");
	    System.exit(0);
	}

	setScriptForTextType(fileType);
	inFile = new File(carafembicScript);
	if (!inFile.exists()) {
	    System.err.println("Script file not found: " + carafembicScript);
	    System.exit(0);
	}

	if (writeBioToFile) {
	    runCarafembic(inputFilename);
	} else {
	    try {
		// String[][] BIOArray = new String[MAX_LEX_COUNT][5];
		String[][] BIOArray = runCarafembicOnString(testText);
		for (int i = 0; i < bioLexCount; i++) {
		    for (int j = 0; j < 5; j++) {
			System.out.print("[" + BIOArray[i][j] + "]");
		    }
		    System.out.println("");
		}
	    }
	    catch (Exception e) {
		System.err.println(e);
		e.printStackTrace();
	    }
	    
	}
    }
    

    public static void setScriptForTextType (String fileType) {

	if (fileType.equalsIgnoreCase("html") ||
	    fileType.equalsIgnoreCase("spock") ||
	    fileType.equalsIgnoreCase("semeval")) {
	    System.out.println("Setting Carafembic script to HTML version...");
	    carafembicScript  = carafembicHTMLScript;
	} else if (fileType.equalsIgnoreCase("text") ||
		   fileType.equalsIgnoreCase("txt")) {
	    System.out.println("Setting Carafembic script to TEXT version...");
	    carafembicScript  = carafembicTextScript;
	} else {
	    System.out.println("Setting Carafembic script to ACE version...");
	    carafembicScript  = carafembicACEScript;
	}
    }



    public static void emitHelp () {
	System.err.println("CarafembicWrapper");
	System.err.println("  -i <input-file-to-be-processed>");
	System.err.println("  -o <output-file-in-BIO-format>");
	System.err.println("  -filetype <file-type-indicator>");
	System.err.println("      Argument value needs to be one of:");
	System.err.println("      html, spock, semeval, ace, text, txt, muc.");
	System.err.println("      spock and semeval are treated the same as html");
	System.err.println("      text is expected to be raw text with no SGML/XML markup");
	System.err.println("  -testnofiles (for developers only)");
	System.err.println("  -h  print this help information");
	System.err.println("  -script <carafembic-script>");
	System.err.println("      use this in case an alternative is desired to the");
	System.err.println("      hard-coded script.  The hard-coded script is here:");
	System.err.println("  " + carafembicScript);
    }


    public static String[][] runCarafembicOnString (String textString) throws IOException {
	writeBioToFile = false;
	fileType       = "text";
	setScriptForTextType(fileType);
	
	try {
	    // Write string to temporary file, since Carafembic script expects an input file.
	    randomInt      = randomGenerator.nextInt(1000);
	    outputFilename = new String(outputFilePrefix + randomInt + ".txt");
	    OutputStream  outStream = new FileOutputStream(outputFilename);
	    OutputStreamWriter outStrmWrtr = new OutputStreamWriter (outStream, "UTF-8");
	    BufferedWriter TmpWriter = new BufferedWriter(outStrmWrtr);
	    TmpWriter.write(textString);
	    TmpWriter.flush();
	    TmpWriter.close();
	
	    // Now invoke shared script on this as input file
	    runCarafembic(outputFilename);
	}
	catch (Exception e) {
	    System.err.println(e);
	    e.printStackTrace();
	}
	return BIOLexTable;
    }

    public static void callCaraf(String inputFilename, String outputFilename)
    {
      BIOLexTable = new String[MAX_LEX_COUNT][5];
        carafembicScript = carafembicACEScript;
        outputFilename = outputFilename;
        writeBioToFile = false;
        BIOFilename = "out.BIO.out";
        	   System.out.println("input file " + inputFilename);
        runCarafembic(inputFilename);
    }
    
    public static void runCarafembic(String inputFilename) {

	Runtime process = Runtime.getRuntime();
	try {
	   System.out.println("input file " + inputFilename);
            // -------------------------------------------------------------
	    // Create a temporary file to which to write Carafembic results
	    randomInt      = randomGenerator.nextInt(1000);
	    outputFilename = new String(outputFilePrefix + randomInt + ".ne-carafe.xml");

	    // Create and execute Carafembic script
	    execString     = new String("/bin/csh " + carafembicScript + " " + inputFilename + " " + outputFilename);
	    System.out.println("About to execute: " + execString);
	    Process execProcess = process.exec(execString);

	    BufferedReader readResult = new BufferedReader ( new InputStreamReader(execProcess.getInputStream()));
	    String scriptMessages = null;
	    System.out.println("Script messages:");
	    while ((scriptMessages = readResult.readLine()) != null) {
		System.out.println("script: " + scriptMessages);
	    }
	    readResult.close();

	    // -------------------------------------------------------------
	    // Open for reading the mention-tagged file
	    XMLReader parser = XMLReaderFactory.createXMLReader(); 
	    InlineAnnotsHandler handler = new InlineAnnotsHandler();
	    parser.setContentHandler(handler);
	    
	    System.out.println("Transform inline XML annotations to BIO format...");
	    InputStream       inStream     = new FileInputStream(outputFilename);
	    InputStreamReader inStrmRdr    = new InputStreamReader (inStream, "UTF8");
	    BufferedReader    inStrmBufRdr = new BufferedReader(inStrmRdr);
	    InputSource       xmlSource    = new InputSource(inStrmBufRdr);

	    if (writeBioToFile) {
		// Write BIO-formatted output file while parsing mention-tagged file
		System.out.println("Writing BIO");
                OutputStream         outStream = new FileOutputStream(BIOFilename);
		OutputStreamWriter outStrmWrtr = new OutputStreamWriter (outStream, "UTF-8");
		BIOWriter                      = new BufferedWriter(outStrmWrtr);
	    }
	    

	    parser.parse(xmlSource);
	    inStream.close();

	    if (writeBioToFile) {
		BIOWriter.flush();
		BIOWriter.close();
		System.out.println("BIO-formatted tokens, part-of-speech and ACE mention annotations placed in file:");
		System.out.println(BIOFilename);
	    }
	}

	catch (Exception e) {
	    System.err.println(e);
	    e.printStackTrace();
	}
    }

}

/*
 * ------------------   RIGHTS STATEMENT   --------------------------
 *
 *                   Copyright (c) 2007
 *                 The MITRE Corporation
 *
 *                  ALL RIGHTS RESERVED
 *
 *
 * The MITRE Corporation (MITRE) provides this software to you without
 * charge to use for your internal purposes only. Any copy you make for
 * such purposes is authorized provided you reproduce MITRE's copyright
 * designation and this License in any such copy. You may not give or
 * sell this software to any other party without the prior written
 * permission of the MITRE Corporation.
 *
 * The government of the United States of America may make unrestricted
 * use of this software.
 *
 * This software is the copyright work of MITRE. No ownership or other
 * proprietary interest in this software is granted you other than what
 * is granted in this license.
 *
 * Any modification or enhancement of this software must inherit this
 * license, including its warranty disclaimers. You hereby agree to
 * provide to MITRE, at no charge, a copy of any such modification or
 * enhancement without limitation.
 *
 * MITRE IS PROVIDING THE PRODUCT "AS IS" AND MAKES NO WARRANTY, EXPRESS
 * OR IMPLIED, AS TO THE ACCURACY, CAPABILITY, EFFICIENCY,
 * MERCHANTABILITY, OR FUNCTIONING OF THIS SOFTWARE AND DOCUMENTATION. IN
 * NO EVENT WILL MITRE BE LIABLE FOR ANY GENERAL, CONSEQUENTIAL,
 * INDIRECT, INCIDENTAL, EXEMPLARY OR SPECIAL DAMAGES, EVEN IF MITRE HAS
 * BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * You accept this software on the condition that you indemnify and hold
 * harmless MITRE, its Board of Trustees, officers, agents, and
 * employees, from any and all liability or damages to third parties,
 * including attorneys' fees, court costs, and other related costs and
 * expenses, arising out of your use of this software irrespective of the
 * cause of said liability.
 *
 * The export from the United States or the subsequent reexport of this
 * software is subject to compliance with United States export control
 * and munitions control restrictions. You agree that in the event you
 * seek to export this software you assume full responsibility for
 * obtaining all necessary export licenses and approvals and for assuring
 * compliance with applicable reexport restrictions.
 */
