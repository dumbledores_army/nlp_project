/*
 * CarafembicNER.java
 *
 * Created on July 27, 2007, 1:38 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package elkfed.mmax.pipeline.taggers;


import edu.stanford.nlp.ling.Word;
import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ling.Sentence;
import elkfed.mmax.DiscourseUtils;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.*;
import elkfed.mmax.minidisc.Markable;
import elkfed.mmax.minidisc.MarkableLevel;

import static elkfed.mmax.MarkableLevels.DEFAULT_POS_LEVEL;
import static elkfed.mmax.MarkableLevels.DEFAULT_ENAMEX_LEVEL;

/**
 *
 * @author Vlad
 */
public class CarafembicNER extends SequenceTagger {

    private static boolean DEBUG = true;
     public static BufferedWriter BIOWriter = null;
         static String openLex = "<lex pos=\"";
         static String openLexCont = "\">";
         static String closeLex = "</lex>";
         
         static String openDoc = "<DOC>";
         static String closeDoc = "</DOC>";
         static String openText = "<TEXT>";
         static String closeText = "</TEXT>";
         static String openSen = "<S>";
         static String closeSen = "</S>";
         
        //Carafe NEEDS THE FULL PATH to the input file, this needs to be specified in config or somewhere TODO
          static String outputFileName = new String("/export/ws07elerfed/software/carafembic/testdata/BIO2InlineXML/toProcess.xml");
           static String fileName= new String("out.out");
     
            private AbstractSequenceClassifier classifier;
      // static private CarafembicWrapper cw = null;
  
       private ArrayList<ArrayList> _toCarafembic;
       private ArrayList<String> _pos;
       private ArrayList<String> _names;
       static int sentenceCount;

     /** Creates a new instance of NER */
    public CarafembicNER()
    {}
    
    
   /**
    * Create internal representation from Carafe output
    */ 
    private static void processBIO(CarafembicWrapper cw){
   if (DEBUG){       
       System.out.println("Process BIO "+ cw.bioLexCount);
       int i=0,j=0; 
       sentenceCount =0;
       while(cw.BIOLexTable[i][0] != null)
        {
        if(cw.BIOLexTable[i][0] == "" && cw.BIOLexTable[i][1]=="" && cw.BIOLexTable[i][2] == "")
        {i++;
        sentenceCount++;
        }else
        {
             //System.out.print(cw.BIOLexTable[i][j] + " ");
            j++;

            if(j%5==0)
            {
           //     System.out.println();
                j=0;
                i++;
            }
        }
        }
    
       System.out.println("Sentence count " + sentenceCount);
       System.out.println("Element added from Bio " + i);
  
    }
    }
    
    /**
     *Using CarafembicWrapper, run Carafe script on ACE data
     *
     */
    private CarafembicWrapper callCarafembic()
    {
        String carafembicACEScript  =
	"/export/ws07elerfed/software/carafembic/systems/carafembic/utilities/scripts/run-carafe-ace.csh";

          
    CarafembicWrapper _cw = new CarafembicWrapper();
    String[] par  = {"-i", outputFileName,"-o",fileName, "-script", carafembicACEScript,"-filetype","ace"};
        
    
    try {
           
          _cw.callCaraf(outputFileName,fileName);
          
          processBIO(_cw); 
          
          return _cw;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
       
      return null;
    }
    
    
    /**
     * Create xml input file for Carafe script, using 2 column internal data structure of lexeme-POS tuples
     */
    private void generateXML(ArrayList<ArrayList> _toCarafembic){
       
        StringBuffer sb = new StringBuffer();
    
        try{

    
 PrintWriter out
   = new PrintWriter(new BufferedWriter(new FileWriter(outputFileName)));
 
        sb.append(openDoc + "\n" + openText + "\n" + openSen );
     
        for(int i =0; i < _toCarafembic.get(0).size();i++)
        {
            if(_toCarafembic.get(0).get(i).equals(""))
           
            {
                sb.append(closeSen + "\n" + openSen);
            }
           
            else
           {
               sb.append
                       (openLex + _toCarafembic.get(1).get(i) + openLexCont + _toCarafembic.get(0).get(i) + closeLex);
           }

        }

    sb.append(closeSen + "\n" + closeText + "\n" + closeDoc);
    out.write(sb.toString());        
    out.close();
       }catch(Exception e){}
        
    }
    
    
     /** Returns the markable level for entity names */
    public String getLevelName() {
        return DEFAULT_ENAMEX_LEVEL;
    }

    protected void tag()
    {
       tags.clear();
clearBuffers();
       int i =0;
       CarafembicWrapper.bioLexCount =0 ;
      
        ArrayList<ArrayList> toCarafembic = new ArrayList<ArrayList>();
        ArrayList<String> _lexemes = new ArrayList<String>();
        ArrayList<String> _pos = new ArrayList<String>();
        System.out.println("Creating Tuple-Lists for Carafe");
        
        final String[][] sentences = DiscourseUtils.getSentenceTokens(currentDocument);
        final String[][] sentenceIDs =
                DiscourseUtils.getSentenceTokenIDs(currentDocument);
        
        final MarkableLevel posLevel =
                currentDocument.getMarkableLevelByName(DEFAULT_POS_LEVEL);

        for (int sentence = 0; sentence < sentences.length; sentence++)
        {
            for (int token = 0; token < sentences[sentence].length; token++)
            {
                final String word = sentences[sentence][token];
                final String posTag = ((Markable)
                    posLevel.getMarkablesAtDiscourseElementID(
                        sentenceIDs[sentence][token], null).get(0)).
                            getAttributeValue(TAG_ATTRIBUTE, "").toUpperCase();
                
                _lexemes.add(word);
                _pos.add(posTag);
            }
            _lexemes.add("");
            _pos.add("");
        }
    
      
        toCarafembic.add(_lexemes);
        toCarafembic.add(_pos);
        
        //create xml input file for Carafe NER 
        System.out.println("Generating XML");
        generateXML(toCarafembic);
        //run Carafe on xml file
        System.out.println("Calling Carafe");
        CarafembicWrapper _cw = callCarafembic();       
        System.out.println("Return from Carafe");
    
             i=0;
        /*
     * process created BIO file to crate internal data structure
     */    
        
     
       final Sentence tokens = new Sentence();
  
       
       
       while(_cw.BIOLexTable[i][0] != null)
        {
           //   if(_cw.BIOLexTable[i][0] == "" && _cw.BIOLexTable[i][1]=="" && _cw.BIOLexTable[i][2] == "")
            //  {i++;}
            //  else{
                  tokens.add(new Word(_cw.BIOLexTable[i][0])); 
                  tags.add(_cw.BIOLexTable[i][2] + "-" + _cw.BIOLexTable[i][3]);
                  tagAttributes.add(_cw.BIOLexTable[i][4]);
    
                  i++;
             //     }
       }
            
     }
    
  
    /** Add markables to the document: assumes IOB representation */
    protected void addMarkables()
    {
        final int docSize=currentDocument.getDiscourseElementCount();
        
        for (int token = 0; token < docSize-1; token++)
        {
            checkToken(token, tags.get(token), tags.get(token+1),tagAttributes.get(token));
         
        }
        checkToken(docSize-1, tags.get(tags.size()-1), null,tagAttributes.get(tagAttributes.size()-1));
    }
    
    
    protected void checkToken(int de, String tag, String nextTag,String tagAttribute)
    {
  
      if (!tag.startsWith("O-"))
        {    
          System.out.println("chunk " + chunkBegin);
            // 1. check for start
            if (tag.startsWith("B-")/* && tagAttribute.equals("NAM")*/)
            {
                System.out.println("Tag : " +tag);
                // we are at the beginning of a chunk
                currentChunk = tag.substring(2).toLowerCase();
                chunkBegin = de;
            }
            
            // 2. check for end
            if (/*tagAttribute.equals("NAM") && */(nextTag == null || nextTag.startsWith("B-") || nextTag.startsWith("O-")) )
            {
                
                // we are at the end of a chunk
                chunkEnd = de;
                 
                // create the markable
                buffer.setLength(0);
                final HashMap<String,String> attributes = new HashMap<String,String>(levelAttributes); 
                
              
                String addValue = currentChunk;
                if(currentChunk.startsWith("loc"))
                {
                addValue = "location";
                }
                else if(currentChunk.startsWith("org"))
                {
                addValue = "organization";
                }
                else if(currentChunk.startsWith("per"))
                {
                addValue = "person";
                }
                
                if(addValue != null)
                {    
                    
                 
                        attributes.put(TAG_ATTRIBUTE, addValue);
                        attributes.put("mtype",tagAttribute);
                        
                        System.out.println("adding " + tag + " " + addValue + " " + TAG_ATTRIBUTE + " " + tagAttribute);

                        currentLevel.addMarkable(chunkBegin,chunkEnd,attributes);
                }
            }
        }
    
     }
}

