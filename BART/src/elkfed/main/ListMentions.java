/*
 * ExtractData.java
 *
 * Created on August 7, 2007, 5:08 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package elkfed.main;

import elkfed.config.ConfigProperties;
import elkfed.coref.CorefTrainer;
import elkfed.coref.mentions.Mention;
import elkfed.coref.processors.TrainerProcessor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;

/**
 *
 * @author yannick
 */
public class ListMentions implements CorefTrainer {
    PrintStream _os;
    ListMentions(PrintStream os) {
        _os=os;
    }
        
    public void encodeDocument(List<Mention> mentions)
    throws IOException {
        _os.println("------------");
        for (int i=1; i<mentions.size(); i++) {
            Mention m_i=mentions.get(i);
            _os.format("%-20s(%s) %s\n",m_i.getMarkableString(),
                    m_i.getMarkable().getID(),
                    m_i.getSetID());
        }
        _os.flush();
    }
    
    public static void main(String[] args) {
        try {
            PrintStream out=new PrintStream(new FileOutputStream(
                    System.getProperty("elkfed.corpus","data")+".out"));
            ListMentions trainer=new ListMentions(out);
            TrainerProcessor proc=new TrainerProcessor(
                    trainer,
                    ConfigProperties.getInstance().getTrainingData(),
                    ConfigProperties.getInstance().getTrainingDataId(),
                    ConfigProperties.getInstance().getMentionFactory()
                    );
            proc.createTrainingData();
//            proc=new TrainerProcessor(
//                    trainer,
//                    ConfigProperties.getInstance().getTestData(),
//                    ConfigProperties.getInstance().getTestDataId(),
//                    ConfigProperties.getInstance().getMentionFactory()
//            );
//            proc.createTrainingData();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
