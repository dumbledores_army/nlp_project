/*
 * ExtractData.java
 *
 * Created on August 7, 2007, 5:08 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package elkfed.main;

import elkfed.config.ConfigProperties;
import elkfed.coref.CorefTrainer;
import elkfed.coref.PairFeatureExtractor;
import elkfed.coref.PairInstance;
import elkfed.coref.eval.SimpleLinkScorer;
import elkfed.coref.features.pairs.FE_SSPron;
import elkfed.coref.mentions.Mention;
import elkfed.coref.processors.TrainerProcessor;
import elkfed.ml.ClassifierFactory;
import elkfed.ml.FeatureDescription;
import elkfed.ml.InstanceWriter;
import elkfed.ml.stacking.StackingClassifierFactory;
import elkfed.ml.svm.SVMClassifierFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static elkfed.lang.EnglishLinguisticConstants.PRONOUN;

/**
 *
 * @author yannick
 */
public class Extract_SSPron implements CorefTrainer {
    InstanceWriter _out;
    List<PairFeatureExtractor> _fes;
    int fold_no=0;
    
    Extract_SSPron(InstanceWriter out)
        throws IOException
    {
        _out=out;
        _fes=FE_SSPron.getExtractors();
        ArrayList<FeatureDescription> fds = new ArrayList<FeatureDescription>();
        for (PairFeatureExtractor fe : _fes) {
            fe.describeFeatures(fds);
        }
        fds.add(PairInstance.FD_POSITIVE);
        _out.setHeader(fds);
    }

    public void encodeDocument(List<Mention> mentions)
    throws IOException {
        for (int i=1; i<mentions.size(); i++) {
            Mention m_i=mentions.get(i);
            if (m_i.getReflPronoun())
            {
                continue;
            }
            if (!SimpleLinkScorer.hasAnte(mentions,i))
                continue;
            for (int j=i-1;j>=0;j--) {
                Mention m_j=mentions.get(j);
                if (m_j.getReflPronoun() ||
                        m_j.getSentId()!=m_i.getSentId()) {
                    continue;
                }
                if (!m_i.getPronoun() && !m_j.getPronoun()) {
                    continue;
                }
                PairInstance inst=new PairInstance(m_i,m_j);
                for (PairFeatureExtractor fe: _fes) {
                    fe.extractFeatures(inst);
                }
                inst.setFeature(StackingClassifierFactory.FD_FOLD_NO, fold_no);
                inst.setFeature(PairInstance.FD_POSITIVE,m_i.isCoreferent(m_j));
                _out.write(inst);
                    
            }
        }
        _out.flush();
        fold_no++;
    }
    
    public static void main(String[] args) {
        try {
            ClassifierFactory fact=new StackingClassifierFactory(
                    SVMClassifierFactory.getInstance());
            InstanceWriter out=fact.getSink("ss_pron","","");
            Extract_SSPron trainer=new Extract_SSPron(out);
            TrainerProcessor proc=new TrainerProcessor(
                    trainer,
                    ConfigProperties.getInstance().getTrainingData(),
                    ConfigProperties.getInstance().getTrainingDataId(),
                    ConfigProperties.getInstance().getMentionFactory()
                    );
            proc.createTrainingData();
//            proc=new TrainerProcessor(
//                    trainer,
//                    ConfigProperties.getInstance().getTestData(),
//                    ConfigProperties.getInstance().getTestDataId(),
//                    ConfigProperties.getInstance().getMentionFactory()
//            );
//            proc.createTrainingData();
            fact.do_learning("ss_pron", "-b 1 -t 4 -d 2 -j 1.5", "");
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
