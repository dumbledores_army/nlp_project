/*
 * ExtractData.java
 *
 * Created on August 7, 2007, 5:08 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package elkfed.main;

import elkfed.config.ConfigProperties;
import elkfed.coref.CorefTrainer;
import elkfed.coref.PairInstance;
import elkfed.coref.features.pairs.FE_Expletive;
import elkfed.coref.mentions.Mention;
import elkfed.coref.processors.TrainerProcessor;
import elkfed.expletives.ExpletiveInstance;
import elkfed.ml.ClassifierFactory;
import elkfed.ml.FeatureDescription;
import elkfed.ml.FeatureExtractor;
import elkfed.ml.InstanceWriter;
import elkfed.ml.stacking.StackingClassifierFactory;
import elkfed.ml.svm.SVMClassifierFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static elkfed.lang.EnglishLinguisticConstants.PRONOUN;

/**
 *
 * @author yannick
 */
public class Extract_Expl implements CorefTrainer {
    InstanceWriter _out;
    List<FeatureExtractor<ExpletiveInstance> > _fes;
    int fold_no=0;
    
    Extract_Expl(InstanceWriter out)
        throws IOException
    {
        _out=out;
        _fes=FE_Expletive.getExtractors();
        ArrayList<FeatureDescription> fds = new ArrayList<FeatureDescription>();
        for (FeatureExtractor<ExpletiveInstance> fe : _fes) {
            fe.describeFeatures(fds);
        }
        fds.add(PairInstance.FD_POSITIVE);
        _out.setHeader(fds);
    }

    public void encodeDocument(List<Mention> mentions)
    throws IOException {
        for (int i=0; i<mentions.size(); i++) {
            Mention m=mentions.get(i);
            if (!m.getMarkableString().equalsIgnoreCase("it")) {
                continue;
            }
            ExpletiveInstance inst=new ExpletiveInstance(m.getSentenceTree(),
                    m.getLowestProjection(),m.toString());
            for (FeatureExtractor fe: _fes) {
               fe.extractFeatures(inst);
            }
            inst.setFeature(StackingClassifierFactory.FD_FOLD_NO, fold_no);
            inst.setFeature(PairInstance.FD_POSITIVE,m.getSetID()!=null);
             _out.write(inst);
        }
        _out.flush();
        fold_no++;
    }
    
    public static void main(String[] args) {
        try {
            //ClassifierFactory fact=new StackingClassifierFactory(
            //        SVMClassifierFactory.getInstance());
            ClassifierFactory fact=SVMClassifierFactory.getInstance();
            InstanceWriter out=fact.getSink("expl","","");
            Extract_Expl trainer=new Extract_Expl(out);
            //System.setProperty("elkfed.corpus", "bnews");
            System.setProperty("elkfed.corpus", "muc6");
            TrainerProcessor proc=new TrainerProcessor(
                    trainer,
                    ConfigProperties.getInstance().getTrainingData(),
                    ConfigProperties.getInstance().getTrainingDataId(),
                    ConfigProperties.getInstance().getMentionFactory()
                    );
            proc.createTrainingData();
            trainer.fold_no=0;
//            System.setProperty("elkfed.corpus", "nwire");
//            proc=new TrainerProcessor(
//                    trainer,
//                    ConfigProperties.getInstance().getTrainingData(),
//                    ConfigProperties.getInstance().getTrainingDataId(),
//                    ConfigProperties.getInstance().getMentionFactory()
//                    );
//            proc.createTrainingData();
//            trainer.fold_no=0;
//            System.setProperty("elkfed.corpus", "npaper");
//            proc=new TrainerProcessor(
//                    trainer,
//                    ConfigProperties.getInstance().getTrainingData(),
//                    ConfigProperties.getInstance().getTrainingDataId(),
//                    ConfigProperties.getInstance().getMentionFactory()
//                    );
//            proc.createTrainingData();
            out.close();
            //fact.do_learning("expl", "-b 1 -t 4 -d 2 -j 1.5", "");
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
