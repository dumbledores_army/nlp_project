/*
 * AdjectiveMap.java
 *
 * Created on August 13, 2007, 12:07 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package elkfed.nlp.util;

import elkfed.config.ConfigProperties;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author yannick
 */
public class AdjectiveMap {
    private static AdjectiveMap _instance;
    private Map<String,String> _map;
    
    public static AdjectiveMap getInstance()
    {
        if (_instance==null)
        {
            try {
                _instance=new AdjectiveMap();
            }
            catch (FileNotFoundException e)
            {
                throw new RuntimeException("File not found",e);
            }
            catch (IOException e)
            {
                throw new RuntimeException("IOException",e);
            }            
        }
        return _instance;
    }
    
    public AdjectiveMap()
                throws FileNotFoundException, IOException
    {
        File path=ConfigProperties.getInstance().getAdjMap();
        BufferedReader br=new BufferedReader(new FileReader(path));
        _map=new HashMap<String,String>();
        String line;
        while ((line=br.readLine())!=null)
        {
            String[] entries=line.split("\t");
            _map.put(entries[0],entries[1]);
        }
    }
    
    public boolean hasKey(String adj)
    {
        return _map.containsKey(adj);
    }
    
    public String map(String adj)
    {
        String result=_map.get(adj);
        if (result==null)
            return adj;
        else
            return result;
    }
}
