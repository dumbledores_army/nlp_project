/*
 *   Copyright 2009 Yannick Versley / CiMeC Univ. Trento
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package elkfed.parse.qdsr;

import elkfed.ml.util.Alphabet;
import elkfed.ml.util.FeatureVector;
import gnu.trove.TIntObjectHashMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import static elkfed.ml.util.FeatureVector.putStringCombo;


/** Implements a Shift-Reduce parser using Nivre's
 *  arc-eager parsing strategy
 *
 * @author yannick
 */
public final class ParserState {
    public static class Recycler {
        Stack<ArrayList> free_lists;
        public ArrayList new_list() {
            if (free_lists.size()>0) {
                return free_lists.pop();
            } else {
                return new ArrayList();
            }
        }
        public void recycle_list(ArrayList l) {
            l.clear();
            free_lists.push(l);
        }
    }
    public static Recycler the_recycler=new Recycler();
    public static final class ParseNode {
        int word_id;
        int parent_id;
        ArrayList<ParseNode> left_deps;
        ArrayList<String> left_labels;
        ArrayList<ParseNode> right_deps;
        ArrayList<String> right_labels;

        ParseNode(int wid) {
            word_id=wid;
            parent_id=0;
            left_deps=the_recycler.new_list();
            left_labels=the_recycler.new_list();
            right_deps=the_recycler.new_list();
            right_labels=the_recycler.new_list();
        }
        void recycle() {
            for (ParseNode n:right_deps) {
                n.recycle();
            }
            the_recycler.recycle_list(right_labels);
            the_recycler.recycle_list(right_deps);
            for (ParseNode n:left_deps) {
                n.recycle();
            }
            the_recycler.recycle_list(left_labels);
            the_recycler.recycle_list(left_deps);
            left_deps=right_deps=null;
        }
    }

    Alphabet<String> pos_alphabet;
    // possible labels for a POS combination
    TIntObjectHashMap<List<String>> lab_tab;

    String[] words;
    int[] pos_tags;
    String[] coarse_pos;
    int[] parent_id;
    List<ParseNode> stack;
    int next_word;

    String get_stack_tag(int offset) {
        int pos=stack.size()+offset;
        if (pos<0) {
            return "*BOT*";
        } else {
            return pos_alphabet.lookupObject(pos_tags[stack.get(pos).word_id-1]);
        }
    }

    String get_stack_word(int offset) {
        int pos=stack.size()+offset;
        if (pos<0) {
            return "*BOT*";
        } else {
            return words[stack.get(pos).word_id-1];
        }
    }

    String get_stack_parent_tag(int offset) {
        int pos=stack.size()+offset;
        if (pos<0) {
            return "*BOT*";
        } else {
            ParseNode pn=stack.get(pos);
            if (pn.parent_id==0) {
                return "*NUL*";
            } else {
                return words[pn.parent_id-1];
            }
        }
    }

    String get_input_word(int offset) {
        int pos=next_word+offset;
        if (pos>=words.length) {
            return "*END*";
        } else if (pos<0) {
            return "*BEG*";
        } else {
            return words[pos];
        }
    }

    String get_input_tag(int offset) {
        int pos=next_word+offset;
        if (pos>=words.length) {
            return "*END*";
        } else if (pos<0) {
            return "*BEG*";
        } else {
            return pos_alphabet.lookupObject(pos_tags[pos]);
        }
    }

    public static interface ParseAction {
        void do_it(ParserState st);
        void extract_features(ParserState st, FeatureVector<String> fv);
    }

    public static class ShiftAction implements ParseAction {
        public void do_it(ParserState st) {
            st.stack.add(new ParseNode(st.next_word));
        }

        public void extract_features(ParserState st, FeatureVector<String> fv) {
            putStringCombo(fv,"sh",2,
                    st.get_input_tag(0),st.get_input_word(0),
                    st.get_input_tag(1),st.get_input_word(1));
        }
    }

    public static class LReduceAction implements ParseAction {
        String my_lbl;
        LReduceAction(String lbl) {my_lbl=lbl;}
        public void do_it(ParserState st) {
            ParseNode top=st.stack.remove(st.stack.size()-1);
            ParseNode top2=st.stack.get(st.stack.size()-1);
            top2.right_deps.add(top);
            top2.right_labels.add(my_lbl);
            top.parent_id=top2.word_id;
            st.parent_id[top.word_id-1]=top2.word_id;
        }

        public void extract_features(ParserState st, FeatureVector<String> fv) {
            putStringCombo(fv,"rL"+my_lbl,2,
                    st.get_stack_tag(-1),st.get_stack_word(-1),
                    st.get_input_tag(0),st.get_input_word(0),
                    st.get_input_tag(1),st.get_input_word(1),
                    st.get_stack_parent_tag(-1));
        }
    }
    public static class RReduceAction implements ParseAction {
        String my_lbl;
        RReduceAction(String lbl) {my_lbl=lbl;}
        public void do_it(ParserState st) {
            ParseNode top=st.stack.get(st.stack.size()-1);
            ParseNode top2=st.stack.remove(st.stack.size()-2);
            top.left_deps.add(top);
            top.left_labels.add(my_lbl);
            top2.parent_id=top.word_id;
            st.parent_id[top2.word_id-1]=top.word_id;
        }

        public void extract_features(ParserState st, FeatureVector<String> fv) {
            putStringCombo(fv,"rR"+my_lbl,2,
                    st.get_stack_tag(-1),st.get_stack_word(-1),
                    st.get_input_tag(0),st.get_input_word(0),
                    st.get_input_tag(1),st.get_input_word(1),
                    st.get_stack_parent_tag(-1));
        }
    }

    public static class ArcAction implements ParseAction {
        public void do_it(ParserState st) {
            ParseNode top=st.stack.get(st.stack.size()-1);
            ParseNode top2=st.stack.get(st.stack.size()-2);
            top.parent_id=top2.word_id;
        }

        public void extract_features(ParserState st, FeatureVector<String> fv) {
            putStringCombo(fv,"arc",2,
                    st.get_stack_tag(-1),st.get_stack_word(-1),
                    st.get_input_tag(0),st.get_input_word(0),
                    st.get_input_tag(1),st.get_input_word(1),
                    st.get_stack_parent_tag(-1));
        }
    }
    public void add_valid_actions(List<ParseAction> good_actions,
            List<ParseAction> bad_actions)
    {
        // RReduce can be done if there are at least two items on the stack
        // RReduce is good if st[-1].parent==st[-2]
        // get possible labels from LabTab
        //
        // RReduce can be done if 2 items *and* parent set
        // Arc can be done if 2 items *and* parent not set
        // shift can be done when there are more words
        // shift is good if no other action is good
    }
}
