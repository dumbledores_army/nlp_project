/*
 * Wrapper around MALTParser based on MaltExamples code.
 *
 * MaltExamples copyright and license:
 *   Copyright (c) 2007-2008 Johan Hall, Jens Nilsson and Joakim Nivre
 *
 *   Redistribution and use in source and binary forms,
 *   with or without modification, are permitted provided
 *   that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *   * Neither the name of MaltExamples nor the names of its contributors
 *     may be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 *  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 *  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 *  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Wrapper Code and (eventual) bugs:
 *   Copyright 2009 Yannick Versley / CiMeC Univ. Trento
 */
package elkfed.parse.malt;

import elkfed.config.ConfigProperties;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import org.maltparser.Engine;
import org.maltparser.core.exception.MaltChainedException;
import org.maltparser.core.flow.FlowChartInstance;
import org.maltparser.core.helper.Util;
import org.maltparser.core.io.dataformat.DataFormatInstance;
import org.maltparser.core.io.dataformat.DataFormatSpecification;
import org.maltparser.core.options.OptionManager;
import org.maltparser.core.symbol.SymbolTable;
import org.maltparser.core.syntaxgraph.DependencyGraph;
import org.maltparser.core.syntaxgraph.DependencyStructure;
import org.maltparser.core.syntaxgraph.node.DependencyNode;
import org.maltparser.core.syntaxgraph.reader.SyntaxGraphReader;
import org.maltparser.core.syntaxgraph.reader.TabReader;
import org.maltparser.core.syntaxgraph.writer.SyntaxGraphWriter;
import org.maltparser.core.syntaxgraph.writer.TabWriter;
import org.maltparser.parser.SingleMalt;

/**
 * provides a wrapper to MALTParser which allows us to run it repeatedly
 * without starting a JVM, loading the SVM models etc.
 *
 * This is based on Examples 5 and 7 from Johan Hall's MaltExamples.
 */
public class MALTWrapper {

    private URL urlMaltJar;
    private int optionContainer;
    private static int lastOptionContainer = -1;
    protected Engine engine;
    protected FlowChartInstance flowChartInstance;
    protected DataFormatInstance dataFormatInstance;

    public MALTWrapper(String modelName) {
        try {
            String maltpath =
                    new File(ConfigProperties.getInstance().getRoot(),
                    "libs/malt.jar").getAbsolutePath();
            urlMaltJar = Util.findURL(maltpath);
            synchronized (OptionManager.instance()) {
                // don't know if this actually helps, but having multiple
                // threads accessing OptionManager concurrently (which we don't
                // do right now, as we do everything single-threaded)
                // sounds like inviting disaster. -yv
                OptionManager.instance().loadOptionDescriptionFile(
                        new URL("jar:" + urlMaltJar.toString() + "!/appdata/options.xml"));
                OptionManager.instance().generateMaps();
                optionContainer = ++lastOptionContainer;
                // change to models directory - MALTParser's system of .mco files
                // makes this necessary since it uses relative paths to access them
                String oldDir = System.getProperty("user.dir");
                try {
                    System.setProperty("user.dir",
                            new File(ConfigProperties.getInstance().getRoot(),
                            "models/parser").getAbsolutePath());
                    OptionManager.instance().parseCommandLine(
                            String.format("-c %s -m parse -lfi /tmp/malt.log", modelName),
                            optionContainer);
                    engine = new Engine();
                    flowChartInstance = engine.initialize(0);
                    DataFormatSpecification dataFormat =
                            new DataFormatSpecification();
                    dataFormat.parseDataFormatXMLfile("jar:" + urlMaltJar.toString() +
                            "!/appdata/dataformat/conllx.xml");
                    dataFormatInstance = dataFormat.createDataFormatInstance(
                            flowChartInstance.getSymbolTables(),
                            "none", "ROOT");
                    if (flowChartInstance.hasPreProcessChartItems()) {
                        flowChartInstance.preprocess();
                    }
                } finally {
                    System.setProperty("user.dir", oldDir);
                }
            }
        } catch (MalformedURLException ex) {
            throw new RuntimeException("Cannot find malt.jar", ex);
        } catch (MaltChainedException ex) {
            throw new RuntimeException("Cannot initialize MALTParser", ex);
        }
    }

    public void parseFile(String inFile, String outFile,
            String charset) throws MaltChainedException {
        DependencyStructure inputGraph =
                new DependencyGraph(flowChartInstance.getSymbolTables());
        DependencyStructure outputGraph =
                new DependencyGraph(flowChartInstance.getSymbolTables());
        SyntaxGraphReader tabReader = new TabReader();
        tabReader.setDataFormatInstance(dataFormatInstance);
        SyntaxGraphWriter tabWriter = new TabWriter();
        tabWriter.setDataFormatInstance(dataFormatInstance);

        tabReader.open(inFile, charset);
        tabWriter.open(outFile, charset);

        boolean moreInput = true;
        SingleMalt singleMalt = (SingleMalt) flowChartInstance.getFlowChartRegistry(SingleMalt.class,
                "singlemalt");
        while (moreInput) {
            moreInput = tabReader.readSentence(inputGraph);
            if (inputGraph.hasTokens()) {
                outputGraph.clear();
                for (int index : inputGraph.getTokenIndices()) {
                    DependencyNode gnode = inputGraph.getTokenNode(index);
                    DependencyNode pnode = outputGraph.addTokenNode(gnode.getIndex());
                    for (SymbolTable table : gnode.getLabelTypes()) {
                        pnode.addLabel(table,
                                gnode.getLabelSymbol(table));
                    }
                }
                singleMalt.parse(outputGraph);
                tabWriter.writeSentence(outputGraph);
            }
        }
        tabReader.close();
        tabWriter.close();
    }

    public void terminate() throws MaltChainedException {
        String oldDir = System.getProperty("user.dir");
        try {
            System.setProperty("user.dir",
                    new File(ConfigProperties.getInstance().getRoot(),
                    "models/parser").getAbsolutePath());
            if (flowChartInstance.hasPostProcessChartItems()) {
                flowChartInstance.postprocess();
            }
            engine.terminate(optionContainer);
        } finally {
            System.setProperty("user.dir", oldDir);
        }
    }

    public static void main(String[] args) {
        try {
            MALTWrapper malt = new MALTWrapper(args[0]);
            for (int i = 1; i < args.length; i += 2) {
                malt.parseFile(args[i], args[i + 1], "ISO-8859-15");
            }
            malt.terminate();
        } catch (MaltChainedException ex) {
            ex.printStackTrace();
            System.exit(1);
        }
    }
}
