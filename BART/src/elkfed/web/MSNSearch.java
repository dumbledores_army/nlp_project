/*
 * MSNSearch.java
 *
 * Created on July 27, 2007, 8:55 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package elkfed.web;
import com.microsoft.schemas.msnsearch._2005._09.fex.*;
import elkfed.config.ConfigProperties;
import elkfed.wsdl.msnlive.MSNSearchServiceStub;
import java.io.File;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.transport.http.HTTPConstants;
import ponzo.util.BerkeleyDB;

/**
 *
 * @author yannick
 */
public class MSNSearch {
    // minimum time between requests, in ms
    public static final long MINIMUM_WAIT=500;
    // maximum time to hang around waiting
    public static final long MAXIMUM_WAIT=30000;
    final MSNSearchServiceStub _stub;
    private BerkeleyDB<String,Integer> _cache;
    private long last_search=0;
    protected static Logger _log=Logger.getAnonymousLogger();
    
    /** Creates a new instance of MSNSearch */
    public MSNSearch() throws AxisFault{
        _stub=new MSNSearchServiceStub("http://soap.search.msn.com/webservices.asmx");
        Options opts=_stub._getServiceClient()
            .getOptions();
        opts.setProperty(HTTPConstants.CHUNKED,false);
        opts.setTimeOutInMilliSeconds(5*60*1000);
        _cache=new BerkeleyDB<String,Integer>(new File("."),"msn_count.db",String.class,Integer.class);
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run()
            {
                _log.log(Level.INFO,"Closing cache DB");
                _cache.close();
                _cache=null;
            }
        });
    }
    
    public int webcount(String query)
        throws RemoteException
    {
        if (_cache.containsKey(query))
        {
            return _cache.get(query);
        }
        else
        {
            int result=webcount_uncached(query);
            _cache.put(query,result);
            return result;
        }
    }
    
    int webcount_uncached(String query) throws RemoteException
    {
        long start_time=System.currentTimeMillis();
        if (start_time<last_search+MINIMUM_WAIT)
        {
            try {
                Thread.sleep(last_search+MINIMUM_WAIT-start_time);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        SearchDocument sdoc=SearchDocument.Factory.newInstance();
        SearchRequest srch=sdoc.addNewSearch().addNewRequest();
        srch.setAppID(ConfigProperties.getInstance().getMSNAppID());
        srch.setQuery(query);
        srch.setCultureInfo("en-US");
        srch.setSafeSearch(SafeSearchOptions.OFF);
        SourceRequest sreq=srch.addNewRequests().addNewSourceRequest();
        sreq.setSource(SourceType.WEB);
        sreq.setCount(10);
        sreq.setOffset(0);
        ArrayList<String> all=new ArrayList<String>();
        all.add("All");
        sreq.setResultFields(all);
        _log.log(Level.INFO,String.format("Querying for '%s'",query));
        long query_time=start_time;
        AxisFault fault=null;
        long time_to_wait=MINIMUM_WAIT*3;
        while (query_time<start_time+MAXIMUM_WAIT)
        {
            try {
                query_time=System.currentTimeMillis();
                SearchResponseDocument resp=_stub.Search(sdoc);
                last_search=System.currentTimeMillis();
                return resp.getSearchResponse().getResponse()
                    .getResponses().getSourceResponseArray(0).getTotal();
            } catch (AxisFault ex) {
                try {
                    _log.log(Level.WARNING,String.format("query error: %s",ex));
                    fault=ex;
                    Thread.sleep(time_to_wait);
                    time_to_wait=time_to_wait+(time_to_wait/2);
                } catch (InterruptedException e2) {
                    _log.log(Level.WARNING,"sleep interrupted",e2);
                }
            }
        }
        throw fault;
        //System.out.println(resp);
    }

    String[] snippets_uncached(String query) throws RemoteException
    {
        long start_time=System.currentTimeMillis();
        if (start_time<last_search+MINIMUM_WAIT)
        {
            try {
                Thread.sleep(last_search+MINIMUM_WAIT-start_time);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        SearchDocument sdoc=SearchDocument.Factory.newInstance();
        SearchRequest srch=sdoc.addNewSearch().addNewRequest();
        srch.setAppID(ConfigProperties.getInstance().getMSNAppID());
        srch.setQuery(query);
        srch.setCultureInfo("en-US");
        srch.setSafeSearch(SafeSearchOptions.OFF);
        SourceRequest sreq=srch.addNewRequests().addNewSourceRequest();
        sreq.setSource(SourceType.WEB);
        sreq.setCount(50);
        sreq.setOffset(0);
        ArrayList<String> all=new ArrayList<String>();
        all.add("All");
        sreq.setResultFields(all);
        _log.log(Level.INFO,String.format("Querying for '%s'",query));
        long query_time=start_time;
        AxisFault fault=null;
        long time_to_wait=MINIMUM_WAIT*3;
        List<String> results=new ArrayList<String>();
        while (query_time<start_time+MAXIMUM_WAIT)
        {
            try {
                query_time=System.currentTimeMillis();
                SearchResponseDocument resp=_stub.Search(sdoc);
                last_search=System.currentTimeMillis();
                for (Result r: resp.getSearchResponse().getResponse()
                    .getResponses().getSourceResponseArray(0)
                        .getResults().getResultArray()) {
                    results.add(r.getSummary());
                }
                return results.toArray(new String[results.size()]);
            } catch (AxisFault ex) {
                try {
                    _log.log(Level.WARNING,String.format("query error: %s",ex));
                    fault=ex;
                    Thread.sleep(time_to_wait);
                    time_to_wait=time_to_wait+(time_to_wait/2);
                } catch (InterruptedException e2) {
                    _log.log(Level.WARNING,"sleep interrupted",e2);
                }
            }
        }
        throw fault;
        //System.out.println(resp);
    }

    
    public int webcount_phrase(String phrase) throws RemoteException
    {
        if (phrase.indexOf(' ')==-1)
            return webcount(phrase);
        else
            return webcount('"'+phrase+'"');    
    }
    
    public static void main(String [] args)
    {
        String[] test_items={"\"green pony\"", "\"pink pony\"", "\"red pony\""};
        try {
            MSNSearch search=new MSNSearch();
            for (String w: test_items)
            {
                int count=search.webcount(w);
                System.out.format("%s -> %d\n",w,count);
            }
            String other="\"Bill Gates and other\"";
            System.out.format("Snippets for %s\n",other);
            for (String w: search.snippets_uncached(other)) {
                System.out.format("> %s\n",w);            
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
