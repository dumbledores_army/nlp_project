/*
 * Copyright 2007 Yannick Versley / Univ. Tuebingen
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package elkfed.lang;

import elkfed.knowledge.SemanticClass;
import elkfed.nlp.util.Gender;
import java.util.EnumSet;

/**
 * contains all the linguistic (as opposed to implementation-dependent)
 * information on a mention.
 * @author versley
 */
public class MentionType {
    // TODO: isDemPronoun is just isDemonstrative&isPronoun,
    // similar for isDemNominal. Could we maybe get rid of them?
    public enum Features
    {
        isProperName, isEnamex,
        isPronoun, isNominal,
        isDefinite, isDemonstrative,
        isFirstSecondPerson, isReflexive,
        isPersPronoun, isPossPronoun, isDemPronoun,
        isDemNominal, isIndefinite,
        isSingular, isPlural;
    }
    public EnumSet<Features> features=EnumSet.noneOf(Features.class);
    public Gender gender=Gender.UNKNOWN;
    public SemanticClass semanticClass=SemanticClass.UNKNOWN;
}
