/*
 * Copyright 2007 EML Research
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package elkfed.lang;

import java.util.regex.Pattern;

/** This is the repository for the constants used in generic
 *  linguistic processing.
 *
 * @author ponzetsp
 */
public class EnglishLinguisticConstants
{   
    /** Just a static repository... */
    private EnglishLinguisticConstants() {}
    
    /** Personal pronoun regexp (nominative case)*/
    public static final String PERSONAL_PRONOUN_NOM = "(i|you|he|she|it|we|you|they)";
    
    /** Personal pronoun regexp (nominative case)*/
    public static final String PERSONAL_PRONOUN_ACCUSATIVE = "(me|you|him|her|it|us|you|them)";
    
    /** Possessive pronoun regexp */
    public static final String REFLEXIVE_PRONOUN =
        "(myself|yourself|yourselves|himself|herself|itself|ourselves|themselves)";
    
    /** Possessive pronoun regexp */
    public static final String POSSESSIVE_PRONOUN = "(mine|yours|his|hers|its|ours|theirs)";
    
    /** Possessive pronoun regexp */
    public static final String POSSESSIVE_ADJECTIVE = "(my|your|his|her|its|our|their)";

    public static final Pattern FIRST_SECOND_PERSON_RE=
            Pattern.compile("(i|me|my|you|your|we|us|our)");

    /** Pronoun regexp */
    public static final String PRONOUN = new StringBuffer().
            append(PERSONAL_PRONOUN_NOM).append("|").
            append(PERSONAL_PRONOUN_ACCUSATIVE).append("|").
            append(REFLEXIVE_PRONOUN).append("|").
            append(POSSESSIVE_PRONOUN).append("|").
            append(POSSESSIVE_ADJECTIVE).append("|").
            append(REFLEXIVE_PRONOUN).toString();
    
    /** Singular pronoun regexp */
    public static final String SINGULAR_PRONOUN_ADJ = new StringBuffer().
        append("(i|you|he|she|it|").append("me|you|him|her|it|").
        append("myself|yourself|himself|herself|itself|").
        append("mine|yours|his|hers|its|").
        append("my|your|his|her|its)").toString();
    
    /** Plural pronoun regexp */
    public static final String PLURAL_PRONOUN_ADJ = new StringBuffer().    
        append("(we|you|they|").append("us|you|them|").
        append("ourselves|yourselves|themselves|").
        append("ours|yours|theirs|").
        append("our|your|their)").toString();
            
    /** Male pronoun regexp */
    public static final String MALE_PRONOUN_ADJ = "(he|him|himself|his)";
    
    /** Female pronoun regexp */
    public static final String FEMALE_PRONOUN_ADJ = "(she|her|herself|hers|her)";
    
    /** Neutral pronoun regexp */
    public static final String NEUTRAL_PRONOUN_ADJ = "(it|itself|its)";
    
    /** Relative pronoun regexp */
    public static final String RELATIVE_PRONOUN = "(who|which|whom|whose|that)";
            
    /** The Saxon genitive */
    public static final String SAXON_GENITIVE = "'s";
 
    /** The definite article */
    public static final String DEF_ARTICLE = "the";
    
    /** The indefinite article */
    public static final String INDEF_ARTICLE = "(a|an)";
    
    /** The article regexp */
    public static final String ARTICLE = "(a|an|the)";

    /** A leading article */
    public static final String LEADING_ARTICLE =
        new StringBuffer().append("^").append(ARTICLE).toString();
    
    /** The demonstrative regexp */
    public static final String DEMONSTRATIVE = "(this|that|these|those)";
    
    /** The array of demonstratives */
    public static final String[] DEMONSTRATIVES =
        DEMONSTRATIVE.replaceAll("\\(|\\)","").split("\\|");    
    
    /** The Peen TB labels for common noun */
    public static final String COMMON_NOUN_POS = "(nn|nns)"; 
    
    /** The Peen TB labels for proper noun */
    public static final String PROPER_NOUN_POS = "(np|nps|nnp|nnps)";
    
    /** The Peen TB labels for noun */
    public static final String NOUN = new StringBuffer().
            append(PROPER_NOUN_POS).append("|").
            append(COMMON_NOUN_POS).toString();
    
    /** The Peen TB labels for cardinal numbers */
    public static final String CARDINAL_NUMBER_POS = "cd";
    
    /** The Peen TB labels for prepositions */
    public static final String PREPOSITION_POS = "in";
    
    /** The Peen TB labels for determiners */
    public static final String DETERMINER_POS = "dt";
    
    /** The adjective regexp */
    public static final String ADJECTIVE_POS = "(jj|jjr|jjs)";
    
    /** The quotation marks regexp */
    // DON'T NEED IT: USE PUNCTUATION_MARK
    // public static final String QUOTATION_MARK = "(``|'')";
    
    /** The punctuation marks regexp */
    public static final String PUNCTUATION_MARK = new StringBuffer().
            append("(`|``|'|''|\"|\\[|\\]|\\(|\\)|-|").
            append("\\.|,|:|;|!|\\?)").toString();
    
    /** A leading punctuation mark */
    public static final String LEADING_PUNCTUATION =
        new StringBuffer().append("^").append(PUNCTUATION_MARK).toString();
    
    /** A trailing punctuation mark */
    public static final String TRAILING_PUNCTUATION =
        new StringBuffer().append(PUNCTUATION_MARK).append("$").toString();
    
    /** The coordinatig conjuction tag */
    public static String COORDINATING_CONJUCTION_POS = "cc";
    
    /** The designator expression for males */
    public static final String MALE_DESIGNATOR = "(mr|messrs|dr)(\\.)?";

    /** The designator expression for females */
    public static final String FEMALE_DESIGNATOR = "(miss|mrs|ms)(\\.)?";
    
    /** The designator expression for companies */
    public static final String COMPANY_DESIGNATOR =
            "(assoc|bros|co|coop|corp|devel|inc|llc|ltd)\\.?";
}
