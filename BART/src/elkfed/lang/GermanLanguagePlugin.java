/*
 *  Copyright 2009 Yannick Versley / CiMeC Univ. Trento
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package elkfed.lang;

import edu.stanford.nlp.trees.Tree;
import elkfed.knowledge.SemanticClass;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 *
 * @author yannick
 */
public class GermanLanguagePlugin extends AbstractLanguagePlugin {

    // comparative particles and pre-determiner adverbs
    public static final String KOKOM_ADV="als|wie|auch|besonders";
    public static final String PUNCT = "[,:;\\.`\'\"\\-\\)\\(]+";
    protected static final Pattern left_unwanted = Pattern.compile(
            String.format("%s|%s", KOKOM_ADV,PUNCT), Pattern.CASE_INSENSITIVE);
    protected static final Pattern right_unwanted = Pattern.compile(PUNCT);
    protected static final Pattern attr_node = Pattern.compile(
            "ADJX");
    protected static final Pattern rel_node = Pattern.compile(
            "PX|R-SIMPX");

    public boolean unwanted_left(String tok) {
        return left_unwanted.matcher(tok).matches();
    }

    public boolean unwanted_right(String tok) {
        return right_unwanted.matcher(tok).matches();
    }

    @Override
    public List<Tree>[] calcParseInfo(Tree sentTree,
            int startWord, int endWord,
            MentionType markableType) {
        /* now, the *proper* way to do this would be to find the
         * head(s) and look for everything non-head. Instead,
         * we just look for children that look like modifiers,
         * which means that we get weird results
         * (i) for appositions
         * (ii) for elliptic NPs (e.g., 'la gialla'),
         *      where the head 'gialla' also gets recruited as a modifier
         */
        List<Tree>[] result = new List[3];
        List<Tree> projections = new ArrayList<Tree>();
        List<Tree> premod = new ArrayList<Tree>();
        List<Tree> postmod = new ArrayList<Tree>();
        result[0] = projections;
        result[1] = premod;
        result[2] = postmod;
        Tree node = calcLowestProjection(sentTree, startWord, endWord);
        projections.add(node);
        for (Tree n : node.children()) {
            String cat = n.value();
            if (attr_node.matcher(cat).matches()) {
                premod.add(n);
            } else if (rel_node.matcher(cat).matches()) {
                postmod.add(n);
            }
        }
        return result;
    }

    private final static Pattern pat_NP=
            Pattern.compile("NX",Pattern.CASE_INSENSITIVE);
    private final static Pattern pat_CN=
            Pattern.compile("NN",Pattern.CASE_INSENSITIVE);
    private final static Pattern pat_PN=
            Pattern.compile("NE",Pattern.CASE_INSENSITIVE);
    private final static Pattern pat_PRO=
            Pattern.compile("PIS|PPER|PDS|PRELS",Pattern.CASE_INSENSITIVE);
    private final static Pattern pat_PP=
            Pattern.compile("PX",Pattern.CASE_INSENSITIVE);
    private final static Pattern pat_PREP=
            Pattern.compile("APP[RO]|APZR",Pattern.CASE_INSENSITIVE);
    private final static Pattern pat_S=
            Pattern.compile("SIMPX|R-SIMPX",Pattern.CASE_INSENSITIVE);
    private final static Pattern pat_DT=
            Pattern.compile("ART|PIAT|PDAT",Pattern.CASE_INSENSITIVE);
    private final static Pattern pat_DT2=
            Pattern.compile("PPOSAT|PIDAT",Pattern.CASE_INSENSITIVE);
    private final static Pattern pat_ADJ=
            Pattern.compile("ADJ[AD]",Pattern.CASE_INSENSITIVE);
    private final static Pattern pat_ADV=
            Pattern.compile("ADV",Pattern.CASE_INSENSITIVE);
    private final static Pattern pat_CC=
            Pattern.compile("KON",Pattern.CASE_INSENSITIVE);
    public NodeCategory labelCat(String cat) {
        if (pat_NP.matcher(cat).matches()) {
            return NodeCategory.NP;
        } else if (pat_CN.matcher(cat).matches()) {
            return NodeCategory.CN;
        } else if (pat_PN.matcher(cat).matches()) {
            return NodeCategory.PN;
        } else if (pat_PRO.matcher(cat).matches()) {
            return NodeCategory.PRO;
        } else if (pat_PP.matcher(cat).matches()) {
            return NodeCategory.PP;
        } else if (pat_PREP.matcher(cat).matches()) {
            return NodeCategory.PREP;
        } else if (pat_S.matcher(cat).matches()) {
            return NodeCategory.S;
        } else if (pat_DT.matcher(cat).matches()) {
            return NodeCategory.DT;
        } else if (pat_DT2.matcher(cat).matches()) {
            return NodeCategory.DT2;
        } else if (pat_ADJ.matcher(cat).matches()) {
            return NodeCategory.ADJ;
        } else if (pat_ADV.matcher(cat).matches()) {
            return NodeCategory.ADV;
        } else if (pat_CC.matcher(cat).matches()) {
            return NodeCategory.CC;
        } else {
            return NodeCategory.OTHER;
        }
    }

    @Override
    public SemanticClass getSemanticClass(String sem_type, String gender) {
        if (sem_type.equalsIgnoreCase("PER")) {
            if (gender.equalsIgnoreCase("fem")) {
                return SemanticClass.FEMALE;
            } else if (gender.equalsIgnoreCase("masc")) {
                return SemanticClass.MALE;
            } else {
                return SemanticClass.PERSON;
            }
        } else if (sem_type.equalsIgnoreCase("ORG")) {
            return SemanticClass.ORGANIZATION;
        } else if (sem_type.equalsIgnoreCase("TMP")) {
            return SemanticClass.TIME;
        } else {
            return SemanticClass.UNKNOWN;
        }
    }


}
