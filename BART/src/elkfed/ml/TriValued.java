/*
 * TriValued.java
 *
 * Created on July 11, 2007, 6:29 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package elkfed.ml;

/** An enumeration used for modeling features which can take 3 values.
 *
 * @author vae2101
 */
public enum TriValued {
    FALSE,
    TRUE,
    UNKNOWN;
}
