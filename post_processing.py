
# coding: utf-8

# In[ ]:

#Imports
import nltk
from nltk.tokenize import sent_tokenize
from nltk.tokenize import PunktSentenceTokenizer
from sklearn import linear_model
import csv
import stanford_ner
from nltk.tokenize import sent_tokenize
from nltk.tokenize import PunktSentenceTokenizer
from question_helper import *
import numpy
import os
import warnings

warnings.filterwarnings("ignore",category=DeprecationWarning)


# In[ ]:

#Takes a list of strings (sentences from the ask.py module)
#Eliminate non-candidates, and return the list of questions that met the criteria
#Criteria for elimination are:
    #1. Question length less than lower bound
    #2. Question length greater than upper bound
    #3. Question does not end with one of the allowed end types (i.e. verb, noun,...)
    #(More criteria can be added as needed...)
#This function will be called in the main ranking function

def cull_questions(question_list):
    #initialize list of candidate questions (i.e. those not discarded)
    candidates = []
    #create list of pos types that are permissible for ending a sentence:
    allowed_end_types = ["NN","NNP","VB","VBD","JJ"]
    #Create lower and upper bounds on question length
    LOWER_BOUND = 5
    UPPER_BOUND = 25
    #Iterate over each question in the list
    for i in range(0, len(question_list)):
        question = question_list[i]
        #Rules 1 and 2: If the question is too short or too long, move onto the next candidate
        question_length = len(question.split())
        if question_length < LOWER_BOUND or question_length > UPPER_BOUND:
            continue
        if question.count(',') >1 or question.count(':') >0 :
            continue
        tokens = nltk.word_tokenize(question)
        text = nltk.Text(tokens)
        tags = nltk.pos_tag(text)
        #print tags[len(tags)-2]
        last_tuple = tags[len(tags)-2]
        #Rule 2: If the last word of the question is not one of the allowed types (verb or true noun), go to the next candidate
        if last_tuple[1] not in allowed_end_types:
            continue
        #If we've made it this far, the question has passed all the test, so add it to the list of candidates
        candidates.append(question)

    return candidates
        
        
        
#test = ["Is a male president permitted?","Which is the biggest country?", "Is the flag's color green?", "Was that one of his?",
#       "Did that ball belong to John Smith?","Was China's island the larger one?", "Was China's island larger than Russia's?"]
#cull_questions(test)
#NOTE THE CASES THAT ARE FAILING ABOVE, BUT WHICH ARE VALID QUESTIONS... consider adding more allowed pos end-types


# In[ ]:

#Create list of wh words
wh_words = ["Who","What","When","Where","Why", "Which", "How", "Whose"]

#Create Stanford NER Tagger instance
tagger = stanford_ner.create_tagger()

#Read in the sentences and labels from the manually-labeled list of questions.  Generate 2 arrays:
#1: a 1-d array of just the labels
#2: an array where the columns are features (type, length, named entities, pronouns)
if not os.path.isfile("feature_matrix.npy") and os.path.isfile("label_matrix.npy"):
    #Initialize the arrays (we have 1029 rows in our labeled training set):
    labels = numpy.array([None]*1029)
    features = numpy.array([None]*1029)
    #Read in the tsv
    with open('data/Questions_and_ranks.tsv','rb') as tsv:
        reader = csv.reader(tsv, delimiter='\t')
        row_num = 0
        for row in reader:
            try:
                #append the label to the labels array
                labels[row_num] = row[1]
                #extract the features from the question, place them in an array, and append the array to the features array
                question = row[0]
                question_features = numpy.array([None]*4)
                words = question.split(" ")
                #element 0 is wh
                if words[0] in wh_words:
                    question_features[0] = 1
                else:
                    question_features[0] = 0
                #element 1 is length
                question_features[1] = len(words)
                #element 2 is pronouns
                pronoun_count = 0
                tokens = nltk.word_tokenize(question)
                text = nltk.Text(tokens)
                tags = nltk.pos_tag(text)
                for word,tag in tags:
                    if tag == 'PRP' or tag == 'PRP$':
                        pronoun_count += 1
                question_features[2] = pronoun_count
                #element 3 is named entities
                ent_list = get_named_entities(tagger, question)
                question_features[3] = len(ent_list)
                #append the current question_features array to the features array
                features[row_num] = question_features
                row_num += 1
            except:
                pass

    #Remove null entries from end of features and labels (a few questions caused errors and were skipped)
    feature_matrix = numpy.delete(features, [1024,1025,1026,1027,1028], 0)
    label_matrix = numpy.delete(labels, [1024,1025,1026,1027,1028], 0)

    numpy.save('feature_matrix', feature_matrix)
    numpy.save('label_matrix', label_matrix)
else: 
    feature_matrix = numpy.load('feature_matrix.npy')
    label_matrix = numpy.load('label_matrix.npy')


#Train the model
clf = linear_model.Lasso(alpha=0.000001)
clf = clf.fit(numpy.vstack(feature_matrix).astype(numpy.float), numpy.vstack(label_matrix).astype(numpy.float))


# In[ ]:

#Main method of this module, which should be called at the end of the "ask.py" module. Returns a culled, ranked list of questions

def rank_questions(question_list):
    
    #Step 1: get the culled list of questions
    culled_list = cull_questions(question_list)
    
    #Step 2: assign ranks to the questions that remain
    #Initialize list to hold the ranks. We'll later zip this list and the question_list together, and then sort on rank
    ranks = []
    for i in range(0, len(culled_list)):
        question = culled_list[i]
        question_features = numpy.array([None]*4)
        words = question.split(" ")
        #element 0 is wh
        if words[0] in wh_words:
            question_features[0] = 1
        else:
            question_features[0] = 0
        #element 1 is length
        question_features[1] = len(words)
        #element 2 is pronoun count
        pronoun_count = 0
        tokens = nltk.word_tokenize(question)
        text = nltk.Text(tokens)
        tags = nltk.pos_tag(text)
        for word,tag in tags:
            if tag == 'PRP' or tag == 'PRP$':
                pronoun_count += 1
        question_features[2] = pronoun_count
        #element 3 is named entity count
        ent_list = get_named_entities(tagger, question)
        question_features[3] = len(ent_list)
        #Calculate the score for the current question, and add that score to the ranks array
        ranks.append(clf.predict(question_features))
        
    #Now we have a list of ranks, whose order corresponds to the input list (from )
    #Next, zip this rank list with the culled_question list
    questions_and_ranks = zip(culled_list, ranks)
    #Now, sort the list of tuples based on rank
    questions_and_ranks.sort(key=lambda x: x[1])
    #Generate the sorted list of questions
    ranked_questions = [x[0] for x in questions_and_ranks]
    return ranked_questions


# In[ ]:



