
# coding: utf-8

# In[7]:

from postag import *
from nltk.tree import Tree
from pattern.en import conjugate, lemma, comparative, superlative
from nltk.corpus import wordnet as wn
import requests
import xml.etree.ElementTree as ET
requests.adapters.DEFAULT_RETRIES = 1

APPOSITION=((SENTENCE, ((NP, (NP, COMMA, NP, COMMA)), VP, PERIOD)))
# converts the tree to string form
def convert_tree_to_string(tree):
  return ' '.join(tree.leaves())

def does_match(myTree, phrase):
    if  (type(phrase) is not tuple):
        return myTree.label() == phrase
    else:
        parent = phrase[0]
        children = phrase[1]
        if myTree.label() == parent and len(myTree) == len(children): # parent matches
            
            for i in xrange(len(myTree)): # check that all children match
                ith_child = myTree[i]
                if not does_match(ith_child, children[i]):
                    return False
            return True
    



def convert_tree_into_string(tree_structure):
    return ' '.join(tree_structure.leaves())



def is_verb(phrase):
    return phrase.label().startswith('V') or phrase.label() == 'MD'
# returns True iff the verb is a modal or auxiliary 'have', 'do'

TENSE_MAPPING = {VERB_INF:'inf' , VERB_PRESENT_SINGULAR:'1sg' ,VERB_PRESENT_PL:'3sg', VERB_PAST:'p' ,VERB_PAST_PART:'ppart' }
def  get_tense(first_verb):
    return TENSE_MAPPING.get(first_verb.label())
    
def get_conjugate(phrase,tense):
    return conjugate(phrase, tense)

VERB_TENSES = [VERB_PAST, VERB_PRESENT_SINGULAR , VERB_PRESENT_PL]
def is_verb_tensed(phrase):
    return phrase.label() in VERB_TENSES



def who_question(list_of_phrases):
    questions=[]
    for phrase in list_of_phrases:
        try:
            parse_tree=phrase[0]
            #divide the sentence into Noun Phrase and Verb Phrase (subject and predicate)
            np =phrase[0] # will be used to check if NP is Person
            vp =phrase[1]
            #In the verb phrase, find the modal verb/aux verb
            first_verb= vp[0]
            if(is_verb_tensed(first_verb) and first_verb.label() != VP):
            #check if first word in verb phrase is a verb and a 
                q= "who" + " " + convert_tree_to_string(vp) +" ?"
                questions.append(q.capitalize())
        except:
            pass
    return questions

        

def where_question(list_of_phrases):
    questions=[]
    for phrase in list_of_phrases:
        try:
            parse_tree=phrase[0] #will be used as subject
            #divide the sentence into Noun Phrase and Verb Phrase (subject and predicate)
            vp =phrase[1]
            vp_len = len(vp)
            #In the verb phrase, find the modal verb/aux verb
            first_verb= vp[0]
            if(is_verb_tensed(first_verb) and first_verb.label() != VP):
                prep_phrase = vp[1]
                if prep_phrase.label() != 'PP':
                    if len(vp) > 2:
                        prep_phrase = vp[2]
                    else:
                        prep_phrase = vp[1][1]
                if prep_phrase.label() == 'PP':
                    if prep_phrase[0].label() in ['IN', 'TO']:
                        #check if first phrase is a modal verb
                        if is_modal(first_verb, vp):
                            modelverb=convert_tree_to_string(first_verb)
                            main_verb = convert_tree_to_string(vp[1][0])
                        #add Do or Did or Does as an artificial model verb
                        else:
                            tense =get_tense(first_verb)
                            modelverb=get_conjugate('do',tense)
                            main_verb = get_conjugate(convert_tree_to_string(vp[0]), VERB_PRESENT_SINGULAR)
                        q= "where " + modelverb + " " + convert_tree_to_string(parse_tree) + " "+ main_verb + "?"
                    questions.append(q.capitalize())
        except:
            pass
    return questions        


def when_question(list_of_phrases):
    questions=[]
    for phrase in list_of_phrases:
        try:
            parse_tree=phrase[0]
            #divide the sentence into Noun Phrase and Verb Phrase (subject and predicate)
            #np =parse_tree[0] # will be used to check if NP is subject
            vp =phrase[1]
            #In the verb phrase, find the modal verb/aux verb
            first_verb= vp[0]
            if(is_verb(first_verb) and first_verb.label() != VP):
                #check if first phrase is a model verb
                if is_modal(first_verb, vp):
                    modelverb=convert_tree_to_string(first_verb)
                    main_verb = convert_tree_to_string(vp[1][0]) + " "+ convert_tree_to_string(vp[1][1])
                #add Do or Did or Does as model verb
                else:
                    tense =get_tense(first_verb)
                    modelverb=get_conjugate('do',tense)
                    main_verb = get_conjugate(convert_tree_to_string(vp[0]), VERB_PRESENT_SINGULAR) + " " + convert_tree_to_string(vp[1])
            #check if first word in verb phrase is a verb and a 
                q= "when " + modelverb + " " + convert_tree_to_string(parse_tree) + " "+ main_verb+ "?"
                questions.append(q.capitalize())
        except:
            pass
    return questions
        

def what_question(list_of_phrases):
    questions=[]
    for phrase in list_of_phrases:
        try:
            #divide the sentence into Noun Phrase and Verb Phrase (subject and predicate)
            np =phrase[0] # will be used to check if NP is Person
            vp =phrase[1]
            #In the verb phrase, find the modal verb/aux verb
            first_verb= vp[0]
            if(is_verb_tensed(first_verb) and first_verb.label() != VP):
                #check if first phrase is a model verb
                if is_modal(first_verb, vp):
                    modelverb=convert_tree_to_string(first_verb)
                    q= "what " + modelverb + " " + convert_tree_to_string(vp[1]) + "?"
                    questions.append(q.capitalize())
        except:
            pass
    return questions


def how_much_question(list_of_phrases):
    print "how"

def how_many_question(list_of_phrases):
    questions=[]
    for phrase in list_of_phrases:
        try:
            #divide the sentence into Noun Phrase and Verb Phrase (subject and predicate)
            np =phrase[0]
            vp = phrase[1]
            main_noun = is_number_plural(np)
            if main_noun:
                q= "how many " + main_noun + " " + convert_tree_to_string(vp) + "?"
            else:
                vp =phrase[1]
                #In the verb phrase, find the modal verb/aux verb
                first_verb= vp[0]
                if(is_verb_tensed(first_verb) and first_verb.label() != VP):
                    sec_noun = vp[1]
                    if sec_noun.label() == 'NP':
                        main_noun = is_number_plural(sec_noun)
                        if main_noun:
                            #check if first phrase is a model verb
                            if is_modal(first_verb, vp):
                                modelverb=convert_tree_to_string(first_verb)
                                main_verb = convert_tree_to_string(vp[1]) 
                            #add Do or Did or Does as model verb
                            else:
                                tense =get_tense(first_verb)
                                modelverb=get_conjugate('do',tense)
                                main_verb = get_conjugate(convert_tree_to_string(first_verb), VERB_PRESENT_SINGULAR)
                        q = "how many " + main_noun + " " + modelverb + " " + convert_tree_to_string(np) + " "+ main_verb+ "?"
            questions.append(q.capitalize())
        except:
            pass
    return questions

def is_number_plural(noun_phrase):
    if noun_phrase[0].label() == 'CD':
        if noun_phrase[-1].label() == 'NNS':
            return noun_phrase[-1][0]

        
def whose_question(list_of_phrases):
    questions=[]
    for phrase in list_of_phrases:
        try:
            np = phrase[0]
            vp = phrase[1]
            possessive_word = is_possessive(np)
            if possessive_word:              
                q = "whose "+ possessive_word + " " + convert_tree_to_string(vp) + "?"
            else:
                vp =phrase[1]
                #In the verb phrase, find the modal verb/aux verb
                first_verb= vp[0]
                if(is_verb_tensed(first_verb) and first_verb.label() != VP):
                    sec_noun = vp[1]
                    if sec_noun.label() == 'NNP':
                        possessive_word = is_possessive(sec_noun)
                        if possessive_word:
                            main_noun = is_num
                            #check if first phrase is a model verb
                            if is_modal(first_verb, vp):
                                modelverb=convert_tree_to_string(first_verb)
                                main_verb = convert_tree_to_string(vp[1])
                            #add Do or Did or Does as model verb
                            else:
                                tense =get_tense(first_verb)
                                modelverb=get_conjugate('do',tense)
                                main_verb = get_conjugate(convert_tree_to_string(first_verb), VERB_PRESENT_SINGULAR)
                            q = "whose " + possessive_word + " " + modelverb + " " + convert_tree_to_string(np) + " "+ main_verb+ "?"
            questions.append(q.capitalize())
        except:
            pass
    return questions


def is_possessive(noun_phrase):
    #print noun_phrase[-1][0]
    if noun_phrase[0][0] == 'NNP':
        if noun_phrase[0][-1].label() == 'POS':
            return noun_phrase[-1][0]
        

def get_continuous_chunks(tagged_sent):
    continuous_chunk = []
    current_chunk = []

    for token, tag in tagged_sent:
        if tag != "O":
            current_chunk.append((token, tag))
        else:
            if current_chunk: # if the current chunk is not empty
                continuous_chunk.append(current_chunk)
                current_chunk = []
    # Flush the final current_chunk into the continuous_chunk, if any.
    if current_chunk:
        continuous_chunk.append(current_chunk)
    return continuous_chunk

def get_named_entities_string(named_entities):
    return [(" ".join([token for token, tag in ne]), ne[0][1]) for ne in named_entities]


def get_named_entities(tagger, sent):
    tags = tagger.tag(sent.split())
    named_entities = get_continuous_chunks(tags)
    return get_named_entities_string(named_entities)


def generate_question_types(list_of_tags, list_of_phrases):
    questions = []
    for index in range(len(list_of_phrases)):
        phrase = list_of_phrases[index]
        tag_list = list_of_tags[index]
        tags = [tag[1] for tag in tag_list]
        tag_val = [tag[0] for tag in tag_list]
        if "LOCATION" in tags:
            questions += where_question(phrase)
            questions += which_question(phrase, tag_val, tags)
        if "ORGANIZATION" in tags:
            questions += which_question(phrase, tag_val, tags)
        #if "MONEY" in tags:
        #    questions += how_much_question(phrase)
        if "DATE" in tags or "TIME" in tags:
            questions += when_question(phrase)
        if "PERSON" in tags:
            questions += who_question(phrase)
            questions += whose_question(phrase)
        questions += how_many_question(phrase)        
        questions += what_question(phrase)
    return questions


# In[6]:


def extract_phrases(parser, myTree, phrase):
    myPhrases = []
    if does_match(myTree, phrase):
        #check for approsition
        if(search_for_matches(myTree, APPOSITION)):
            tree = myTree.copy(True)
            new_sent=list(tree)[0]
            new_sent[0][2].append(new_sent[1])
            sentence = convert_tree_into_string(new_sent[0][2])+"."
            myPhrases.append(parser.raw_parse(sentence).next())
            new_sent[0][0].append(new_sent[1])
            sentence = convert_tree_into_string(new_sent[0][0])+"."
            myPhrases.append(parser.raw_parse(sentence).next())
        else:          
            myPhrases.append( myTree.copy(True) )
    for child in myTree:
        if (type(child) is Tree):
            list_of_phrases = extract_phrases(parser,child, phrase)
            if (len(list_of_phrases) > 0):
                myPhrases.extend(list_of_phrases)
                
    return myPhrases

def search_for_matches(parse_tree, pattern):
    matches = False;
    if does_match(parse_tree, pattern):
        return True
    for child in parse_tree:
        if (type(child) is Tree):
            return search_for_matches(child, pattern)
    return matches

def does_match(myTree, phrase):
    if  (type(phrase) is not tuple):
        return myTree.label() == phrase
    else:
        parent = phrase[0]
        children = phrase[1]
        if myTree.label() == parent and len(myTree) == len(children): # parent matches
            
            for i in xrange(len(myTree)): # check that all children match
                ith_child = myTree[i]
                if not does_match(ith_child, children[i]):
                    return False
            return True
    

def is_modal(first_verb, vp):
    return first_verb.label() == MODAL or (lemma(first_verb[0]) in ['have', 'do', 'is', 'be']
      and len(vp) > 1 and vp.label() == VP)


# In[3]:

#inverts the subj and aux (do-insertion if necessary) to transform the declarative sentence into a binary interrogative form
def factoid_questions(list_of_noun_phrases):
    questions=[]
        
    for phrase in list_of_noun_phrases:
        try:
            parse_tree=phrase[0]
            #divide the sentence into Noun Phrase and Verb Phrase (subject and predicate)
            np =parse_tree[0]
            vp=parse_tree[1]
            #In the verb phrase, find the modal verb/aux verb
            first_verb= vp[0]
            #check if first word in verb phrase is a verb and a 

            if(is_verb(first_verb) and first_verb.label() != VP):

                #check if first phrase is a model verb
                if is_modal(first_verb, vp):
                    modelverb=convert_tree_to_string(first_verb)
                    vp=vp[1]
                #add Do or Did or Does as model verb
                else:
                    tense =get_tense(first_verb)
                    modelverb=get_conjugate('do',tense)

                q= modelverb + " " + convert_tree_to_string(np)+" "+ convert_tree_to_string(vp) +" ?"
                questions.append(q.capitalize())
                #check if vp contains adjective
                verb_phrase =""
                contains_adj=False
                for word in vp.pos():
                    if(word[1].startswith('J')):
                        lst = list(word)
                        ant =find_antonyms(word[0])
                        if(ant is not None):
                            contains_adj=True
                            if(word[1].endswith('S')):
                                ant =superlative(ant)
                            if(word[1].endswith('R')):
                                ant =comparative(ant)
                            lst[0] = ant
                            word = tuple(lst)
                    verb_phrase+=word[0]+" "
                if contains_adj  :      
                    q= modelverb + " " + convert_tree_to_string(np)+" "+ verb_phrase +" ?"
                    questions.append(q.capitalize())
        except:
            pass
    return questions

    
def find_antonyms(phrase):
    phrase=wn.morphy(phrase, wn.ADJ)
    if phrase is not None:
        good = wn.synset(lemma(phrase)+'.a.01')
        bad =good.lemmas()[0].antonyms()
        if(len(bad)>0):
            return bad[0].name()
        else:
            return None
    else:
        return None


# In[ ]:

def which_question(list_of_phrases, tag_val, tag_names):
    questions=[]
    for phrase in list_of_phrases:
        try:
            #divide the sentence into Noun Phrase and Verb Phrase (subject and predicate)
            np =phrase[0] # will be used to check if NP is Person
            vp =phrase[1]
            #In the verb phrase, find the modal verb/aux verb
            first_verb= vp[0]
            text = convert_tree_to_string(np)      
            for index in range(0, len(tag_val)):
                tag = tag_val[index]
                tag_name = tag_names[index]
                if tag in text:
                    if(is_verb_tensed(first_verb) and first_verb.label() != VP):
                        #check if first phrase is a model verb
                        if is_modal(first_verb, vp):
                            entities = extract_entities(tag)
                            name_entity_pairs = get_name_entitiy_pairs(ET.ElementTree(ET.fromstring(entities.text)))
                            #print name_entity_pairs
                            #print tag
                            if len(name_entity_pairs) != 1:
                                continue
                            modelverb=convert_tree_to_string(first_verb)

                            label = name_entity_pairs[0][1]
                            ORG = ['Company', 'Organization', 'Facility'] 
                            LOC = ['Location', 'Country', 'City']

                            if (label in ORG and tag_name == "ORGANIZATION") or (label in LOC and tag_name == "LOCATION"):
                                q= "which " + label+ " "+ modelverb + " " + convert_tree_to_string(vp[1]) + "?"
                                if(len(q.strip().split()) > 5):
                                    questions.append(q.capitalize())
        except:
            pass
    return questions


# In[ ]:

def extract_entities(text):
    baseURL="http://access.alchemyapi.com/calls/text/TextGetRankedNamedEntities"
    payload = {'apikey': '4c114af82077ed75c0ad49a3f9871c4eabd73d88', 'text': text}
    r = requests.get(baseURL, params = payload)
    return r

def get_name_entitiy_pairs(tree):
    entities = tree.getroot().find('entities')
    name_entity_pairs = []
    for entity in entities.findall('entity'):
        name = entity.find('text').text
        entity = entity.find('type').text
        name_entity_pairs.append((name, entity))
    return name_entity_pairs


# In[ ]:



