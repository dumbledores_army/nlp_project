
# coding: utf-8

# In[54]:

import nltk, string
from sklearn.feature_extraction.text import TfidfVectorizer
stemmer = nltk.stem.porter.PorterStemmer()
from ans_preprocess import *
import numpy as np
from itertools import chain
from ans_preprocess import *
import sys
import stanford_ner
import stanford_parser
from question_helper import *
from postag import *
import re
import itertools
from nltk.stem.snowball import SnowballStemmer
stemmer2 = SnowballStemmer("english")


# In[55]:

#compute cosine sil=milarity betwen doc and question

def csim(text1, text2):
    try:
        vectorize =  create_vector()
        tot_text = [text1, [text2]]
        fin_text = list(chain.from_iterable(tot_text))
        tfidf = vectorize.fit_transform(fin_text)
        cf = ((tfidf * tfidf.T).A)#[0,1]
    except:
        pass
    return cf
    


# In[56]:

def best_match(text1,text2):
    try:
        
        d = csim(text1, text2)
        #find the no of rows and columns
        no_row = d.shape[0]
        no_col = d.shape[1]
        #last row is of the question
        q_vec = d[no_row-1]
        q_vec[no_col- 1] = 0
        #print type(np.argmax(q_vec))
        c = np.argmax(q_vec)
        #print type(c)
        c = int(c)
        #print type(c)
    except:
        pass
    return c#np.argmax(q_vec)
    
        


# In[57]:




# In[58]:

trivial_word_list =["have", "did" ,"ever"]

def is_noun(tag):
    return tag in ['NN', 'NNS', 'NNP', 'NNPS']


def is_verb(tag):
    return tag in ['VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ']


def is_adverb(tag):
    return tag in ['RB', 'RBR', 'RBS']


def is_adjective(tag):
    return tag in ['JJ', 'JJR', 'JJS']


def pos_to_wnpos(tag):
    try:

        if is_adjective(tag):
            return wn.ADJ
        elif is_noun(tag):
            return wn.NOUN
        elif is_adverb(tag):
            return wn.ADV
        elif is_verb(tag):
            return wn.VERB
    except:
        pass
    return None

def lematize_words(token):
    try:
        wnl = nltk.WordNetLemmatizer()
        newlist = []
        for t in token:
            tag = pos_to_wnpos(t[1])
            lst =list(t)
            if(tag is not None):
                lst[0] = stemmer.stem(wnl.lemmatize(t[0],tag))


            newlist.append(lst)
        token =tuple(newlist)
    except:
        pass
    return token      

def lemmalist(str):
    try:
        syn_set = []
        for synset in wn.synsets(str):
            for item in synset.lemma_names():
                syn_set.append(item)
    except:
        pass
    return syn_set

def transform_qa(q_t,a_t):
    try:
        a_tag= nltk.pos_tag(a_t.lower().split())
        q_tag= nltk.pos_tag(q_t.lower().split()[1:])
        a_tag= lematize_words(a_tag)
        q_tag =lematize_words(q_tag)
        a_t = [x[0] for x in a_tag]
        q_t = [x[0] for x in q_tag]
    except:
        pass
    return a_t, q_t, a_tag, q_tag


def answer_binary(q,a):
    try:
        a_t, q_t, a_tag, q_tag= transform_qa(q,a)
        d = dict()
        for x,y in a_tag:
            if(d.has_key(y)):
                d[y].append(x)
            else:
                d[y] = [x]
        answer ="Yes"

        for i in range (len(q_tag)):
            tag =q_tag[i]
            if tag[1] in [PREP,DET]:
                continue
            if tag[0] in trivial_word_list:
                continue
            if not((d.get(tag[1]) is not None and tag[0] in d.get(tag[1])) or tag[0] in a_t or q.split()[i+1] in a.split()):
                #check for synonyms
                if set(lemmalist(tag[0]) ).isdisjoint(a_t) :
                    if not ( tag[1].startswith('N') or tag[1].startswith('PRP')):
                        answer="No"
    except:
        pass
    return answer
   


# In[59]:

def answer_who(q,a,tags):
    try:
        ans= [tag[0][0] for tag in tags if tag[0][1] == "PERSON"]
        ans = remove_question_word(ans,q)
        if(len(ans)==1):
            a=ans[0]
    except:
        pass
    return a


def answer_where(q,a,tags):
    try:
        ans= [tag[0][0] for tag in tags if tag[0][1] == "LOCATION"]
        ans = remove_question_word(ans,q)
        if(len(ans)==1):
            a=ans[0]
    except:
        pass
    return a


def answer_when(q,a,tags):
    try:
        ans= [tag[0][0] for tag in tags if tag[0][1] == "DATE" or  tag[0][1]== "TIME"]
        ans = remove_question_word(ans,q)
        if(len(ans)==1):
              a=ans[0]
    except:
        pass
    return a

def remove_question_word(ans,q):
    try:
        for ac in ans:
            if ac in q.split():
                ans.remove(ac)
    except:
        pass
    return ans
    
def answer_how(q,a,tags):
    try:
        a_tag= nltk.pos_tag(a.lower().split())
        ans =[item[0] for item in a_tag if item[1] == NUMBER]
        ans = remove_question_word(ans,q)

        if(len(ans)==1):
            list_of_words = a.split()
            next_word = list_of_words[list_of_words.index(ans[0]) + 1]
            a=ans[0] +" " +next_word
    except:
        pass    
    return a
    
    


# In[60]:

def tokenize_list(tot_text):
    try:
        tl = list()
        for u in tot_text:
            for i in u:
                thl2 =list()
                a = stemmer2.stem(i)
                thl2.append(a)
            tl.append( thl2)
        merged = list(itertools.chain.from_iterable(tl))
        #print merged
    except:
        pass
    return merged


# In[62]:




# In[ ]:



