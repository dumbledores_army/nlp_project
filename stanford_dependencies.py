
# coding: utf-8

# In[ ]:

import StanfordDependencies


# In[ ]:

# locations of Stanford parser/models, Java, and English PCFG model
USER_STANFORD_PARAMS = {}
HOME_ROOT = 'LibraryFiles/lib/stanford-parser-full-2015-01-30/'
USER_STANFORD_PARAMS['home'] = [HOME_ROOT, HOME_ROOT, '/usr/bin/java', HOME_ROOT + '/englishPCFG.ser.gz']


# In[ ]:




# In[ ]:

sd = StanfordDependencies.get_instance(jar_filename='point to Stanford Parser JAR file')


# In[ ]:

print sd.convert_tree(str(nltk_tree)) # str() to convert the NLTK tree to Penn Treebank format

