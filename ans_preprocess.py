
# coding: utf-8

# In[1]:

import nltk
import bs4
import string
import operator
import timeit
from bs4 import BeautifulSoup
from nltk.tokenize import sent_tokenize
from nltk.tokenize import PunktSentenceTokenizer
from itertools import chain
from sklearn.feature_extraction.text import TfidfVectorizer
import re
stemmer = nltk.stem.porter.PorterStemmer()
remove_punc = dict((ord(char), None) for char in string.punctuation)

from nltk.stem.snowball import SnowballStemmer
stemmer2 = SnowballStemmer("english")


# In[2]:

#filename  = "data/S08/data/set1/a8o.htm"


# In[3]:

def get_data(htm_file):
    try:
        f = open(htm_file, "r")
        soup = BeautifulSoup(f,"lxml")
        stop = timeit.default_timer();
        tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
        data = soup.decode('utf-8')
        sentence_list = []
        all_sent =list()
        for paragraph in soup.find_all('p'):
            text = paragraph.get_text()
            #Remove parens and contents
            regex = re.compile('\(.+?\)')
            text = regex.sub('', text)
            #Remove brackets and contents
            regex1 = re.compile('\[.+?\]')
            text = regex1.sub('', text)
            text = text.encode('ascii', 'ignore').decode('ascii','ignore')
            text = str(text)
            sent_tokenizer = PunktSentenceTokenizer(text)
            sents = sent_tokenizer.tokenize(text)
            all_sent.append(sents)
        f.close()
    except:
        pass
    return all_sent


# In[4]:

def prep_data(filen):
    try:
        # creates a list of lists. Inner lists may contain 0,1 or more sentences
        sent = get_data(filen)
        # flattens the list of lists into a single list of sentences fom the document
        input_text = list(chain.from_iterable(sent))
        # print input_text
    except:
        pass
    return input_text


# In[5]:

#prep_data(filename)


# In[6]:

def tok_stem(tokens):
    return [stemmer2.stem(item) for item in tokens]


# In[7]:

#removes punctuations,converts to lowercase and then does stemming'''
def norm(text):
    return tok_stem(nltk.word_tokenize(text.lower().translate(remove_punc)))

def create_vector():
    vectorizer = TfidfVectorizer(tokenizer=norm, stop_words='english')
    return vectorizer

