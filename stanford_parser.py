
# coding: utf-8

# In[4]:

import os
from nltk.parse.stanford import StanfordParser
from nltk.internals import find_jars_within_path

#import StanfordDependencies


# In[5]:

# locations of Stanford parser/models, Java, and English PCFG model
USER_STANFORD_PARAMS = {}
HOME_ROOT = 'LibraryFiles/lib/stanford-parser-full-2015-01-30/'
USER_STANFORD_PARAMS['home'] = [HOME_ROOT, HOME_ROOT, '/usr/bin/java', HOME_ROOT + 'englishPCFG.ser.gz']


# creates an NLTK interface to the Stanford parser installed on user's machine
def create_parser():
    params = USER_STANFORD_PARAMS['home']
    os.environ['STANFORD_PARSER'] = params[0]
    os.environ['STANFORD_MODELS'] = params[1]
    os.environ['JAVAHOME'] = params[2]
    os.environ['CLASSPATH'] = params[2]
    global ENGLISH_PCFG_LOC
    ENGLISH_PCFG_LOC = params[3]
    parser=StanfordParser(model_path=ENGLISH_PCFG_LOC)
    return parser
  


# In[3]:

# parser = create_parser()
# print parser._classpath
# print list(parser.raw_parse("the quick brown fox jumps over the lazy dog"))


# In[ ]:



