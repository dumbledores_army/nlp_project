
# coding: utf-8

# In[1]:

import stanford_ner
import stanford_parser
import timeit
from nltk.tree import *
from postag import *
from preprocessing import *
from question_helper import *
import sys


# In[2]:

APPOSITION=((SENTENCE, ((NP, (NP, COMMA, NP, COMMA)), VP, PERIOD)))

# converts the tree to string form
def convert_tree_to_string(tree):
  return ' '.join(tree.leaves())

def does_match(myTree, phrase):
    if  (type(phrase) is not tuple):
        return myTree.label() == phrase
    else:
        parent = phrase[0]
        children = phrase[1]
        if myTree.label() == parent and len(myTree) == len(children): # parent matches
            
            for i in xrange(len(myTree)): # check that all children match
                ith_child = myTree[i]
                if not does_match(ith_child, children[i]):
                    return False
            return True
    

def extract_phrases(parser, myTree, phrase):
    myPhrases = []
    if does_match(myTree, phrase):
        #check for approsition
        if(search_for_matches(myTree, APPOSITION)):
            tree = myTree.copy(True)
            new_sent=list(tree)[0]
            new_sent[0][2].append(new_sent[1])
            sentence = convert_tree_into_string(new_sent[0][2])+"."
            myPhrases.append(parser.raw_parse(sentence).next())
            new_sent[0][0].append(new_sent[1])
            sentence = convert_tree_into_string(new_sent[0][0])+"."
            myPhrases.append(parser.raw_parse(sentence).next())
        else:          
            myPhrases.append( myTree.copy(True) )
    for child in myTree:
        if (type(child) is Tree):
            list_of_phrases = extract_phrases(parser,child, phrase)
            if (len(list_of_phrases) > 0):
                myPhrases.extend(list_of_phrases)
                
    return myPhrases

def convert_tree_into_string(tree_structure):
  return ' '.join(tree_structure.leaves())


# In[3]:

def search_for_matches(parse_tree, pattern):
    matches = False;
    if does_match(parse_tree, pattern):
        return True
    for child in parse_tree:
        if (type(child) is Tree):
            return search_for_matches(child, pattern)
    return matches

def does_match(myTree, phrase):
    if  (type(phrase) is not tuple):
        return myTree.label() == phrase
    else:
        parent = phrase[0]
        children = phrase[1]
        if myTree.label() == parent and len(myTree) == len(children): # parent matches
            
            for i in xrange(len(myTree)): # check that all children match
                ith_child = myTree[i]
                if not does_match(ith_child, children[i]):
                    return False
            return True
    

def is_modal(first_verb, vp):
    return first_verb.label() == MODAL or (lemma(first_verb[0]) in ['have', 'do', 'is', 'be']
      and len(vp) > 1 and vp.label() == VP)

def convert_tree_into_string(tree_structure):
  return ' '.join(tree_structure.leaves())


def is_verb(phrase):
  return phrase.label().startswith('V') or phrase.label() == 'MD'
# returns True iff the verb is a modal or auxiliary 'have', 'do'

TENSE_MAPPING = {VERB_INF:'inf' , VERB_PRESENT_SINGULAR:'1sg' ,VERB_PRESENT_PL:'3sg', VERB_PAST:'p' ,VERB_PAST_PART:'ppart' }
def  get_tense(first_verb):
    return TENSE_MAPPING.get(first_verb.label())
    
def get_conjugate(phrase,tense):
    return conjugate(phrase, tense)

VERB_TENSES = [VERB_PAST, VERB_PRESENT_SINGULAR , VERB_PRESENT_PL]
def is_verb_tensed(phrase):
    return phrase.label() in VERB_TENSES


# In[4]:

#inverts the subj and aux (do-insertion if necessary) to transform the declarative sentence into a binary interrogative form
def factoid_questions(list_of_noun_phrases):
    questions=[]
    for phrase in list_of_noun_phrases:
        try:
            parse_tree=phrase[0]
            #divide the sentence into Noun Phrase and Verb Phrase (subject and predicate)
            np =parse_tree[0]
            vp=parse_tree[1]
            #In the verb phrase, find the modal verb/aux verb
            first_verb= vp[0]
            #check if first word in verb phrase is a verb and a 

            if(is_verb(first_verb) and first_verb.label() != VP):

                #check if first phrase is a model verb
                if is_modal(first_verb, vp):
                    modelverb=convert_tree_to_string(first_verb)
                    vp=vp[1]
                #add Do or Did or Does as model verb
                else:
                    tense =get_tense(first_verb)
                    modelverb=get_conjugate('do',tense)
                q= modelverb + " " + convert_tree_to_string(np)+" "+ convert_tree_to_string(vp) +" ?"
                questions.append(q.capitalize())
        except:
            pass
    return questions


# In[5]:

def who_question(list_of_phrases):
    questions=[]
    for phrase in list_of_phrases:
        try:
            parse_tree=phrase[0]
            #divide the sentence into Noun Phrase and Verb Phrase (subject and predicate)
            np =parse_tree[0] # will be used to check if NP is Person
            vp =parse_tree[1]
            #In the verb phrase, find the modal verb/aux verb
            first_verb= vp[0]
            if(is_verb_tensed(first_verb) and first_verb.label() != VP):
            #check if first word in verb phrase is a verb and a 
                q= "who" + " " + convert_tree_to_string(vp) +" ?"
                questions.append(q.capitalize())
        except:
            pass
    return questions
        


# In[6]:

def where_question(list_of_phrases):
    questions=[]
    for phrase in list_of_phrases:
        try:
            parse_tree=phrase[0] #will be used as subject
            #divide the sentence into Noun Phrase and Verb Phrase (subject and predicate)
            vp =phrase[1]
            #In the verb phrase, find the modal verb/aux verb
            first_verb= vp[0]
            if(is_verb_tensed(first_verb) and first_verb.label() != VP):
                #check if first phrase is a modal verb
                if is_modal(first_verb, vp):
                    modelverb=convert_tree_to_string(first_verb)
                    main_verb = convert_tree_to_string(vp[1][0])
                #add Do or Did or Does as an artificial model verb
                else:
                    tense =get_tense(first_verb)
                    modelverb=get_conjugate('do',tense)
                    main_verb = get_conjugate(convert_tree_to_string(vp[0]), VERB_PRESENT_SINGULAR)
                q= "where " + modelverb + " " + convert_tree_to_string(parse_tree) + " "+ main_verb + "?"
                questions.append(q.capitalize())
        except:
            pass
    return questions
        


# In[7]:

def when_question(list_of_phrases):
    questions=[]
    for phrase in list_of_phrases:
        parse_tree=phrase[0]
    #divide the sentence into Noun Phrase and Verb Phrase (subject and predicate)
        np =parse_tree[0] # will be used to check if NP is subject
        vp =phrase[1]
        #In the verb phrase, find the modal verb/aux verb
        first_verb= vp[0]
        if(is_verb(first_verb) and first_verb.label() != VP):
            #check if first phrase is a model verb
            if is_modal(first_verb, vp):
                modelverb=convert_tree_to_string(first_verb)
                main_verb = convert_tree_to_string(vp[1])
            #add Do or Did or Does as model verb
            else:
                tense =get_tense(first_verb)
                modelverb=get_conjugate('do',tense)
                main_verb = get_conjugate(convert_tree_to_string(vp[0]), VERB_PRESENT_SINGULAR) + " " + convert_tree_to_string(vp[1])
        #check if first word in verb phrase is a verb and a 
            q= "when " + modelverb + " " + convert_tree_to_string(np) + " "+ main_verb+ "?"
            questions.append(q.capitalize())
    return questions
        


# In[8]:

def what_question(list_of_phrases):
    questions=[]
    for phrase in list_of_phrases:
        try:
            #divide the sentence into Noun Phrase and Verb Phrase (subject and predicate)
            np =phrase[0] # will be used to check if NP is Person
            vp =phrase[1]
            #In the verb phrase, find the modal verb/aux verb
            first_verb= vp[0]
            if(is_verb_tensed(first_verb) and first_verb.label() != VP):
                #check if first phrase is a model verb
                if is_modal(first_verb, vp):
                    modelverb=convert_tree_to_string(first_verb)
                    vp=vp[1]
                #add Do or Did or Does as model verb
                else:
                    tense =get_tense(first_verb)
                    modelverb=get_conjugate('do',tense)
            #check if first word in verb phrase is a verb and a 
                q= "what " + convert_tree_to_string(first_verb) + " " + convert_tree_to_string(np) + "?"
            questions.append(q.capitalize())
        except:
            pass
    return questions


# In[9]:

def how_much_question(list_of_phrases):
    print "how"


# In[145]:

def how_many_question(list_of_phrases):
    questions=[]
    for phrase in list_of_phrases:
        try:
            #divide the sentence into Noun Phrase and Verb Phrase (subject and predicate)
            np =phrase[0]
            vp = phrase[1]
            main_noun = is_number_plural(np)
            if main_noun:
                q= "how many " + main_noun + " " + convert_tree_to_string(vp) + "?"
            else:
                vp =phrase[1]
                #In the verb phrase, find the modal verb/aux verb
                first_verb= vp[0]
                if(is_verb_tensed(first_verb) and first_verb.label() != VP):
                    sec_noun = vp[1]
                    if sec_noun.label() == 'NP':
                        main_noun = is_number_plural(sec_noun)
                        if main_noun:
                            #check if first phrase is a model verb
                            if is_modal(first_verb, vp):
                                modelverb=convert_tree_to_string(first_verb)
                                main_verb = convert_tree_to_string(vp[1])
                            #add Do or Did or Does as model verb
                            else:
                                tense =get_tense(first_verb)
                                modelverb=get_conjugate('do',tense)
                                main_verb = get_conjugate(convert_tree_to_string(first_verb), VERB_PRESENT_SINGULAR)
                        q = "how many " + main_noun + " " + modelverb + " " + convert_tree_to_string(np) + " "+ main_verb+ "?"
            questions.append(q.capitalize())
        except:
            pass
    return questions


# In[102]:

def is_number_plural(noun_phrase):
    if noun_phrase[0].label() == 'CD':
        if noun_phrase[-1].label() == 'NNS':
            return noun_phrase[-1][0]


# In[159]:

def whose_question(list_of_phrases):
    questions=[]
    for phrase in list_of_phrases:
        np = phrase[0]
        vp = phrase[1]
        possessive_word = is_possessive(np)
        print possessive_word
        if possessive_word:              
            q = "whose "+ possessive_word + " " + convert_tree_to_string(vp) + "?"
        else:
            vp =phrase[1]
            #In the verb phrase, find the modal verb/aux verb
            first_verb= vp[0]
            if(is_verb_tensed(first_verb) and first_verb.label() != VP):
                sec_noun = vp[1]
                if sec_noun.label() == 'NNP':
                    possessive_word = is_possessive(sec_noun)
                    if possessive_word:
                        main_noun = is_num
                        #check if first phrase is a model verb
                        if is_modal(first_verb, vp):
                            modelverb=convert_tree_to_string(first_verb)
                            main_verb = convert_tree_to_string(vp[1])
                        #add Do or Did or Does as model verb
                        else:
                            tense =get_tense(first_verb)
                            modelverb=get_conjugate('do',tense)
                            main_verb = get_conjugate(convert_tree_to_string(first_verb), VERB_PRESENT_SINGULAR)
                        q = "whose " + possessive_word + " " + modelverb + " " + convert_tree_to_string(np) + " "+ main_verb+ "?"
        questions.append(q.capitalize())
    return questions


# In[160]:

def is_possessive(noun_phrase):
    #print noun_phrase[-1][0]
    if noun_phrase[0][0] == 'NNP':
        if noun_phrase[0][-1].label() == 'POS':
            return noun_phrase[-1][0]


# In[ ]:

def which_type(phrase):
    print "which"
        


# In[10]:

def get_continuous_chunks(tagged_sent):
    continuous_chunk = []
    current_chunk = []

    for token, tag in tagged_sent:
        if tag != "O":
            current_chunk.append((token, tag))
        else:
            if current_chunk: # if the current chunk is not empty
                continuous_chunk.append(current_chunk)
                current_chunk = []
    # Flush the final current_chunk into the continuous_chunk, if any.
    if current_chunk:
        continuous_chunk.append(current_chunk)
    return continuous_chunk


# In[11]:

def get_named_entities_string(named_entities):
    return [(" ".join([token for token, tag in ne]), ne[0][1]) for ne in named_entities]


# In[12]:

NER_QUESTIONS = ['who', 'where', 'when', 'what']
NON_NER_QUESTIONS = ['how', 'why', 'whose', 'which', 'how_many', 'how_much'] #how would also check for how_much and how_many


# In[13]:

question_types = ['who_question']
determinants = ['this', 'that', 'these', 'those']


# In[121]:

def generate_question_types(list_of_tags, list_of_phrases):
    
    questions = []
    for index in range(len(list_of_phrases)):
        phrase = list_of_phrases[index]
        tags = list_of_tags[index]
        for tag in tags:
            val = tag[1]
            if(val == "LOCATION"):
                questions += where_question(phrase)
            if (val == "MONEY"):
                questions += how_much_question(phrase)
            if (val == "DATE" or val == "TIME"):
                questions += when_question(phrase)
            if (val == "PERSON"):
                questions += who_question(phrase)
                questions += whose_question(phrase)
            if is_how_many_question(phrase):
                questions += how_many_question(phrase)        
        questions += what_question(phrase)
    return questions


# In[109]:

SUB_PRED =(ROOT, ((SENTENCE, (NP, VP, PERIOD)),))
APPOSITION=((SENTENCE, ((NP, (NP, COMMA, NP, COMMA)), VP, PERIOD)))

filename  = "data/S08/data/set2/a1o.htm"

#Create Stanford Parser instance
parser = stanford_parser.create_parser()

#Create Stanford NER Tagger instance
tagger = stanford_ner.create_tagger()

#get preprocessed text
#text =get_sentences(filename)
#text = ("The cat had slept on the mat.","I enjoyed my cookies yesterday.","Steve Smith lived in New York.","Obama, president of USA, was born in Kenya.")
#text = ("300 people died in the fire last night.","The company employs 300 people.")
text = ("Ray Kroc's plan did not fail.","We were surprised by Peter's actions.")

sents =parser.raw_parse_sents(text)

list_of_phrases=[]
questions =[]     
list_of_named_entities = []


def process_txt(sentence,parser,list_of_phrases,list_of_named_entities):
    phrase = extract_phrases(parser,sentence, SUB_PRED)
    for p in phrase:
        sent = convert_tree_into_string(sentence)   
        tags = tagger.tag(sent.split())
        named_entities = get_continuous_chunks(tags)
        named_entities_str_tag = get_named_entities_string(named_entities)
        list_of_named_entities.append(named_entities_str_tag)
        list_of_phrases.append(p)


# In[110]:

i=1;
for line in sents:
    for sentence in line:
        process_txt(sentence,parser,list_of_phrases,list_of_named_entities)


# In[111]:

print len(list_of_named_entities)
print len(list_of_phrases)


# In[157]:

questions = []


# In[161]:

#Create factoid Questions
#questions.extend(factoid_questions(list_of_phrases))
#Create WH Questions
questions.extend(generate_question_types(list_of_named_entities, list_of_phrases))


# In[155]:

questions
##Below code is being tested
            


# In[ ]:

#testing timex for extracting features for when type questions
from timex import tag
tag("Dave lived in New York and payed $ 10,000 as rent on 22 January 2015 at 10:15 and next month as well.")


# In[ ]:

#dep_instance = stanford_parser.create_dependency_instance()


# In[ ]:

#sent_parsed = parser.parse(("Steve Smith was living in New York.","Steve Smith lived in New York.", "Steve Smith used to live in New York.")) 
#for parsed in sent_parsed:
#    tokens = dep_instance.convert_tree(str(parsed), universal=False)
#    for token in tokens:
#        print token


# In[ ]:




# In[ ]:



