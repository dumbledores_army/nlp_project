
# coding: utf-8

# In[7]:

import sys
import os
import urlparse
import requests
import xml.etree.ElementTree as ET
import nltk
BART_SERVER = 'http://localhost:8125'




# In[26]:

# text ="Barak Obama works for the goverment . He lives in New York along with his wife Michel"
# a ="He lives in New York along with his wife Michel"

# text =" Abraham Lincoln (February 12, 1809  April 15, 1865) was the sixteenth President of the United States, serving from March 4, 1861 until his assassination. Lincoln won the Republican Party nomination in 1860 and was elected president later that year. During his term, he helped preserve the United States by leading the defeat of the secessionist Confederate States of America in the American Civil War. He introduced measures that resulted in the abolition of slavery, issuing his Emancipation Proclamation in 1863 and promoting the passage of the Thirteenth Amendment to the Constitution in 1865. "
# a="He introduced measures that resulted in the abolition of slavery, issuing his Emancipation Proclamation in 1863 and promoting the passage of the Thirteenth Amendment to the Constitution in 1865."


# In[37]:

def process_answer(text,a):
    try:
        post_url = urlparse.urljoin(BART_SERVER, '/BARTDemo/ShowText/process/')
        response = requests.post(post_url, text)
        a_tag= nltk.pos_tag(a.split()[0:])
        pronoun_list = [item[0] for item in a_tag if item[1] == 'PRP']
        xmlstring = response.content
        tree = ET.ElementTree(ET.fromstring(xmlstring))
        #print xmlstring

        for pronoun in pronoun_list:
            doc = tree.getroot()

            sent = doc.findall('s')

            ans_sent = sent[len(sent)-1]

        proper_noun =""
        for coref in ans_sent.findall('coref'):
                set_id=0
                for word in coref.findall('w'):
                     if (word.text == pronoun):
                        set_id = coref.get("set-id")
                complete = False
                for s in sent:
                    for coref in s.findall('coref'):
                         if(coref.get("set-id")==set_id):
                            #check if all the words are NP
                            complete= True
                            for word in coref.findall('w'):
                                if (word.get("pos") !='nnp'):
                                    complete= False
                                    break
                                else:
                                    proper_noun  = proper_noun +" "+word.text

                            if complete:
                                break
                    if complete:
                        break
                if(len(proper_noun)> 0):
                    a= a.replace(pronoun,proper_noun)
    except:
        pass
    return a


# In[38]:

#process_answer(text,a)


# In[ ]:




# In[ ]:



