
# coding: utf-8

# In[ ]:

#!/usr/bin/python

import stanford_ner
import stanford_parser
import timeit
from nltk.tree import *
from postag import *
from preprocessing import *
from question_helper import *
import sys
from post_processing import *
import random


# In[ ]:

# filename  = "data/S08/data/set2/a4o.htm"
# no_questions = 20

filename  = sys.argv[1]
no_questions = int(sys.argv[2])




# In[ ]:

SUB_PRED =(ROOT, ((SENTENCE, (NP, VP, PERIOD)),))
APPOSITION=((SENTENCE, ((NP, (NP, COMMA, NP, COMMA)), VP, PERIOD)))


#Create Stanford Parser instance
parser = stanford_parser.create_parser()

#Create Stanford NER Tagger instance
tagger = stanford_ner.create_tagger()

#get preprocessed text
text =get_sentences(filename)
#text = ("The cat had slept on the mat.","I enjoyed my cookies yesterday.","Steve Smith lived in New York.","Obama, president of USA, was born in Kenya.")
#text = ("New York is the biggest city in the world.","New Jersey sucks.")
if(len(text)>100):
    text = random.sample(text, 100)


print "Pre Processing complete"

sents =parser.raw_parse_sents(text)


# In[ ]:

list_of_phrases=[]
questions =[]     
list_of_named_entities = []


# In[ ]:

def process_txt(sentence):
    phrase = extract_phrases(parser,sentence, SUB_PRED)
    for p in phrase:
        sent = convert_tree_into_string(sentence)   
        named_entities_str_tag = named_entities_str_tag = get_named_entities(tagger, sent)
        list_of_named_entities.append(named_entities_str_tag)
        list_of_phrases.append(p)
   


# In[ ]:

def get_next(questions, new_q_list, no_questions):
    q= next(questions,None)
    if(q is not None):
        new_q_list.append(q)
        no_questions -=1
    return new_q_list, no_questions


# In[ ]:

for line in sents:
    for sentence in line:
        process_txt(sentence)
print "Parsing complete"


# In[ ]:

#Create factoid Questions
questions.extend(factoid_questions(list_of_phrases))
#Create WH Questions
questions.extend(generate_question_types(list_of_named_entities, list_of_phrases))
print "Question Generation complete"


# In[ ]:

questions = rank_questions(list(set(questions)))
print "Ranking Module complete"


# In[ ]:

#TODO: Rank Question 

# if(len(questions)<int(no_questions)):
#     no_questions=len(questions)
# for i in range (int(no_questions)):
#     print questions[i]

what =iter([s for s in questions if s.startswith('What')])

when =iter( [s for s in questions if s.startswith('When')])

where = iter([s for s in questions if s.startswith('Where')])

who = iter([s for s in questions if s.startswith('Who')])

how = iter([s for s in questions if s.startswith('How')])

whose = iter([s for s in questions if s.startswith('Whose')])

which = iter([s for s in questions if s.startswith('Which')])

factoid = [s for s in questions if not (s.startswith('Which') or s.startswith('Who') or s.startswith('What') or s.startswith('How') or s.startswith('When') or s.startswith('Where') or s.startswith('Whose'))]

no_choice = no_questions/2
new_q_list =random.sample(factoid[:no_choice],no_questions/2)

no_questions -= no_choice
print "Questions"

while no_questions >= 0:
    new_q_list, no_questions = get_next(which, new_q_list, no_questions)
    new_q_list, no_questions = get_next(who, new_q_list, no_questions)
    new_q_list, no_questions = get_next(where, new_q_list, no_questions)
    new_q_list, no_questions = get_next(when, new_q_list, no_questions)
    new_q_list, no_questions = get_next(how, new_q_list, no_questions)
    new_q_list, no_questions = get_next(whose, new_q_list, no_questions)
    new_q_list, no_questions = get_next(what, new_q_list, no_questions)
    


# In[ ]:

for q in new_q_list:
    print q


# In[ ]:



