
# coding: utf-8

# In[7]:

import nltk, string
from sklearn.feature_extraction.text import TfidfVectorizer
stemmer = nltk.stem.porter.PorterStemmer()
from ans_preprocess import *
import numpy as np
from itertools import chain


# In[8]:

#compute cosine sil=milarity betwen doc and question
def csim(text1, text2):
    try:
        vectorize =  create_vector()
        tot_text = [text1, [text2]]
        fin_text = list(chain.from_iterable(tot_text))
        tfidf = vectorize.fit_transform(fin_text)
        cf = ((tfidf * tfidf.T).A)
    except:
        pass
    return cf
    


# In[9]:

def best_match(text1,text2):
    try:
        
        d = csim(text1, text2)
        #find the no of rows and columns
        no_row = d.shape[0]
        no_col = d.shape[1]
        #last row is of the question
        q_vec = d[no_row-1]
        q_vec[no_col- 1] = 0
        #print np.argmax(q_vec)
    except:
        pass
    return text1[ np.argmax(q_vec)]
    
        


# In[14]:

# #example 2 sataset s08/ a4
# print best_match(['Abraham Lincoln (February 12, 1809 – April 15, 1865) was the sixteenth President of the United States, serving from March 4, 1861 until his assassination.',
#                   'Abraham Lincoln was born on February 12, 1809, to Thomas Lincoln and Nancy Hanks, two uneducated farmers.',
#                   'Lincoln was born in a one-room log cabin on the   Sinking Spring Farm, in southeast Hardin County, Kentucky (now part of LaRue County).',
#                   'Lincoln began his political career in 1832, at age 23, with an unsuccessful campaign for the Illinois General Assembly, as a member of the Whig Party.'], 
#                  'Was Abraham Lincoln the sixteenth President of the United States?')
# #


# In[11]:

#example
# d = best_match(['a little heighted fat bird', 'a big bat', 'a cat gladiator','a big fat bird'], 
#                  'what is a bird')
# print d


# In[12]:

# # this is just as a backup
# def cosine_sim(text1, text2):
#     max = 0
#     max_i = 0
#     i = 0
#     for a in text1:
#         #print [a, text2]
#         tfidf = vectorizer.fit_transform([a, text2])
#         c = ((tfidf * tfidf.T).A)[0,1]
#         print c
#         if c > max:
#             max = c
#             max_i = i
#         i += 1
#     print max    
#     print max_i
#     return text1[max_i]
    


# In[13]:

# #example 1
# print cosine_sim(['a little bird', 'a big bat', 'a fat gladiator','a big fat bird'], 
#                  'what is a fat bird')

# #example 2 sataset s08/ a4
# print cosine_sim(['Abraham Lincoln (February 12, 1809 – April 15, 1865) was the sixteenth President of the United States, serving from March 4, 1861 until his assassination.',
#                   'Abraham Lincoln was born on February 12, 1809, to Thomas Lincoln and Nancy Hanks, two uneducated farmers.',
#                   'Lincoln was born in a one-room log cabin on the   Sinking Spring Farm, in southeast Hardin County, Kentucky (now part of LaRue County).',
#                   'Lincoln began his political career in 1832, at age 23, with an unsuccessful campaign for the Illinois General Assembly, as a member of the Whig Party.'], 
#                  'Was Abraham Lincoln the sixteenth President of the United States?')
# #example 3
# print cosine_sim(['a little heighted fat bird', 'a big bat', 'a cat gladiator','a big fat bird'], 
#                  'what is a bird')


# In[ ]:



