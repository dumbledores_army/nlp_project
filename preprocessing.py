
# coding: utf-8

# In[8]:

import nltk
import bs4
from bs4 import BeautifulSoup
from nltk.tokenize import sent_tokenize
from nltk.tokenize import PunktSentenceTokenizer
import string
import operator
import re
import timeit


# In[9]:

#Alternative to linking verb filter (is less strict; can be applied in get_sentences() method)
def contains_helping_verb(sentence):
    helping_verbs = [" am "," is "," are "," was "," were "," have "," has "," had "," do "," does "," did "," could "," would "," should ",
                    " can "," will "," may "," must "," might "]
    for verb in helping_verbs:
        if verb in sentence:
            return True
        else:
            continue
    return False


# In[10]:

#Linking verb filter (applied in the get_sentences() method)
def contains_linking_verb(sentence):
    linking_verbs = [" is "," are "," has been "," have been "]
    for verb in linking_verbs:
        if verb in sentence:
            return True

    return False


# In[11]:

#Given a list of sentences, this method returns a ranked list (best at index 0, worst at end) using a simple
#scoring calculation that positively weights nouns and named entities, and negatively weights pronouns
def rank_sentences(sentence_list):
#     ranked_list = []
#     sentence_map = dict()
#     for sent in sentence_list:
#         score = 0
#         noun_count = 0
#         proper_noun_count= 0
#         pronoun_count = 0
#         tokens = nltk.word_tokenize(sent)
#         text = nltk.Text(tokens)
#         tags = nltk.pos_tag(text)
#         for word,tag in tags:
#             if tag == 'PRP':
#                 pronoun_count += 1
#             elif tag == 'NNP':
#                 proper_noun_count += 1
#             elif tag == 'NN':
#                 noun_count += 1
#         #simple scoring calculation; can change it later
#         score = noun_count + proper_noun_count - pronoun_count
#         sentence_map[sent] = score
#     #get list of sorted tuples (sentence,score)
#     sorted_sentence_list = sorted(sentence_map.items(), key=operator.itemgetter(1), reverse = True)
#     #returns list of sentences, with best at index 0, and worst at the last index of the list
#     for key,val in sorted_sentence_list:
#         ranked_list.append(key)
#     return ranked_list
    sentence_list.sort(key=len, reverse=False)
    return sentence_list
            
#Testing the method    
test = []
test.append("John is great today.")
test.append("She has she want, but he does not.")
rank_sentences(test)


# In[12]:

#This is the preprocessing method that should be called to generate a ranked list of sentences, using filters
#All other methods in this class exist to support this method
def get_sentences(htm_file):
    f = open(htm_file, "r")
    soup = BeautifulSoup(f,"lxml")
    tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
    data = f.read().decode('utf-8')
    sentence_list = []
    [s.extract() for s in soup('[]')]
    for paragraph in soup.find_all('p'):
        text = paragraph.get_text()
        
        #Remove parens and contents
        regex = re.compile('\(.+?\)')
        text = regex.sub('', text)
        #Remove brackets and contents
        regex1 = re.compile('\[.+?\]')
        text = regex1.sub('', text)
        ##Outstanding issues: quotations, sentences with "-",":",";"... obscures the parsing
        ##Should update the ranking criteria for preprocessing to address this, if not eliminate such instances
        
        sent_tokenizer = PunktSentenceTokenizer(text)
        sents = sent_tokenizer.tokenize(text)
        
        for sent in sents:
            #Only include sentences that are 25 words or less, and contains a linking verb
            #print sent
            if len(sent.split()) <= 25 and contains_linking_verb(sent): 
                sentence_list.append(sent)
            else:
                continue
    
    return rank_sentences(sentence_list)


# In[18]:

#Testing the get_sentences() method
#slist = get_sentences('data/S08/data/set2/a1o.htm')

#for i in range(0,len(slist)-1):
#    print slist[i]


# In[ ]:




# In[ ]:




# In[ ]:



